<?php
/**
 * Configuration overrides for WP_ENV === 'staging'
 */

use Roots\WPConfig\Config;

/**
 * You should try to keep staging as close to production as possible. However,
 * should you need to, you can always override production configuration values
 * with `Config::define`.
 *
 * Example: `Config::define('WP_DEBUG', true);`
 * Example: `Config::define('DISALLOW_FILE_MODS', false);`
 */

define('WP_REDIS_PREFIX', 'ys');
define('WP_REDIS_DISABLE_BANNERS', true);
define('WP_REDIS_DISABLE_METRICS', true);
