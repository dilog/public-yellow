<?php

namespace App\Controllers;

use App;
use WP_Query;
use Sober\Controller\Controller;

class FrontPage extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        add_action(
            'wp_ajax_send_contact_form',
            [$this, 'sendContactForm']
        );
        add_action(
            'wp_ajax_nopriv_send_contact_form',
            [$this, 'sendContactForm']
        );
    }

    /**
     * Hero image
     *
     * @return string
     */
    public function heroImage()
    {
        return App\responsiveImage(
            get_field('hero_image'),
            [
                [
                    'size' => [768, 505],
                    'media' => '(max-width: 768px)'
                ],
                [
                    'size' => [1216, 800],
                    'media' => '(min-width: 769px)'
                ],
            ],
            'hero__image'
        );
    }

    /**
     * Shooting & Video PDF
     *
     * @return bool|array
     */
    public function shootingVideoPdf()
    {
        return get_field('shoot_editing_pdf');
    }

    /**
     * Shooting & Video PDF
     *
     * @return bool|array
     */
    public function techSupportPdf()
    {
        return get_field('tech_support_pdf');
    }

    /**
     * Workspaces list
     *
     * @return array|boolean
     */
    public function workspaces()
    {
        $args = [
            'post_type' => 'workspace',
            'posts_per_page' => -1,
            'no_found_rows' => true,
            'update_post_term_cache' => false
        ];
        $the_query = new Wp_Query($args);
        if ($the_query->have_posts()) {
            return collect($the_query->posts)->map(function ($item) {
                return App\cardWorkspaceData($item);
            })->all();
        }
        return false;
    }

    /**
     * SmartSquare contacts
     *
     * @return array|boolean
     */
    public function contacts()
    {
        return [
            'map' => [
                'lat' => '41.904716',
                'lng' => '12.504365'
            ],
            'title' => __('SmartSquare Roma', 'ys'),
            'street' => 'Via Palestro 50',
            'zip' => '00185',
            'city' => __('Roma', 'ys'),
            'country' => __('Italia', 'ys'),
            'email' => antispambot('info@smartsquare.co'),
            'location' => get_field('location')
        ];
    }

    /**
     * Nonce for contact form
     *
     * @return string
     */
    public function nonce()
    {
        return wp_nonce_field('contact-smartsquare', 'nonce', false, false);
    }

    /**
     * Handle Send List form submit
     *
     */
    public static function sendContactForm()
    {
        check_ajax_referer('contact-smartsquare', 'nonce');

        $form = $_POST;
        stripslashes_deep($form);

        // - Honeypot
        if (isset($form['check_email']) && (bool) $form['check_email'] === true) {
            die();
        }

        // - Send notification
        $fullname = $form['fname']. ' ' . $form['lname'];
        $headers  = 'From: SmartSquare Website <noreply@smartsquare.co>' . PHP_EOL;
        $headers .= sprintf(
            'Reply-To: %s <%s>',
            $fullname,
            $form['email']
        ) . PHP_EOL;
        $headers .= 'Content-Type: text/plain; charset=utf-8' . PHP_EOL;

        $body  = 'New message from SmartSquare contact form.' . PHP_EOL . PHP_EOL;
        $body .= 'First Name: ' . $form['fname'] . PHP_EOL;
        $body .= 'Last Name: ' . $form['lname'] . PHP_EOL;
        $body .= 'Email: ' . $form['email'] . PHP_EOL;

        if (!empty($form['phone'])) {
            $body .= 'Phone: ' . $form['phone'] . PHP_EOL;
        }

        if (!empty($form['note'])) {
            $body .= 'Note: ' . $form['note'] . PHP_EOL . PHP_EOL;
        }

        $body .= 'IP address: ' . $_SERVER['REMOTE_ADDR'] . PHP_EOL . PHP_EOL;

        wp_mail(
            'info@smartsquare.co',
            '[SmartSquare Website] Info request',
            $body,
            $headers
        );

        // - Send confirmation email


        $content = App\template('email.confirmation', [
            'title' => sprintf(
                _x('Grazie %s!', 'email', 'smartsquare'),
                $form['fname']
            ),
            'form' => $form,
        ]);

        // Email headers
        $headers  = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=".get_bloginfo('charset')."" . "\r\n";
        $headers .= 'From: SmartSquare <info@smartsquare.co>' . "\r\n";

        // Email Subject
        $subject  = __('SmartSquare, conferma ricezione.', 'ts');

        wp_mail($form['email'], $subject, $content, $headers);

        wp_send_json_success();
    }
}
