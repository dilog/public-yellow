<?php

namespace App;

/**
 * Workstation card data model
 *
 * @param $item
 * @param $classes
 */
function cardWorkspaceData($item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    $gallery_ids = get_field('gallery');
    $facilities = get_field('facilities');
    $prices = [];
    if (have_rows('prices')) {
        while (have_rows('prices')) {
            the_row();
            $type = get_sub_field('type');
            $price = get_sub_field('price');
            $description = get_sub_field('description');
            $prices[] = [
                'type' => $type,
                'price' => $price,
                'description' => $description
            ];
        }
    }

    $data = [
        'class' => 'card card--workspace',
        'id' => get_the_id(),
        'gallery' => collect($gallery_ids)->map(
            function ($image_id) {
                $image_src = wp_get_attachment_image_src($image_id, 'card-workspace');
                return [
                    'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
                    'caption' => wp_get_attachment_caption($image_id),
                    'src' => $image_src[0],
                ];
            }
        )->all(),
        'title' => get_the_title(),
        'content' => get_the_content(),
        'facilities'=> $facilities,
        'people' => get_field('people'),
        'prices' => $prices,
        'form_url' => get_field('form_url')
    ];
    wp_reset_postdata();
    return $data;
}
