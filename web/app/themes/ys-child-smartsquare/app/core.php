<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use Livy\Plumbing\Templates;

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(
            str_replace(
                'ys-child-smartsquare',
                'ys',
                get_theme_file_path().'/dist/assets.json'
            ),
            str_replace(
                'ys-child-smartsquare',
                'ys',
                get_theme_file_uri().'/dist'
            )
        );
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
    * Create @asset() Blade directive
    */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * SmartSquare specific assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script(
        'google-maps',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBEn8xh46Gjw27NisuQyE1sZ33BciyjVxQ',
        [],
        null,
        true
    );
    wp_enqueue_style(
        'ys/smartsquare.css',
        asset_path('styles/smartsquare.css'),
        ['ys/main.css'],
        false,
        null
    );

    if (WP_ENV === 'development') {
        wp_enqueue_script('ys/smartsquare.js', asset_path('scripts/smartsquare.js'), [], null, true);
    }
}, 100);

/**
 * WP Stage Switcher setup
 */
$envs = [
    'development' => 'https://smartsquare.localhost',
    'staging'     => 'https://staging-smartsquare.dev01.it',
    'production'  => ''
];
define('ENVIRONMENTS', $envs);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    add_image_size('card-workspace', 721, 494, true);

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'mobile_navigation' => __('Mobile Navigation', 'sage'),
        'footer_navigation' => __('Footer Navigation', 'sage'),
    ]);
}, 20);

/**
 * Save ACF fields.
 * - Description as post content.
 * - First Gallery image as post thumbnail.
 *
 * @return void
 */
add_action('acf/save_post', function ($post_id) {
    $values = $_POST['acf'];
    $field_description = 'field_5f5b7ca420572';
    if (isset($values[$field_description])) {
        $data = array(
            'ID' => $post_id,
            'post_content' => $values[$field_description],
        );
        wp_update_post($data);
    }

    $field_gallery = 'field_5f5b7a8792f63';
    if (isset($values[$field_gallery]) && !empty($values[$field_gallery])) {
        set_post_thumbnail($post_id, $values[$field_gallery][0]);
    }
}, 5);

/**
 * Courses Select Box
 *
 * Get options data from CRM.
 */
add_filter('acf/load_field/key=field_5f5b86e1df9bd', function ($field) {
    $field['choices'] = [];
    $field['choices']['half-day'] = __('Mezza giornata', 'smartsquare');
    $field['choices']['working-day'] = __('Giornata intera', 'smartsquare');
    $field['choices']['monthly'] = __('Mensile', 'smartsquare');
    return $field;
});
