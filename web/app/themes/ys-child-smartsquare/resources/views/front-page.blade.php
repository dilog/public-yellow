@extends('layouts.app')
@section('content')
  @while(have_posts()) @php the_post() @endphp
    {{--
    //
    // Hero
    //
    --}}
    @include('partials.hero')
    {{--
    //
    // Why SmartSquare?
    //
    --}}
    @include('partials.why')
    {{--
    //
    // Workspaces
    //
    --}}
    @include('partials.workspaces')
    {{--
    //
    // Shooting
    //
    --}}
    @include('partials.shooting')
    {{--
    //
    // Support
    //
    --}}
    @include('partials.support')
    {{--
    //
    // Community
    //
    --}}
    @include('partials.community')
    {{--
    //
    // Prices
    //
    --}}
    @include('partials.prices')
    {{--
    //
    // Services
    //
    --}}
    @include('partials.services')
    {{--
    //
    // Map
    //
    --}}
    @include('partials.map')
    {{--
    //
    // Contact
    //
    --}}
    @include('partials.contact')
  @endwhile
@endsection
