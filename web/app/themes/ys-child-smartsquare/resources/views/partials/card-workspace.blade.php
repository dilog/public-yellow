<div class="{{ $item['class'] }}">
  <div class="card__media">
    @if ($item['gallery'])
      @component('components.carousel-card')
        @foreach ($item['gallery'] as $image)
          @include('partials.slides.card')
        @endforeach
      @endcomponent
    @endif
  </div><!-- /.card__media -->
  <div class="card__main">
    <div class="card__header">
      <h2 class="card__title">{!! $item['title'] !!}</h2>
    </div>
    <div class="card__content">
      <p>{!! $item['content'] !!}</p>
      @if (isset($item['facilities']))
        <ul class="facilities facilities--workspace">
          <li>
            <span class="icon ys-people-number"></span> {{ $item['people'] }} {{ _n('persona', 'persone', $item['people'], 'smartsquare') }}
          </li>
          @foreach ($item['facilities'] as $facility)
            <li>
              <span class="icon ys-{{$facility['value']}}"></span> {{ $facility['label'] }}
            </li>
          @endforeach
        </ul>
      @endif
    </div>
    <div class="card__action">
      <a class="button button--s js-scrollto" href="#workspace-{{ $item['id'] }}">{{ __('Controlla prezzi', 'smartsquare') }}</a>
    </div>
  </div><!-- /.card__main -->
</div><!-- /.card -->
