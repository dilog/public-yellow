<section id="support" class="section section--support">
  @component('components.6-columns')
    <header class="section__header section__header--compact">
      <h2 class="section__title">
        <span class="fancy-underline fancy-underline--green">{{ __('Supporto tecnico', 'smartsquare') }}</span></h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.container')
    <div class="tile tile--light-grey tile--support">
      <div class="tile__media">
      <img srcset="@asset('images/smartsquare/supporto-tecnico-01@1x.jpg'), @asset('images/smartsquare/supporto-tecnico-01@2x.jpg') 2x" src="@asset('images/smartsquare/supporto-tecnico-01@1x.jpg')" alt="" />
      <img srcset="@asset('images/smartsquare/supporto-tecnico-02@1x.jpg'), @asset('images/smartsquare/supporto-tecnico-02@2x.jpg') 2x" src="@asset('images/smartsquare/supporto-tecnico-02@1x.jpg')" alt="" />
      </div>
      <div class="tile__main">
        <p>{{ __('Se hai bisogno di organizzare una video conferenza, registrare il tuo audio o video podcast, gestire il materiale multimediale per le tue lezioni dal vivo nella nostra classe, SmartSquare è a disposizione per il set up più adatto al tuo obiettivo.', 'smartsquare') }}</p>
      </div><!-- /.tile__main -->
      <header class="tile__header">
        <h3 class="tile__title h5">{{ __("Vuoi avere più informazioni sulle nostro materiale tecnico?", 'smartsquare') }}</h3>
      </header><!-- /.tile__header -->
      <div class="tile__footer">
        @if ($tech_support_pdf)
          <a href="{{ $tech_support_pdf['url'] }}" class="button" rel="noopener" target="_blank">{{ __('Scarica la scheda tecnica', 'smartsquare') }}</a>
        @endif
      </div>
    </div>
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section -->
