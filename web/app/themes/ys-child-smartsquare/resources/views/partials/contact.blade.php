<section class="section section--light-grey section--full-width-mobile">
  @component('components.8-columns')
    <header class="section__header">
      <h2 class="section__title">
        <span class="section__title-desc is-hidden-mobile">{{ __('Hai ancora dubbi?', 'smartsquare') }}</span>
        {{ __('Contattaci', 'smartsquare') }}
      </h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.8-columns')
    <form id="js-contact-us" autocomplete="off" class="form form-contact" method="post">
      {!! $nonce !!}
      <input type="checkbox" name="check_email" value="1" style="display:none !important" tabindex="-1" autocomplete="off">
      <div class="form__fieldsets">
        <fieldset class="form__fieldset">
          <div class="form__fieldset-fields">
            <div class="form__row form__row-stacked-mobile">
              <div class="form__field">
                <input name="fname" class="control" placeholder="{{ __('Nome', 'smartsquare') }}" type="text" required />
                </select>
              </div>
              <div class="form__field">
                <input name="lname" class="control" placeholder="{{ __('Cognome', 'smartsquare') }}" type="text" required />
              </div>
            </div><!-- /.form__row -->
            <div class="form__row form__row-stacked-mobile">
              <div class="form__field">
                <input name="phone" class="control" placeholder="{{ __('Telefono', 'smartsquare') }}" type="text" required />
                </select>
              </div>
              <div class="form__field">
                <input name="email" class="control" placeholder="{{ __('Email', 'smartsquare') }}" type="email" required />
              </div>
            </div><!-- /.form__row -->
            <div class="form__row form__row--can-grow">
              <div class="form__field">
                <textarea name="note" class="control" placeholder="{{ __('Informazioni aggiuntive', 'smartsquare') }}"></textarea>
              </div>
            </div><!-- /.form__row -->
            <div class="form__row">
              <div class="form__field">
                <label class="checkbox">
                  <input type="checkbox" class="checkbox__input" value="accepted" name="privacy" required />
                  <span class="checkbox__label">{{ __('Ho letto e accetto la privacy policy', 'smartsquare') }}</span>
                </label>
              </div>
            </div><!-- /.form__row -->
          </div><!-- /.form__fieldset-fields -->
        </fieldset><!-- /.form__fieldset -->
      </div>
      <div class="form__actions">
        <button class="button" type="submit">
          {{ __('Invia', 'smartsquare') }}
        </button>
      </div>
    </form>
  @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section -->
