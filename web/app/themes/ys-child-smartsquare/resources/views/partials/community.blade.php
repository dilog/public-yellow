<section class="community-promo">
  @component('components.container')
    <div class="columns">
      <div class="column is-offset-4">
        <header class="community-promo__header">
          <h2 class="community-promo__title">
            {{ __('Entra nella nostra community con la card "I\'m Yellow" hai diritto a:', 'smartsquare') }}
          </h2>
          <img src="@asset('images/smartsquare/20.svg')" aria-hidden="true" />
        </header><!-- /.community-promo__header -->
      </div>
    </div>
    <div class="columns">
      <div class="community-promo__card">
        <img class="community-promo__card-image" src="@asset('images/smartsquare/fidelity-card@1x.png')" aria-hidden="true" />
      </div>
      <div class="community-promo__content">
        <ul>
          <li>{{ __('50 euro di credito per te da consumare in cibo e bevande allo Yellow Bar se hai sottoscritto un abbonamento con postazione fissa mensile o semestrale nel nostro spazio co-working. Caffè, succhi, panini gourmet, le nostre ciotole di riso e (perché no?!) uno spritz per chiudere la giornata ti aspettano al nostro bar', 'smartsquare') }}</li>
          <li>{{ __('20% di sconto sulle bevande alcoliche allo Yellow Bar', 'smartsquare') }}</li>
          <li>@php(printf(__("15%% di sconto nel nostro ostello <a href='%s'>YellowSquare</a> se siete in cerca di un stanza privata o un posto letto ", 'smartsquare'), 'https://yellowsquare.it'))</li>
        </ul>
        <p>{{ __('La card "I\'m Yellow" ti verrà consegnata al momento della sottoscrizione dell\'abbonamento mensile/semestrale con postazione fissa.', 'smartaquare') }}</p>
      </div>
    </div>
  @endcomponent
</section><!-- /.section -->
