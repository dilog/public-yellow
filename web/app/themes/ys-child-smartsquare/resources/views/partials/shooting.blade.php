<section id="shooting" class="section section--shooting">
  @component('components.6-columns')
    <header class="section__header section__header--compact">
      <h2 class="section__title">
        <span class="fancy-underline fancy-underline--salmon">{{ __('Shooting e Editing', 'smartsquare') }}</span></h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.container')
      <div class="tile tile--violet tile--shoooting">
        <header class="tile__header">
          <div class="tile__media">
            <img srcset="@asset('images/smartsquare/shooting-editing-01@1x.jpg'), @asset('images/smartsquare/shooting-editing-01@2x.jpg') 2x" src="@asset('images/smartsquare/shooting-editing-01@1x.jpg')" alt="" />
            <img srcset="@asset('images/smartsquare/shooting-editing-02@1x.jpg'), @asset('images/smartsquare/shooting-editing-02@2x.jpg') 2x"  src="@asset('images/smartsquare/shooting-editing-02@1x.jpg')" alt="" />
          </div>
          @component('components.8-columns')
            <h3 class="tile__title h5">{{ __("Sei un blogger o uno Youtuber e sei in cerca di un posto dove girare il tuo video?", 'smartsquare') }}<br/>{{ _x('Ti servono delle foto professionali?', 'smartsquare') }}</h3>
          @endcomponent
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ __('Grazie alla YellowSquad, un team di fotografi, videomakers e fonici professionisti avrete tutto il supporto necessario. Grazie alla Sala soundproof c’è spazio per le proprie dirette senza rumori e in un ambiente professionale.', 'smartsquare') }}</p>
        </div><!-- /.tile__main -->
        <div class="tile__footer">
          @if ($shooting_video_pdf)
            <a href="{{ $shooting_video_pdf['url'] }}" class="button" rel="noopener" target="_blank">{{ __('Scopri di più', 'smartsquare') }}</a>
          @endif
        </div>
      </div>
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section -->
