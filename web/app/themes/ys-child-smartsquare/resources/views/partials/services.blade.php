<section id="services" class="section section--full-width-mobile">
  @component('components.8-columns')
    <header class="section__header">
      <h2 class="section__title">
        {{ __('Approfitta dei nostri servizi', 'smartsquare') }}
      </h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.container')
      <ul class="features">
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/wifi.svg')" alt="@php _e('Icona wi-fi', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Wi-Fi', 'smartsquare') @endphp</h3>
        </li>
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/smarttv.svg')" alt="@php _e('Icona Smart Tv', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Smart TV', 'smartsquare') @endphp</h3>
        </li>
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/relax.svg')" alt="@php _e('Icona relax', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Relax Area', 'smartsquare') @endphp</h3>
        </li>
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/printer.svg')" alt="@php _e('Icona stampante', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Stampante', 'smartsquare') @endphp</h3>
        </li>
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/reception.svg')" alt="@php _e('Icona reception', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Reception 24h', 'smartsquare') @endphp</h3>
        </li>
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/whiteboard.svg')" alt="@php _e('Icona lavagna', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Lavagne 24h', 'smartsquare') @endphp</h3>
        </li>
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/lockers.svg')" alt="@php _e('Icona armadietto', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Armadietti', 'smartsquare') @endphp</h3>
        </li>
        <li class="features__item">
          <img class="features__item-media" src="@asset('images/icons/2tone/cooking.svg')" alt="@php _e('Icona cooking area', 'smartsquare-a11y') @endphp" aria-hidden="true" />
          <h3 class="features__item-title">@php _e('Cooking Area', 'smartsquare') @endphp</h3>
        </li>
      </ul>
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section -->
