<footer class="content-info">
  <div class="container">
    @if (has_nav_menu('footer_navigation'))
      <nav class="nav-footer">
        {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav', 'container' => null]) !!}
      </nav>
    @endif
    <div class="colophon">
      <p>Via Palestro, 51 - 00185 Roma {{ __('Telefono', 'smartsquare') }}+39 064463554 - Fax 064463914 home@bc-hospitality.com - {{ __('Partita Iva', 'smartsquare') }} 11209161006
      </p>
    </div>
  </div>
</footer><!-- /.content-info -->
