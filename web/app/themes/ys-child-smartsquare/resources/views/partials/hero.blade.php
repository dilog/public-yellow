<section class="hero hero--smartsquare">
  @component('components.container')
    {!! $hero_image !!}
    <div class="hero__layer">
      <header class="hero__header">
        <div class="hero__title">
          <span class="fancy-underline">{{ _x('SmartSquare?', 'corporate@front-page', 'ys') }}</span><br>
          <span>{!! _x('Un posto dove lavorare,<br> studiare e conoscersi.', 'smartsquare') !!}</span>
        </div><!-- /.hero__title -->
        <div class="hero__subtitle">

        </div><!-- /.hero__subtitle -->
      </header><!-- /.hero__header -->
      <div class="hero__content">
        <div class="hero__box">
          {{ __("Ci piacciono i posti dove puoi trovare il comfort di casa e la creatività di uno studio d’artista, la tecnologia di un ufficio e l'atmosfera di una caffetteria. Così è nato Smart Square.", 'smartsquare') }}
        </div>
      </div><!-- /.hero__content -->
    </div>
  @endcomponent
</section><!-- /.hero -->
