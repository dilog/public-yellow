<section id="prices" class="section section--blue">
  @component('components.6-columns')
    <header class="section__header">
      <h2 class="section__title">
        <span class="fancy-underline fancy-underline--orange">{{ __('Prezzi postazioni', 'smartsquare') }}</span></h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.container')
      @foreach ($workspaces as $item)
        <div id="workspace-{{ $item['id'] }}" class="workspace-prices">
          <div class="workspace-prices__header">
            <h3 class="workspace-prices__header-title">{{ $item['title'] }}</h3>
            <a title="{{ sprintf(__('Prenota %s'), $item['title']) }}" href="{{ $item['form_url'] }}" rel="noopener" class="button button-sm" target="_blank">{{ __('Prenota', 'smartsquare') }}</a>
          </div>
          <div class="workspace-prices__main">
            @if ($item['prices'])
              <div class="workspace-prices__card">
                <ul class="workspace-prices__list">
                @foreach ($item['prices'] as $price)
                  <li class="workspace-prices__list-item">
                    <div class="workspace-prices__price">
                      <span class="workspace-prices__price-label">{{ $price['type'] }}</span>
                      <span class="workspace-prices__price-value">&euro; {{ $price['price'] }}</span>
                      <div class="workspace-prices__price-description">{{ $price['description'] }}</div>
                    </div>
                  </li>
                @endforeach
                </ul>
              </div>
            @endif
          </div>
        </div>
      @endforeach
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section -->
