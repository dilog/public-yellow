<section id="workspaces" class="section section--full-width-mobile section--light-grey">
  @component('components.8-columns')
    <header class="section__header">
      <h1 class="section__title">
        <span class="fancy-underline fancy-underline--violet">{{ __('I nostri spazi', 'smartsquare') }}</span><br/><span class="h2">{{ __('Scegli quello più adatto a te', 'smartsquare') }}</span>
      </h1>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.container')
      @foreach ($workspaces as $item)
        @include('partials.card-workspace')
      @endforeach
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section -->
