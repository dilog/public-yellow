<section id="map" class="contacts">
  <div class="contacts__main">
    <div class="contacts__title">{{ $contacts['title'] }}</div>
    <p class="contacts__address">{{ $contacts['street'] }}<br>{{ $contacts['zip'] }} {{ $contacts['city'] }}<br>{{ $contacts['country'] }}</p>
    <p>{{ __('Telefono:', 'ys') }}<br><span class="whatsapp-phone">+39 06 49382682</span></p>
    <p class="contacts__email">{!! $contacts['email'] !!}</p>
    <p class="is-hidden-tablet">
      <a class="button button--outline" href="#" rel="noopener noreferrer" target="_blank" rel>{{ __('Dimmi di più', 'ys') }}</a>
    </p>
  </div>
  <div class="contacts__map">
    <div id="js-map" class="map" data-lat="{{ $contacts['location']['lat'] }}" data-lng="{{ $contacts['location']['lng'] }}"></div>
  </div>
</section>
