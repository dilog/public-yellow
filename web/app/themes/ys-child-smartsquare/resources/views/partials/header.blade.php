<header id="js-banner" class="banner">
  {{--
    Mobile Nav Toggle
  --}}
  <button id="nav-panel-toggle" class="nav-panel-toggle">
    <span class="nav-panel-toggle__close"></span>
    <span></span>
    <span class="nav-panel-toggle__close"></span>
  </button>
  {{--
    Brand
  --}}
  <a class="brand" href="{{ home_url('/') }}" aria-label="{{ _x('Vai alla Home', 'aria', 'ys') }}">
    <img class="brand__image" src="@asset('images/logo-smartsquare.svg')" alt="Logo Yellow Square" />
  </a>
  {{--
    Primary Nav
  --}}
  @if (has_nav_menu('primary_navigation'))
    <nav class="nav-primary">
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav js-nav']) !!}
    </nav>
  @endif
  {{--
    Mobile Nav Panel
  --}}
  <div id="js-nav-panel" class="nav-panel">
    <div class="nav-panel__block">
      <div class="nav-panel__main">
        <nav class="nav-mobile js-nav">
        @if (has_nav_menu('mobile_navigation'))
          {!! wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'nav nav--vertical', 'container' => false]) !!}
        @endif
        </nav>
      </div>
      <div class="nav-panel__footer">
        @if (has_nav_menu('social_links'))
        {!! wp_nav_menu(['container' => false, 'theme_location' => 'social_links', 'menu_class' => 'nav-social']) !!}
        @endif
      </div>
    </div>
  </div>
</header><!-- /banner -->
