<section id="why" class="section">
  @component('components.6-columns')
    <header class="section__header">
      <h2 class="section__title">{{ __('Perché SmartSquare', 'smartsquare') }}</h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.container')
      <div class="columns">
        <div class="column">
          <div class="callout">
            <img class="callout__media" src="@asset('images/elements/world-in-a-box.svg')" alt="@php _e('Una scatola con dentro il mondo', 'smartsquare-a11y') @endphp" aria-hidden="true" />
            <div class="callout__main">
              <h3 class="callout__title">@php _e('Non ufficio <br>ma una piazza', 'smartsquare') @endphp</h3>
              <p class="callout__content">@php _e('Uno spazio aperto e internazionale, dove lavorare, scambiarsi idee, fare networking, organizzare eventi.', 'smartsquare') @endphp</p>
            </div>
          </div><!-- /.callout -->
        </div><!-- /.column -->
        <div class="column">
          <div class="callout">
            <img class="callout__media" src="@asset('images/elements/good-place.svg')" alt="@php _e('Disegno di una mappa con marker', 'smartsquare-a11y') @endphp" aria-hidden="true" />
            <div class="callout__main">
              <h3 class="callout__title">@php _e('Posizione<br>strategica', 'smartsquare') @endphp</h3>
              <p class="callout__content">@php _e('Vicino alla Stazione Termini, a 500 metri dalla metro A e B (Termini) e alla metro B (Castro Pretorio); vicino a bar, ristoranti e supermercati.', 'smartsquare') @endphp</p>
            </div>
          </div><!-- /.callout -->
        </div>
        <div class="column">
          <div class="callout">
            <img class="callout__media" src="@asset('images/elements/guru.svg')" alt="@php _e('Guru in meditazione', 'smartsquare-a11y') @endphp" aria-hidden="true" />
            <div class="callout__main">
              <h3 class="callout__title">@php _e('No stress', 'smartsquare') @endphp</h3>
              <p class="callout__content">@php _e('Non devi pensare a pagare utenze, affitti o configurazioni, vieni e inizia a lavorare.', 'smartsquare') @endphp</p>
            </div>
          </div><!-- /.callout -->
        </div><!-- /.column -->
      </div><!-- /.columns -->
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section -->
