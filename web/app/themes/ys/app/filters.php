<?php

namespace App;

/**
 * Optimize enqueued script
 */
// add_filter('script_loader_tag', function ($tag, $handle, $src) {
//     $defer = ['google-maps', 'sage/main.js'];
//     if (in_array($handle, $defer)) {
//         return '<script src="' . $src . '" async defer></script>' . "\n";
//     }
//     return $tag;
// }, 10, 3);

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add a class for sharing javascript between templates */
    if (is_home() || is_tax('destination') || is_category()) {
        $classes[] = 'js-blog';
    }

    if (in_array(get_current_blog_id(), config('theme.destinations'))) {
        $classes[0] = $classes[0] . '-destination';
    }

    // SmartSquare site
    if (get_current_blog_id() === config('theme.smartsquare')) {
        $classes[0] = $classes[0] . '-smartsquare';
        $classes[] = 'smartsquare';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    if (strpos($template, 'translation-manager.php')) {
        return $template;
    }
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Lowerize priority for Wordpress SEO metabox
 */
add_filter('wpseo_metabox_prio', function () {
    return 'low';
});

add_filter('acf/load_field/key=field_5de53c3a05ccd', function ($field) {
    $field['choices'] = [];
    $choices = [
        'room-bathroom' => _x('Bagno in stanza', 'Room Facilities', 'ys'),
        'bed-sheets' => _x('Lenzuola', 'Room Facilities', 'ys'),
        'safebox' => _x('Cassaforte personale', 'Room Facilities', 'ys'),
        'towels' => _x('Asciugamano a noleggio', 'Room Facilities', 'ys'),
        'towelsIncluded' => _x('Asciugamano incluso', 'Room Facilities', 'ys'),
        'house-keeping' => _x('Pulizia giornaliera', 'Room Facilities', 'ys'),
        'wifi' => _x('Wi-Fi', 'Room Facilities', 'ys'),
        'air-conditioning' => _x('Aria condizionata', 'Room Facilities', 'ys'),
        'h24' => _x('Reception H24', 'Room Facilities', 'ys'),
        'storage' => _x('Cassettone', 'Room Facilities', 'ys'),
    ];
    if (is_array($choices)) {
        foreach ($choices as $key => $value) {
            $field['choices'][ $key ] = $value;
        }
    }
    return $field;
});
