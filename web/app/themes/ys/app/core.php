<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use Livy\Plumbing\Templates;

/**
 * Wordpress page templates
 */
Templates\register_template_directory('views/page-templates');

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * WP Stage Switcher setup
 */
$envs = [
    'development' => 'https://yellow-square.test',
    'staging'     => 'https://staging-yellowsquare.dev01.it/',
    'production'  => ''
];
define('ENVIRONMENTS', $envs);

/**
 * Limit Post per page on Blog page
 */
add_action('pre_get_posts', function ($query) {
    if (!is_admin() && $query->is_main_query() && is_home()) {
        $query->set('posts_per_page', 8);
        return;
    }
}, 1);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'topbar_navigation' => __('Topbar Navigation', 'sage'),
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'destination_navigation' => __('Destination Navigation', 'sage'),
        'footer_1_navigation' => __('Footer 1 Navigation', 'sage'),
        'footer_2_navigation' => __('Footer 2 Navigation', 'sage'),
        'footer_3_navigation' => __('Footer 3 Navigation', 'sage'),
        'social_links' => __('Social Links', 'sage'),
        'mobile_navigation' => __('Mobile Navigation', 'sage'),
    ]);
}, 20);
