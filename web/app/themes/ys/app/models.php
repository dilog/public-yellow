<?php

namespace App;

function destinations($destination = null)
{
    if (is_null($destination)) {
        throw new \Exception('Destination not set.');
    }

    switch ($destination) {
        case 2:
            return _x('Roma', 'destinations', 'ys');
        case 6:
            return _x('Milano', 'destinations', 'ys');
        default:
            return '';
    }
}

/**
 * Post card data model
 *
 * @param $item
 * @param $classes
 */
function cardPostData($item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    $author_id = get_the_author_meta('ID');
    $class = implode(' ', [...$classes]);
    $data = [
        'author' => [
            'avatar' => get_avatar_url($author_id, [
                'size' => 72,
            ]),
            'fullname' => get_the_author(),
            'shortname' => format_author_name($author_id),
        ],
        'category' => get_primary_category(get_the_id()),
        'class' => join(' ', get_post_class($class)),
        'datetime' => get_post_time('c', true),
        'datetime_hr' => get_the_date(),
        'excerpt' => get_the_excerpt(),
        'id' => get_the_id(),
        'is_big' => in_array('card--big', $classes),
        'patch' => get_field('patch_type'),
        'permalink' => get_permalink(),
        'post_type' => get_post_type(),
        'thumbnail' => responsiveImage(
            get_post_thumbnail_id(),
            config('theme.card_figure_sizes'),
            'card__image'
        ),
        'title' => get_the_title(),
    ];
    return $data;
}

/**
 * Offer card data model
 *
 * @param $item
 * @param $classes
 */
function cardOfferData($destination, $item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    // Get card color and push data as CSS class
    $color = get_field('card_color');
    $color_class = $color ? "card--{$color}" : '';
    $class = implode(' ', [
        ...$classes,
        $color_class,
    ]);

    $data = [
        'class' => join(' ', get_post_class($class)),
        'destination' => destinations($destination),
        'id' => get_the_id(),
        'patch' => get_field('offer_patch'),
        'permalink' => get_permalink(),
        'subtitle' => get_field('subtitle'),
        'title' => get_the_title(),
        'thumbnail' => responsiveImage(
            get_field('offer_thumbnail'),
            config('theme.card_figure_sizes'),
            'card__image'
        ),
        'banner' => responsiveImage(
            get_post_thumbnail_id(),
            config('theme.card_figure_sizes'),
            'card__image'
        ),
        'created_at' => strtotime(get_the_date()),
    ];
    wp_reset_postdata();
    return $data;
}

/**
 * Offer card data model
 *
 * @param $item
 * @param $classes
 */
function cardEatPartyData($destination, $item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    // Get card color and push data as CSS class
    $color = get_field('card_color');
    $color_class = $color ? "card--{$color}" : '';
    $class = implode(' ', [
        ...$classes,
        $color_class,
    ]);

    $data = [
        'class' => join(' ', get_post_class($class)),
        'destination' => destinations($destination),
        'id' => get_the_id(),
        'patch' => get_field('offer_patch'),
        'permalink' => get_permalink(),
        'subtitle' => get_field('card_summary'),
        'title' => get_the_title(),
        'gallery' => get_field('gallery'),
        'post_link_label' => get_field('post_link_label'),
    ];
    wp_reset_postdata();
    return $data;
}

/**
 * Tour card data model
 *
 * @param $item
 * @param $classes
 */
function tourData($destination, $item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    // Get card color and push data as CSS class
    $color = get_field('card_color');
    $color_class = $color ? "card--{$color}" : '';
    $class = implode(' ', [
        ...$classes,
        $color_class,
    ]);

    $category_term = get_the_terms($post->ID, 'tour-cat');

    $data = [
        'class' => join(' ', get_post_class($class)),
        'destination' => destinations($destination),
        'id' => get_the_id(),
        'patch' => get_field('offer_patch'),
        'permalink' => get_permalink(),
        'subtitle' => get_field('card_summary'),
        'title' => get_the_title(),
        'category' => $category_term[0]->name,
        'thumbnail' => get_the_post_thumbnail_url(),
    ];
    wp_reset_postdata();
    return $data;
}

/**
 * Work & Meet data model
 *
 * @param $item
 * @param $classes
 */
function workAndMeetData($destination, $item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    // Get card color and push data as CSS class
    $color = get_field('card_color');
    $color_class = $color ? "card--{$color}" : '';
    $class = implode(' ', [
        ...$classes,
        $color_class,
    ]);

    $gallery = get_field('place_gallery');
    $category_term = get_the_terms($post->ID, 'tour-cat');

    $data = [
        'class' => join(' ', get_post_class($class)),
        'destination' => destinations($destination),
        'id' => get_the_id(),
        'patch' => get_field('tour_patch'),
        'permalink' => get_permalink(),
        'subtitle' => get_field('card_summary'),
        'title' => get_the_title(),
        'category' => $category_term[0]->name,
        'thumbnail' => get_the_post_thumbnail_url(),
        'content' => get_the_content(),
        'place' => get_field('place'),
        'opening_hours' => get_field('opening_hours'),
        'capacity' => get_field('capacity'),
        'action' => array(
            'label' => get_field('button_label'),
            'link' => get_field('button_url'),
        ),
        'facilities' => get_field('place_facilities'),
        'spec_sheet' => get_field('spec_sheet'),
    ];

    if ($gallery) {
        foreach ($gallery as $image_id) {
            $image_src = fly_get_attachment_image_src(
                $image_id,
                [620, 413],
                true
            );
            $data['gallery'][] = [
                'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
                'caption' => wp_get_attachment_caption($image_id),
                'src' => $image_src['src']
            ];
        }
    }
    wp_reset_postdata();
    return $data;
}

/**
 * Event card data model
 *
 * @param $item
 * @param $classes
 */
function cardEventData($item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    // Get event category push data as CSS class
    $category = get_field('event_cat');
    $category_class = $category ? "card--{$category->slug}" : '';
    $class = implode(' ', [
        ...['card', 'card--event', 'h-event'],
        $category_class,
    ]);

    $datetime_start = get_field('event_datetime_start');
    $datetime_end = get_field('event_datetime_end');
    $data = [
        'category' => $category ? $category->name : '-',
        'class' => join(' ', get_post_class($class)),
        'date' => [
            'start' => date_i18n('D, j F', strtotime($datetime_start)),
            'end' => date_i18n('D, j F', strtotime($datetime_end)),
        ],
        'datetime' => [
            'start' => $datetime_start,
            'end' => $datetime_end,
        ],
        'duration' => get_field('event_duration'),
        'url' => get_field('url'),
        'id' => get_the_id(),
        'patch' => get_field('patch_type'),
        'permalink' => get_permalink(),
        'thumbnail' => responsiveImage(
            get_post_thumbnail_id(),
            config('theme.card_figure_sizes'),
            'card__image'
        ),
        'time' => [
            'start' => date_i18n('H:i', strtotime($datetime_start)),
            'end' => date_i18n('H:i', strtotime($datetime_end)),
        ],
        'title' => get_the_title(),
        'content' => apply_filters('the_content', get_the_content()),
    ];
    wp_reset_postdata();
    return $data;
}

/**
 * Idea card data model
 *
 * @param $item
 * @param $classes
 */
function cardIdeaData($key, $item = null, $classes = [])
{
    if (!is_null($item)) {
        global $post;
        $post = $item;
        setup_postdata($post);
    }

    $class = [
        ...['feature', 'feature--idea', 'feature--equal-media'],
        $key % 2 == !0 ? 'feature--reverse' : '',
    ];

    $avatar_id = get_field('author_avatar');
    $name = get_field('author_name');
    $avatar_src = false;
    if ($avatar_id) {
        $avatar_alt = sprintf('%s, %s', __('Avatar', 'ys'), $name);
        $avatar_src = \fly_get_attachment_image_src($avatar_id, [64, 64], true);
        $avatar_src['alt'] = $avatar_alt;
    }

    $data = [
        'author' => [
            'avatar' => $avatar_src,
            'color' => get_field('author_card_color'),
            'country' => get_field('author_country'),
            'name' => $name,
        ],
        'class' => join(' ', get_post_class($class)),
        'destination' => get_field('destination'),
        'id' => get_the_id(),
        'permalink' => get_permalink(),
        'thumbnail' => responsiveImage(
            get_post_thumbnail_id(),
            config('theme.card_figure_sizes'),
            'card__image'
        ),
        'title' => get_the_title(),
        'excerpt' => get_the_excerpt(),
    ];
    wp_reset_postdata();
    return $data;
}
