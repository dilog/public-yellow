<?php

namespace App;

/**
 * Init Whoopss
 */
if (WP_DEBUG) {
    if (!is_admin()) {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
        $whoops->register();
    }
}

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('ys/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('ys/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_localize_script('ys/main.js', 'ysjs', [
        'ajaxurl' => admin_url('admin-ajax.php')
    ]);
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));

    add_post_type_support('page', 'excerpt');

    $palette = collect(config('theme.palette'))->map(function ($value, $key) {
        return [
            'name' => ucfirst($key),
            'slug' => $key,
            'color' => $value,
        ];
    })->values()->all();

    add_theme_support('editor-color-palette', $palette);
}, 20);

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * SMTP setup for PhpMailer
 */
if (WP_ENV !== 'development') {
    add_action('phpmailer_init', function ($phpmailer) {
        $phpmailer->isSMTP();
        $phpmailer->Host = 'smtp-relay.gmail.com';
        $phpmailer->SMTPAuth = false;
        $phpmailer->Port = 587;
        $phpmailer->SMTPSecure = 'tls';
    });
}

add_action('acf/init', function () {
    acf_update_setting('google_api_key', env('GOOGLE_API_KEY'));
});


// add_action( ‘wp_loaded’, ‘myprefix_sendform’, 15 );

if($_REQUEST['submit_group_form'] == 'submit_group_form') {
    $first_name = $_POST['fname'];
    $last_name = $_POST['lname'];
    $email = $_POST['email'];
    $note = $_POST['note'];
    $phone = $_POST['phone'];
    $checkin = $_POST['checkin'];
    $checkout = $_POST['checkout'];
    $destination = $_POST['destination'];
    $group_type = $_POST['group_type'];
    $people = $_POST['people'];

    $message = 'First Name: ' . $first_name . '</br>';
    $message .= 'Last Name: ' . $last_name . '</br>';
    $message .= 'Email: ' . $email . '</br>';
    $message .= 'Phone: ' . $phone . '</br>';
    $message .= 'Destination: ' . $destination . '</br>';
    $message .= 'Check In: ' . $checkin . '</br>';
    $message .= 'Check Out: ' . $checkout . '</br>';
    $message .= 'Group Type: ' . $group_type . '</br>';
    $message .= 'People: ' . $people . '</br>';

    $to = 'gabriele@alfacommunication.it';

    $subject = 'A new form submission on Group Form';

    $headers = "From Yellow Square \r\n";

    mail($to,$subject,$message,$headers);
}