<?php

namespace App\PostType\Offer;

use App;

add_action('init', function () {
    $labels = [
        'singular_name' => __('Offer', 'ys-backend'),
        'menu_name' => __('Offers', 'ys-backend'),
    ];
    $args = [
        'label' => __('Offers', 'ys-backend'),
        'description' => __('Yellow Square Offers', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'excerpt', 'thumbnail', 'author'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => App\is_destinations(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-tag',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => [ 'slug' => 'offer' ]
    ];
    register_post_type('offer', $args);
}, 0);

/**
 * Custom post type columns
 */
add_action('manage_edit-offer_columns', function ($columns) {
    // We don't need published date for offers
    unset($columns['date']);
    $after_title_columns = [
        'code' => __('Coupon Code', 'ys-backend')
    ];
    $columns = array_splice($columns, 0, 2) + $after_title_columns + $columns;
    return $columns;
});

/**
 * Custom post type columns data
 */
add_action('manage_offer_posts_custom_column', function ($column, $post_id) {
    switch ($column) {
        case 'code':
            $code = get_field('coupon_code', $post_id);
            printf('<pre>%s</pre>', $code ?: '-');
            break;
    }
}, 10, 2);
