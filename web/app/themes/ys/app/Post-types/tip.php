<?php

namespace App\PostType\Tip;

use App;

add_action('init', function () {
    $labels = [
        'name' => __('Tips', 'Post Type General Name', 'ys-backend'),
        'singular_name' => __('Tip', 'Post Type Singular Name', 'ys-backend'),
        'menu_name' => __('Tips', 'ys-backend'),
        'search_items' => __('Search Tips', 'ys-backend'),
    ];
    $args = [
        'label' => __('Tips', 'ys-backend'),
        'description' => __('Yellow Square staff tips', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'author'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => App\is_destinations(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-thumbs-up',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => false,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'capability_type' => 'post',
        'rewrite' => false
    ];
    register_post_type('tip', $args);
}, 0);

/**
 * Custom post type columns
 */
add_action('manage_edit-tip_columns', function ($columns) {
    unset($columns['date']);
    return $columns;
});
