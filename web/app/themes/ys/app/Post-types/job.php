<?php

namespace App\PostType\Job;

use App;

add_action('init', function () {
    $labels = [
        'name' => __('Jobs', 'Post Type General Name', 'ys-backend'),
        'singular_name' => __('Job', 'Post Type Singular Name', 'ys-backend'),
        'menu_name' => __('Jobs', 'ys-backend'),
        'search_items' => __('Search Jobs', 'ys-backend'),
    ];
    $args = [
        'label' => __('Job', 'ys-backend'),
        'description' => __('Yellow Square open positions', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'author'],
        'hierarchical' => false,
        'public' => false,
        'show_ui' => App\is_corporate_site(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-nametag',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'capability_type' => 'post',
        'rewrite' => [ 'slug' => 'jobs' ]
    ];
    register_post_type('job', $args);
}, 0);

/**
 * Custom post type columns
 */
add_action('manage_edit-job_columns', function ($columns) {
    unset($columns['date']);
    $columns['title'] = __('Position', 'ys-backend');
    return $columns;
});

/**
 * Custom post type enter title here
 */
add_filter('enter_title_here', function () {
    $screen = get_current_screen();
    $title = '';
    if ('job' == $screen->post_type) {
        $title = __('Type position here', 'ys-backend');
    }
    return $title;
});
