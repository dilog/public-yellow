<?php

namespace App\PostType\Faq;

use App;

add_action('init', function () {
    $labels = [
        'singular_name' => __('FAQ', 'ys-backend'),
        'menu_name' => __('FAQ', 'ys-backend'),
    ];
    $args = [
        'label' => __('FAQ', 'ys-backend'),
        'description' => __('Yellow Square Faq', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'author'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => App\is_corporate_site(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-editor-help',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => [ 'slug' => 'faq' ]
    ];
    register_post_type('faq', $args);
}, 0);

/**
 * Custom post type columns
 */
add_action('manage_edit-faq_columns', function ($columns) {
    unset($columns['date']);
    return $columns;
});

/**
 * Custom post type enter title here
 */
add_filter('enter_title_here', function () {
    $screen = get_current_screen();
    $title = '';
    if ('faq' == $screen->post_type) {
        $title = __('Type question here', 'ys-backend');
    }
    return $title;
});
