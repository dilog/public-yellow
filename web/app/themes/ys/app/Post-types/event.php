<?php

namespace App\PostType\Event;

use App;

add_action('init', function () {
    $labels = [
        'singular_name' => __('Event', 'ys-backend'),
        'menu_name' => __('Events & Tours', 'ys-backend'),
    ];
    $args = [
        'label' => __('Events', 'ys-backend'),
        'description' => __('Yellow Square Events', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'excerpt', 'thumbnail', 'author'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => App\is_destinations(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-tickets-alt',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => [ 'slug' => 'room' ]
    ];
    register_post_type('event', $args);
}, 0);

/**
 * Custom post type columns
 */
add_action('manage_edit-event_columns', function ($columns) {
    unset($columns['date']);
    $before_title_columns = [
        'category' => __('Category', 'ys-backend')
    ];
    $columns = array_splice($columns, 0, 1) + $before_title_columns + $columns;
    return $columns;
});

/**
 * Custom post type columns data
 */
add_action('manage_event_posts_custom_column', function ($column, $post_id) {
    switch ($column) {
        case 'category':
            $category = get_field('event_cat', $post_id);
            printf('<span>%s</span>', $category->name ?: '-');
            break;
    }
}, 10, 2);
