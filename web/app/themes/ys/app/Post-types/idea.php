<?php

namespace App\PostType\Idea;

use App;

add_action('init', function () {
    $labels = [
        'name' => __('Ideas', 'Post Type General Name', 'ys-backend'),
        'singular_name' => __('Idea', 'Post Type Singular Name', 'ys-backend'),
        'menu_name' => __('Ideas', 'ys-backend'),
        'search_items' => __('Search Ideas', 'ys-backend'),
    ];
    $args = [
        'label' => __('Job', 'ys-backend'),
        'description' => __('Yellow Square ideas', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'excerpt', 'editor', 'author'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => App\is_corporate_site(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-lightbulb',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => [ 'slug' => 'responsibility' ]
    ];
    register_post_type('idea', $args);
}, 0);

/**
 * Custom post type columns
 */
add_action('manage_edit-idea_columns', function ($columns) {
    unset($columns['date']);
    return $columns;
});
