<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/**
 * Gutenberg Assets
 */
add_action('enqueue_block_editor_assets', function () {
    wp_enqueue_script(
        'sage/gutenberg.js',
        asset_path('scripts/gutenberg.js')
    );
    wp_enqueue_style(
        'sage/gutenberg.css',
        asset_path('styles/gutenberg.css')
    );
});

/**
 * Customize Dashboard
 */
add_action('wp_dashboard_setup', function () {
    remove_action('welcome_panel', 'wp_welcome_panel');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_activity', 'dashboard', 'normal');
    remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal');
    remove_meta_box('pvc_dashboard', 'dashboard', 'normal');
});

/**
 * Customize Admin menu
 */
add_action('admin_bar_menu', function ($wp_admin_bar) {
    $wp_admin_bar->remove_node('wp-logo');
    $all_nodes = $wp_admin_bar->get_nodes();
    foreach ($all_nodes as $key => $val) {
        $current_node = $all_nodes[$key];
        $wp_admin_bar->remove_node($key);
        if ($key === 'my-sites') {
            $current_node->title = 'YS sites';
        }
        $wp_admin_bar->add_node($current_node);
    }
}, 999);

/**
 * Add theme palette to ACF Color Picker swatches
 */
add_action('acf/input/admin_footer', function () {
    $palette = json_encode(array_values(config('theme.palette')));
    ?>
    <script type="text/javascript">
        (function($) {
            acf.add_filter('color_picker_args', function( args, $field ) {
                args.palettes = <?php echo $palette ?>;
                return args;
            });
        })(jQuery);
    </script>
<?php });

add_action('admin_enqueue_scripts', function () {
    wp_enqueue_style('custom_wp_admin_css', asset_path('styles/admin.css'));
});

add_action('admin_init', [__NAMESPACE__ . '\\NetworkSites', 'init']);
class NetworkSites
{
    const LANG = 'en_EN';

    public static function init()
    {
        $class = __CLASS__;
        new $class;
    }

    public function __construct()
    {
        global $pagenow;
        if ('nav-menus.php' !== $pagenow) {
            return;
        }
        $this->addSomeMetaBox();
    }

    /**
     * Adds the meta box container
     */
    public function addSomeMetaBox()
    {
        add_meta_box(
            'info_meta_box_',
            __('YS Network', self::LANG),
            [$this, 'renderMetaBoxContent'],
            'nav-menus',
            'side',
            'low'
        );
    }

    /**
     * Render Meta Box content
     */
    public function renderMetaBoxContent()
    {
        global $nav_menu_selected_id;
        $walker = new \Walker_Nav_Menu_Checklist();
        $current_tab = 'all';
        $sites = get_sites();

        foreach ($sites as $site) {
            if ($site->path !== '/') {
                $title = ucfirst(str_replace('/', '', $site->path));
            } else {
                $title = 'Corporate';
            }

            $site->classes = [];
            $site->type = 'custom';
            $site->object_id = $site->blog_id;
            $site->title = $title;
            $site->object = 'custom';
            $site->url = $site->domain . $site->path;
            $site->attr_title = str_replace('/', '', $site->path);
        }
        $removed_args = array( 'action', 'customlink-tab', 'edit-menu-item', 'menu-item', 'page-tab', '_wpnonce' );
        $href = esc_url(add_query_arg('networksites-tab', 'all', remove_query_arg($removed_args)))
        ?>
        <div id="networksites" class="categorydiv">
            <ul id="networksites-tabs" class="networksites-tabs add-menu-item-tabs">
                <li <?php echo ('all' == $current_tab ? ' class="tabs"' : ''); ?>>
                    <a class="nav-tab-link" data-type="tabs-panel-authorarchive-all" href="<?php $nav_menu_selected_id ? $href : ''; ?>#tabs-panel-networksites-all">
                    <?php _e('All Sites', 'ys-backend'); ?>
                    </a>
                </li><!-- /.tabs -->
            </ul>
            <div id="tabs-panel-networksites-all" class="tabs-panel tabs-panel-view-all <?php echo ( 'all' == $current_tab ? 'tabs-panel-active' : 'tabs-panel-inactive' ); ?>">
                <ul id="networksites-checklist-all" class="categorychecklist form-no-clear">
                <?php
                    echo walk_nav_menu_tree(array_map('wp_setup_nav_menu_item', $sites), 0, (object) array('walker' => $walker));
                ?>
                </ul>
            </div><!-- /.tabs-panel -->
            <p class="button-controls wp-clearfix">
                <span class="add-to-menu">
                <input type="submit"<?php wp_nav_menu_disabled_check($nav_menu_selected_id); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e('Add to Menu'); ?>" name="add-networksites-menu-item" id="submit-networksites" />
                <span class="spinner"></span>
                </span>
            </p>
        </div>
    <?php }
}
