<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class FrontPage extends Controller
{
    use Partials\Posts;

    public function video()
    {
        return [
            'mp4' => get_field('video_mp4'),
            'webm' => get_field('video_webm'),
        ];
    }

    public function eventsRome()
    {
        switch_to_blog(App\config('theme.destinations.rome'));
        $args = [
            'post_type' => 'event',
            'posts_per_page' => 9,
            'no_found_rows' => true,
            'update_post_term_cache' => true
        ];
        $the_query = new \Wp_Query($args);
        $data = false;
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item) {
                return App\cardEventData($item);
            })->all();
        }
        restore_current_blog();
        return $data;
    }

    public function eventsMilan()
    {
        switch_to_blog(App\config('theme.destinations.milan'));
        $args = [
            'post_type' => 'event',
            'posts_per_page' => 9,
            'no_found_rows' => true,
            'update_post_term_cache' => true
        ];
        $the_query = new \Wp_Query($args);
        $data = false;
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item) {
                return App\cardEventData($item);
            })->all();
        }
        restore_current_blog();
        return $data;
    }

    public function offers()
    {
        $offers = collect();
        foreach ([
            App\config('theme.destinations.rome'),
            App\config('theme.destinations.milan')
        ] as $destination) {
            switch_to_blog($destination);
            $args = [
                'post_type' => 'offer',
                'posts_per_page' => -1,
                'no_found_rows' => true,
                'update_post_term_cache' => false
            ];
            $the_query = new \Wp_Query($args);
            if ($the_query->have_posts()) {
                $destination_offers = collect($the_query->posts)->map(function ($item) use ($destination) {
                    return App\cardOfferData($destination, $item, ['card', 'card--offer']);
                });
                $offers = $offers->merge($destination_offers);
            }
            restore_current_blog();
        }
        $offers = $offers->all();
        usort($offers, function($a, $b) {
            if ($a['created_at'] < $b['created_at']) {
                return 1;
            } else {
                return 0;
            }
        });

        return $offers;
    }

    public function epGallery()
    {
        $upload_dir = wp_upload_dir();
        return [

            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/EP_00_01_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_02_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/EP_00_01_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_02_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/EP_00_02_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/EP_00_02_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/EP_00_03_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_04_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/EP_00_03_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_04_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/EP_00_04_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_03_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/EP_00_04_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_03_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/EP_00_05_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_03_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/EP_00_05_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/EP_00_03_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],

        ];
    }

    public function teGallery()
    {
        return [
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/TE_00_01_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/TE_00_01_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/TE_00_02_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/TE_00_02_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/TE_00_03_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/TE_00_03_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/TE_00_04_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/TE_00_04_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/TE_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Rome'
            ],
        ];
    }

    public function wmGallery()
    {
        return [
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/WM_00_01_442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/WM_00_01_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Roma'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/WM_00_02_442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/WM_00_02_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Roma'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/WM_00_03_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/WM_00_03_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Roma'
            ],
            [
                'src' => [
                    [
                        'mobile' => [
                            '1x' => '/app/uploads/2021/05/WM_00_04_1442x980.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_1442x980@2x.jpg'),
                        ],
                        'desktop' => [
                            '1x' => '/app/uploads/2021/05/WM_00_04_880x1080.jpg',
                            //'2x' => App\asset_path('images/corporate/home/WM_00_01_880x1080@2x.jpg'),
                        ],
                    ]
                ],
                'caption' => 'Roma'
            ],

        ];
    }
}
