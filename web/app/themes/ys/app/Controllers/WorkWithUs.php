<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class WorkWithUs extends Controller
{
    public function jobs()
    {
        $args = [
            'post_type' => 'job',
            'posts_per_page' => -1,
            'no_found_rows' => true,
            'update_post_term_cache' => false
        ];
        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item) {
                global $post;
                $post = $item;
                setup_postdata($post);
                return [
                    'title' => get_the_title(),
                    'content' => apply_filters('the_content', get_the_content()),
                    'destination' => get_field('destination')->name
                ];
            })->all();
            wp_reset_postdata();
            return $data;
        }
        return false;
    }
}
