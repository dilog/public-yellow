<?php

namespace App\Controllers;

use App as Config;
use Sober\Controller\Controller;

class App extends Controller
{
    use Partials\Common;

    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            return __('Blog', 'ys');
        }
        if (is_tax('destination') || is_category()) {
            return __('Blog', 'ys');
        }
        if (is_tag()) {
            return single_term_title('', false);
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function subtitle()
    {
        if (is_home()
        || is_tax('destination')
        || is_category()
        || is_tag()
        ) {
            return get_the_excerpt(get_option('page_for_posts'));
        }
        return get_the_excerpt();
    }

    public function sites()
    {
        return [
            'rome' => get_home_url(Config\config('theme.destinations.rome')),
            'milan' => get_home_url(Config\config('theme.destinations.milan')),
        ];
    }
}
