<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class Faq extends Controller
{
    public function __construct()
    {
        $args = [
            'post_type' => 'faq',
            'posts_per_page' => -1,
            'no_found_rows' => true,
            'update_post_term_cache' => false
        ];
        $the_query = new \Wp_Query($args);
        $this->faqs = $the_query->posts;
    }

    public function categories()
    {
        $terms = get_terms([
            'taxonomy' => 'faq-cat',
            'hide_empty' => false,
        ]);

        if (!is_wp_error($terms) && !empty($terms)) {
            return collect($terms)->map(function ($term, $key) {
                return [
                    'id' => $term->term_id,
                    'class' => '',
                    'name' => $term->name,
                ];
            })->prepend([
                'id' => 0,
                'class' => 'nav__item--active',
                'name' => __('Tutto', 'ys'),
            ])->all();
        }

        return false;
    }

    public function groups()
    {
        if (!empty($this->faqs)) {
            $data = collect($this->faqs)
            ->map(function ($item) {
                global $post;
                $post = $item;
                setup_postdata($post);
                return [
                    'id' => get_the_id(),
                    'anchor' => 'faq-' . get_the_id(),
                    'title' => get_the_title(),
                    'category' => get_field('faq_cat'),
                    'content' => apply_filters('the_content', get_the_content()),
                ];
            })
            ->sortBy('category.name')
            ->groupBy('category.term_id')->map(function ($group) {
                $title = $group[0]['category']->name;
                return [
                    'title' => $title,
                    'items' => $group
                ];
            })->all();
            wp_reset_postdata();
            return $data;
        }
        return false;
    }

    public function faqIndex()
    {
        if (!empty($this->faqs)) {
            $data = collect($this->faqs)->map(function ($item) {
                global $post;
                $post = $item;
                setup_postdata($post);
                return [
                    'id' => '#faq-' . get_the_id(),
                    'title' => strtolower(get_the_title()),
                ];
            })->toJson();
            wp_reset_postdata();
            return $data;
        }
    }

    public static function getFaqs()
    {
        $id = intval($_POST['id']);

        $args = [
            'post_type' => 'faq',
            'posts_per_page' => -1,
            'no_found_rows' => true,
            'update_post_term_cache' => false
        ];

        if ($id > 0) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'faq-cat',
                    'terms' => $id

                ],
            ];
        }

        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $index = [];
            $groups = collect($the_query->posts)
            ->map(function ($item) use (&$index) {
                global $post;
                $post = $item;
                setup_postdata($post);

                $index[] = [
                    'id' => '#faq-' . get_the_id(),
                    'title' => strtolower(get_the_title()),
                ];

                return [
                    'id' => get_the_id(),
                    'anchor' => 'faq-' . get_the_id(),
                    'title' => get_the_title(),
                    'category' => get_field('faq_cat'),
                    'content' => apply_filters('the_content', get_the_content()),
                ];
            })
            ->sortBy('category.name')
            ->groupBy('category.term_id')->map(function ($group) {
                $title = $group[0]['category']->name;
                return [
                    'title' => $title,
                    'items' => $group
                ];
            })->all();
            wp_reset_postdata();
            $data = App\template('partials.faq-group', ['groups' => $groups]);
            wp_send_json_success(['results' => $data, 'index' => $index]);
        }
        wp_send_json_error();
    }
}
