<?php

namespace App\Controllers;

use App;
use Illuminate\Support\Facades\Config;
use Sober\Controller\Controller;

class EatAndParty extends Controller
{
    public function eventsRome()
    {
        switch_to_blog(App\config('theme.destinations.rome'));
        $args = [
            'post_type' => 'event',
            'posts_per_page' => 9,
            'no_found_rows' => true,
            'update_post_term_cache' => true
        ];
        $the_query = new \Wp_Query($args);
        $data = false;
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item) {
                return App\cardEventData($item);
            })->all();
        }
        restore_current_blog();
        return $data;
    }

    public function eventsMilan()
    {
        switch_to_blog(App\config('theme.destinations.milan'));
        $args = [
            'post_type' => 'event',
            'posts_per_page' => 9,
            'no_found_rows' => true,
            'update_post_term_cache' => true
        ];
        $the_query = new \Wp_Query($args);
        $data = false;
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item) {
                return App\cardEventData($item);
            })->all();
        }
        restore_current_blog();
        return $data;
    }
}
