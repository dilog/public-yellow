<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class Responsibility extends Controller
{
    public function ideas()
    {
        $args = [
            'post_type' => 'idea',
            'posts_per_page' => -1,
            'no_found_rows' => true,
            'update_post_term_cache' => true
        ];
        $the_query = new \Wp_Query($args);
        $data = false;
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item, $key) {
                return App\cardIdeaData($key, $item);
            })->all();
        }
        return $data;
    }
}
