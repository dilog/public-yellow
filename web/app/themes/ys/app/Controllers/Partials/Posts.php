<?php

namespace App\Controllers\Partials;

use App;

trait Posts
{
    public function recentPosts()
    {
        $data = [];
        $args = [
            'post_type' => 'post',
            'posts_per_page' => 6,
        ];
        $the_query = new \WP_Query($args);
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                array_push($data, App\cardPostData(null, ['card', 'card--post', 'card--yellow']));
            }
            wp_reset_postdata();
            return $data;
        }
        return false;
    }
}
