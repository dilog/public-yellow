<?php

namespace App\Controllers\Partials;

use App;

trait Blog
{
    public static function injectCardClass($key)
    {
        $class = [];

        switch ($key) {
            case 0:
                $class = ['card--big'];
                break;
            case in_array($key, range(1, 4)):
                $class = ['card--horz'];
                break;
            case $key >= 5:
                $class = ['card--horz-on-mobile'];
        }

        return $class;
    }

    /**
     * First Tab: main query
     */
    public function blogPosts()
    {
        global $wp_query;
        return collect($wp_query->posts)->map(function ($post, $key) {
            $class = Blog::injectCardClass($key);
            return App\cardPostData(
                $post,
                [
                    'card',
                    'card--post',
                    ...$class
                ]
            );
        })->all();
    }

    /**
     * 2th Tab: trending posts
     */
    public function trendingPosts()
    {
        $args = [
            'post_type' => 'post',
            'posts_per_page' => -1,
            'orderby' => 'post_views',
            'no_found_rows' => true,
            'update_post_term_cache' => true,
        ];
        $the_query = new \WP_Query($args);
        if ($the_query->have_posts()) {
            return collect($the_query->posts)->map(function ($post, $key) {
                $class = Blog::injectCardClass($key);
                return App\cardPostData(
                    $post,
                    [
                        'card',
                        'card--post',
                        ...$class
                    ]
                );
            })->all();
        }
        return false;
    }

    /**
     * 3th Tab: best of
     */
    public function bestOfPosts()
    {
        $ids = get_field('blog_best_of', get_option('page_for_posts'));
        $args = [
            'post_type' => 'post',
            'posts_per_page' => -1,
            'post__in' => $ids,
            'orderby' => 'post__in',
            'ignore_sticky_posts' => true,
            'no_found_rows' => true,
            'update_post_term_cache' => true
        ];
        $the_query = new \WP_Query($args);
        if ($the_query->have_posts()) {
            return collect($the_query->posts)->map(function ($post, $key) {
                $class = Blog::injectCardClass($key);
                return App\cardPostData(
                    $post,
                    [
                        'card',
                        'card--post',
                        ...$class
                    ]
                );
            })->all();
        }
        return false;
    }

    /**
     * Destination Tax select options
     * @return array
     */
    public function destination()
    {
        $selected = null;

        $options = collect(get_terms([
            'fields' => 'id=>name',
            'hide_empty' => false,
            'taxonomy' => 'destination',
        ]))->map(function ($item, $key) {
            return [
                'id' => $key,
                'name' => $item,
                'value' => $key
            ];
        })->all();

        if (is_tax('destination')) {
            $selected = get_queried_object()->term_id;
        }

        return [
            'options' => $options,
            'label' => __('Tutte le città', 'ys'),
            'selected' => $selected,
            'value' => -1
        ];
    }

    /**
     * Category Tax select options
     * @return array
     */
    public function category()
    {
        $selected = null;

        $options = collect(get_terms([
            'fields' => 'id=>name',
            'hide_empty' => false,
            'taxonomy' => 'category',
        ]))->map(function ($item, $key) {
            return [
                'id' => $key,
                'name' => $item,
                'value' => $key
            ];
        })->all();

        if (is_category()) {
            $selected = get_queried_object()->term_id;
        }

        return [
            'options' => $options,
            'label' => __('Tutte le categorie', 'ys'),
            'selected' => $selected,
            'value' => -1
        ];
    }

    /**
     * Blog authors query
     * @return array
     */
    public function authors()
    {
        $args = array (
            'blog_id' => 0,
            'exclude_admin' => false,
            'fields' => ['id', 'user_email'],
            'role' => ['author'],
        );
        return collect(get_users($args))->map(function ($user) {
            $first_name = get_user_meta($user->id, 'first_name', true);
            $last_name = get_user_meta($user->id, 'last_name', true);
            return [
                'avatar' => [
                    'alt' => sprintf('%s %s avatar', $first_name, $last_name),
                    'thumb' => get_avatar_url($user->user_email),
                ],
                'description' => get_user_meta($user->id, 'description', true),
                'first_name' => $first_name,
                'fullname' => "$first_name $last_name",
                'last_name' => $last_name,
            ];
        })->all();
    }
}
