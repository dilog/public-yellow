<?php

namespace App;

use Exception;
use Illuminate\Container\EntryNotFoundException;
use Roots\Sage\Container;
use Roots\Sage\Config;

function __log($msg)
{
    error_log(print_r($msg, 1));
}

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                    ];
                })
                ->concat([
                    "{$template}.blade.php",
                    "{$template}.php",
                ]);
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

/**
 * Corporate site check
 *
 * @return bool
 */
function is_corporate_site()
{
    return is_main_site();
}

/**
 * Rome site check
 *
 * @return bool
 */
function is_rome_site()
{
    $current_site = get_current_blog_id();
    //return $current_site;
    if ($current_site === env('SITE_ROME')) {
        return true;
    }
    return false;
}

/**
 * Milan site check
 *
 * @return bool
 */
function is_milan_site()
{
    $current_site = get_current_blog_id();
    if ($current_site === env('SITE_MILAN')) {
        return true;
    }
    return false;
}

/**
 * Destinations site check
 *
 * @return bool
 */
function is_destinations()
{
    $check_array = config('theme.destinations');

    $current_site = get_current_blog_id();
    if (in_array($current_site, $check_array)) {
        return true;
    }
    return false;
}

/**
 * Responsive image generator
 *
 * @param int $id
 * @param array $sizes
 * @param string $class
 * @return null|string
 */
function responsiveImage($id, $sizes, $class = '')
{
    if (empty($id)) {
        return false;
    }

    $data = [];

    foreach ($sizes as $size) {
        $image_src = fly_get_attachment_image_src(
            $id,
            $size['size'],
            true
        );
        $size['src'] = $image_src['src'];
        $data[] = $size;
    }

    $lqip_src = fly_get_attachment_image_src(
        $id,
        [100, 67],
        true
    );
    $lqip = [
        'src' => $lqip_src['src']
    ];

    return Template(
        'partials.picture',
        [
            'alt' => get_post_meta($id, '_wp_attachment_image_alt', true),
            'class' => $class,
            'data' => $data,
            'placeholder' => $lqip
        ]
    );
}

/**
 * Get Primary category
 *
 * @param int|null $post_id
 * @param string $taxonomy
 * @return array
 * @throws Exception
 */
function get_primary_category($post_id = null, $taxonomy = 'category')
{
    if (is_null($post_id)) {
        throw new \Exception('Missing Post ID.');
    }

    $category = get_the_category($post_id);
    $primary_category = [];
    if ($category) {
        $category_display = '';
        $category_slug = '';
        $category_link = '';
        $category_id = '';

        if (class_exists('WPSEO_Primary_Term')) {
            $wpseo_primary_term = new \WPSEO_Primary_Term($taxonomy, $post_id);
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term = get_term($wpseo_primary_term);
            if (is_wp_error($term)) {
                $category_display = $category[0]->name;
                $category_slug = $category[0]->slug;
                $category_link = get_category_link($category[0]->term_id);
                $category_id = $category[0]->term_id;
            } else {
                $category_display = $term->name;
                $category_slug = $term->slug;
                $category_link = get_category_link($term->term_id);
                $category_id = $term->term_id;
            }
        } else {
            $category_display = $category[0]->name;
            $category_slug = $category[0]->slug;
            $category_link = get_category_link($category[0]->term_id);
            $category_id = $category[0]->term_id;
        }

        $primary_category['url'] = $category_link;
        $primary_category['slug'] = $category_slug;
        $primary_category['title'] = $category_display;
        $primary_category['id'] = $category_id;
    }
    return $primary_category;
}

/**
 * Format the author full name
 *
 * @param mixed $id - Author ID
 * @return false|string
 */
function format_author_name($id, $type = 'short')
{
    if (is_null($id)) {
        throw new \Exception('Missing Author ID.');
    }

    $first_name = get_the_author_meta('first_name', $id);
    $last_name = get_the_author_meta('last_name', $id);

    return sprintf(
        '%s %s',
        $first_name,
        (!empty($last_name) && $type === 'short')
            ? substr($last_name, 0, 1) . '.'
            : $last_name
    );
}

/**
 * Get YouTube video ID from URL
 *
 * @param string $url
 * @return string Youtube video id or FALSE if none found.
 */
function youtube_id_from_url($url)
{
    $pattern =
        '%^# Match any youtube URL
        (?:https?://)?      # Optional scheme. Either http or https
        (?:www\.)?          # Optional www subdomain
        (?:                 # Group host alternatives
            youtu\.be/      # Either youtu.be,
            | youtube\.com  # or youtube.com
        (?:                 # Group path alternatives
            /embed/         # Either /embed/
            | /v/           # or /v/
            | /watch\?v=    # or /watch\?v=
        )                   # End path alternatives.
        )                   # End host alternatives.
        ([\w-]{10,12})      # Allow 10-12 for 11 char youtube id.
        $%x'
        ;
    $result = preg_match($pattern, $url, $matches);
    if ($result) {
        return $matches[1];
    }
    return false;
}
