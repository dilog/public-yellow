<?php

namespace App\Taxonomy\Destination;

add_action('init', function () {
    $labels = array(
        'name' => _x('Destinations', 'Taxonomy General Name', 'ys-backend'),
        'singular_name' => _x('Destination', 'Taxonomy Singular Name', 'ys-backend'),
        'add_new_item' => __('Add new destination', 'ys-backend')
    );

    $args = [
        'labels' => $labels,
        'hierarchical' => false,
        'public' => false,
        'publicly_queryable' => true,
        'rewrite' => ['slug' => 'destinazione'],
        'show_ui' => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'show_tagcloud' => false,
        'show_in_quick_edit' => true,
    ];

    register_taxonomy('destination', ['job', 'post', 'idea'], $args);
}, 0);

/**
 * Disable display of Gutenberg Post Setting UI for a specific
 * taxonomy. While this isn't the official API for this need,
 * it works for now because only Gutenberg is dependent on the
 * REST API response.
 *
 * @link https://github.com/WordPress/gutenberg/issues/6912
 */
add_filter('rest_prepare_taxonomy', function ($response, $taxonomy) {
    if ('destination' === $taxonomy->name) {
        $response->data['visibility']['show_ui'] = false;
    }
    return $response;
}, 10, 2);
