<?php

namespace App\Taxonomy\FAQCat;

add_action('init', function () {
    $labels = array(
        'name' => _x('Categories', 'Taxonomy General Name', 'ys-backend'),
        'singular_name' => _x('Category', 'Taxonomy Singular Name', 'ys-backend'),
        'add_new_item' => __('Add new category', 'ys-backend')
    );

    $args = [
        'labels' => $labels,
        'hierarchical' => false,
        'public' => false,
        'show_ui' => true,
        'show_admin_column' => false,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'show_tagcloud' => false,
        'show_in_quick_edit' => false
    ];

    register_taxonomy('faq-cat', ['faq'], $args);
}, 0);
