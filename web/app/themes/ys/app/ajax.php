<?php

namespace App;

add_action("wp_ajax_get_faqs", [\App\Controllers\Faq::class, 'getFaqs']);
add_action("wp_ajax_nopriv_get_faqs", [\App\Controllers\Faq::class, 'getFaqs']);

add_action("wp_ajax_filtered_posts", __NAMESPACE__ . "\\load_filtered_posts");
add_action("wp_ajax_nopriv_filtered_posts", __NAMESPACE__ . "\\load_filtered_posts");

function load_filtered_posts()
{
    $filters = $_POST["filters"];
    $filters = array_map('intval', $filters);
    $tax_query = [];

    $args = [
        'post_type' => 'post',
        'posts_per_page' => 8,
    ];

    if (isset($filters['destination'])) {
        if ($filters['destination'] === -1) {
            unset($tax_query['destination']);
        } else {
            $tax_query['destination'] = [
                'taxonomy' => 'destination',
                'terms'    => $filters['destination'],
            ];
        }
    }

    if (isset($filters['category']) && $filters['category'] !== -1) {
        $tax_query['category'] = [
            'taxonomy' => 'category',
            'terms'    => $filters['category'],
        ];
    }

    if (!empty($tax_query)) {
        $args['tax_query'] = array_values($tax_query);
    }

    $the_query = new \WP_Query($args);

    if ($the_query->have_posts()) {
        $data = collect($the_query->posts)->map(function ($post, $key) {
            $class = [];

            switch ($key) {
                case 0:
                    $class = ['card--big'];
                    break;
                case in_array($key, range(1, 4)):
                    $class = ['card--horz'];
                    break;
                case $key >= 5:
                    $class = ['card--horz-on-mobile'];
            }

            $card_data = cardPostData(
                $post,
                [
                    'card',
                    'card--post',
                    ...$class
                ]
            );

            return template('partials.cards.card-post', ['item' => $card_data]);
        })->all();
        wp_send_json_success($data);
    }

    wp_send_json_error();
}
