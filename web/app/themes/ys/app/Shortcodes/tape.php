<?php

namespace App\Shortcode\Tape;

add_shortcode('tape', function ($atts, $content = null) {
    extract(shortcode_atts([
        'color' => false,
    ], $atts));
    $classes = implode(' ', ['tape', $color ? 'tape--' . $color : '' ]);
    return sprintf('<span class="%s">%s</span>', $classes, $content);
});
