import {cardCarousel, gtkCarousel, roomCarousel} from '../carousels';

export default {
  init() {
    // Init Card carousel
    cardCarousel();

    // Init Good To Know carousel
    roomCarousel();

    // Init Good To Know carousel
    gtkCarousel();

    console.log('Work');
  },
};
