import flatpickr from 'flatpickr';
import {addDays} from 'date-fns'

import {destinationsCarousel} from '../carousels';

export default {
  init() {
    destinationsCarousel();

    flatpickr('#checkin', {
      minDate: new Date(),
      static: true,
      showMonths: 2,
      onValueUpdate: function(selectedDates, dateStr) {
       checkout.set('minDate', addDays(new Date(dateStr), 1));
      },
    });

    const checkout = flatpickr('#checkout', {
      minDate: new Date(),
      static: true,
      showMonths: 2,
    });

    $('.js-stepper').on('click', 'button', function(){
      const field = $(this).parent().find('input');
      const max = parseInt(field.attr('max'));
      const min = parseInt(field.attr('min'));
      const value = parseInt(field.val());
      const step = parseInt($(this).attr('data-step'));

      if (step > 0 && (value >= max)) {
        return false;
      }

      if (step < 0 && (value < min)) {
        return false;
      }

      field.val(value + step);
    });
  },
  finalize() {},
};
