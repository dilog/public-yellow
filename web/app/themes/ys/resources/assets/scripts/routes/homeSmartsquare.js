import {cardCarousel} from '../carousels';
import * as markerimage from '../../images/icons/marker.svg';

export default {
  init() {
    /**
     * Init carousels
     */
    cardCarousel()

    /**
     * Contact form
     */
    $('#js-contact-us').submit(function(e) {
      e.preventDefault();
      const $form = $(this);
      const $button = $(this).find('button');
      $button.prop('disabled', true);
      $button.addClass('button--working');
      let data = $form.serializeArray();
      data[data.length] = {
        name: 'action',
        value: 'send_contact_form',
      };

      $.ajax({
        type: 'post',
        url: ysjs.ajaxurl,
        data: data,
        dataType: 'json',
        complete: function() {
          $form.get(0).reset();
          $button.removeClass('button--working');
          $button.prop('disabled', false);
        },
      });
    })

    /**
     * Google Map init
     */
    const $map = $('#js-map');
    const lat = parseFloat($map.attr('data-lat'));
    const lng = parseFloat($map.attr('data-lng'));
    const styles = [
      {
        featureType: 'poi.business',
        stylers: [{visibility: 'off'}],
      },
    ];

    const icon = {
        url: markerimage,
        scaledSize: new window.google.maps.Size(29, 44),
        origin: new window.google.maps.Point(0,0),
        anchor: new window.google.maps.Point(0, 0),
    };

    const map = new window.google.maps.Map($map.get(0), {
      center: {lat, lng},
      zoom: 16,
      styles: styles,
      mapTypeControl: false,
    });

    new window.google.maps.Marker({
      position: {lat, lng},
      map: map,
      icon,
    });
  },
  finalize() {},
};
