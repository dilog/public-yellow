import {namespacedCarousel} from '../carousels';

export default {
  init() {
    namespacedCarousel();
  },
};
