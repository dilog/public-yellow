import stickybits from 'stickybits';
import LazyLoad from 'vanilla-lazyload';
import '@fancyapps/fancybox'; //

import tabs from '../tabs';
import Animations from '../animations';

export default {
  init() {
    /** Nav dropdown sub menu */
    $('.js-nav .menu-item-has-children > a').on('click', function(e) {
      e.preventDefault();
      $(this).parent().toggleClass('sub-manu-is-visible');
    } );

    /** Patch animations init */
    Animations();

    /** Sticky Banner */
    stickybits('#js-banner', {
      useStickyClasses: true,
    });

    /** Lazy load images */
    new LazyLoad({
      elements_selector: '.js-lazy',
    });

    /** Init Tabs */
    tabs();

    /** Handle Mobile Navigation panel */
    (function($){
      $('#nav-panel-toggle').on('click', function(){
        const $banner = $('#js-banner');
        const windowH = window.innerHeight;
        const bannerH = $banner.height();
        const bookNow = $('#js-book-now').height();
        $('#js-nav-panel').css({
          height: windowH - (bannerH + bookNow),
          top: $banner.outerHeight(),
        });
        $banner.toggleClass('banner--nav-panel-visible');
      })
    })(jQuery);

    $('.js-nav .menu-item-has-children').on('click', 'a', function(){
      $(this).parent().toggleClass('menu-item-active');
    });

    /**
     * Handle Open Slide Menu
     */

     (function($){
        const jsBookNow = $('[data-open="book-now"]');
        const closePanelDiv = $('.book-now-menu__close-panel');
        const closePanelButton = $('.book-now-menu__close-button');

        jsBookNow.on('click', openSlidePanel);

        closePanelDiv.on('click', closeSlidePanel);
        closePanelButton.on('click', closeSlidePanel);

        function openSlidePanel(event) {
          event.preventDefault();
          event.stopPropagation();

          const bookNowMenu = $('.book-now-menu');

          $('body').addClass('slide-panel-open');
          bookNowMenu.addClass('open');

          setTimeout(function() {
            bookNowMenu.addClass('slide');
          }, 10);

        }

        function closeSlidePanel(event) {
          event.preventDefault();
          event.stopPropagation();

          const bookNowMenu = $('.book-now-menu');

          bookNowMenu.removeClass('slide');

          setTimeout(function() {
            $('body').removeClass('slide-panel-open');
            bookNowMenu.removeClass('open');
          }, 300);


        }

        /**
         * Booking Form
         */

        const bookingForms = $('.booking__form');

        bookingForms.each(function() {
          $(this).on('submit', function(event) {
            event.preventDefault();

            let coupon = $(this).closest('.booking').find('[name="coupon"]');
            let destination = '';

            if (!coupon.length) {
              coupon = $(this).find('[name="coupon"]');
            }

            if ($(this).find('[name="booking_destination"]').val()=='rome') {
              destination = '4538';
            }

            if ($(this).find('[name="booking_destination"]').val()=='milan') {
              destination = '11266';
            }

            if ($(this).find('[name="booking_destination"]').val()=='florence') {
              destination = '';
            }

            const urlParam = `?product_search[arrival]=${$(this).find('[name="hidden-checkin"]').val()}&product_search[departure]=${$(this).find('[name="hidden-checkout"]').val()}&product_search[bonus_code]=${coupon.val()}`;
            window.open(`https://sky-eu1.clock-software.com/18025/${destination}/wbe/en/products${encodeURI(urlParam)}`, '_blank').focus();
          });
        });
      })(jQuery);

    /**
     * Dropdown menu
     */
    $('.js-dropdown button').on('click', function() {
      const $dropdown = $(this).parents('.js-dropdown');
      $dropdown.toggleClass('dropdown--is-active');
    });

    $('.js-dropdown a').on('click', function() {
      const $dropdown = $(this).parents('.js-dropdown');
      const label = $(this).text();
      $dropdown.find('.dropdown__button-label').text(label);
      $dropdown.toggleClass('dropdown--is-active');
    });

    /**
     * Footer dropdown menu
     * - Visible only on mobile
     */
    $('.js-dropdown-footer').on('click', function(){
      $(this).parent().toggleClass('footer-nav--active');
    })

    $('details').on('toggle', function() {
      if (this.open) {
        $(this).addClass('details--open');
      } else {
        $(this).removeClass('details--open');
      }
    });

    /**
     * Scroll To
     */
    $('.js-scrollto').on('click', function(e) {
      e.preventDefault();
      let $link = $(this);

      if (!$(this).is('a')) {
        $link = $(this).find('a');
      }

      const id = $link.attr('href');

      $('html').animate({
        scrollTop: $(id).offset().top - 100,
      }, 100, 'linear')
    });
  },
};
