import {cardCarousel, namespacedCarousel} from '../carousels';

export default {
  init() {
    cardCarousel();
    namespacedCarousel();
  },
};
