import {fullCarousel} from '../carousels';

export default {
  init() {
    fullCarousel();
  },
};
