import {namespacedCarousel, cardCarousel} from '../carousels';

export default {
  init() {
    cardCarousel();
    namespacedCarousel();
    console.log('tour');
  },
};
