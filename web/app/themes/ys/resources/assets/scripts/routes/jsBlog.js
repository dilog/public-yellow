import {authorsCarousel} from '../carousels';
import loader from '../loader';

export default {
  init() {
    // Posts filters
    const filters = {};
    $('#js-posts-filters').on('change', 'select', function(e) {
      e.preventDefault();
      loader.show();
      const term_id = e.target.value;
      const filter = $(this).attr('data-filter');
      filters[filter] = term_id;
      $.ajax({
        type : 'post',
        dataType : 'json',
        url : window.ysjs.ajaxurl,
        data : {action: 'filtered_posts', filters},
        success: function(response) {
          if(response.success) {
            $('#js-grid-blog').html(response.data);
            loader.hide();
          }
        },
      })
    });

    // Init carousels
    authorsCarousel();

    $('#js-tabs-blog').on('ys:tabs:show', function(e, param){
      e.stopPropagation();
      if(param.selected === '#recent-posts') {
        $('#js-posts-filters').find('select').prop('disabled', false);
      } else {
        $('#js-posts-filters').find('select').prop('disabled', true);
      }
    })
  },
  finalize() {},
};
