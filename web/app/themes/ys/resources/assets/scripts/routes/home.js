import booking from '../booking';
import {destinationsCarousel, cardCarousel, fullCarousel} from '../carousels';

export default {
  init() {
    booking();
    destinationsCarousel();
    cardCarousel();
    fullCarousel();
  },
};
