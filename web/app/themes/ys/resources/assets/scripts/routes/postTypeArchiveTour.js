import loader from '../loader';

export default {
  init() {
    // Nav
    $('#js-categories').on('change', 'input', function(ev) {
      // ev.preventDefault();
      const parentForm = $(this).closest('form');
      const checkboxes = parentForm.find('[type="checkbox"]:checked');
      const clickedValue = this.value;

      if (parentForm.data('reset') == 'reset') {
        ev.preventDefault();
        checkboxes.not($(this)).each(function() {
          if (clickedValue != this.value) {
            $(this).attr('checked', false);
          }
        });
        parentForm.data('reset', '');
        $(this).prop('checked', true);
        $(this).trigger('change');
        return;
      }
      if (checkboxes.length == parentForm.find('[type="checkbox"]').length) {
        parentForm.data('reset', '');
      }
      runAjax(ev.target);
    });

    function runAjax(item) {
      const idObj = $(item).closest('form').serializeArray();
      let ids = [];
      for (let item in idObj) {
        ids.push(Number(idObj[item]['value']));
      }
      ids = ids.join(',');
      loader.show();
      $.ajax({
        type : 'post',
        dataType : 'json',
        url : window.ysjs.ajaxurl,
        data : {action: 'get_tours', ids},
        success: function(response) {
          if(response.success && typeof response.data.results == 'string') {
            loader.hide();
            $('#filtered-posts').html(response.data.results);
            window.faqIndex = response.data.index;
          } else {
            loader.hide();
            $('#filtered-posts').html('');
          }
        },
      });
    }
  },
};
