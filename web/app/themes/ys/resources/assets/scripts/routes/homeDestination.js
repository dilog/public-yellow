import booking from '../booking';
import {namespacedCarousel, gtkCarousel, cardCarousel,fullCarousel} from '../carousels';

import * as markerimage from '../../images/icons/marker.svg';

export default {
  init() {
    // Load Booking form js
    booking();

    // Init Good To Know carousel
    gtkCarousel();

    // Init Namespaced carousel
    namespacedCarousel();

    // Init Card carousel
    cardCarousel();

    fullCarousel();

    /**
     * Destination gallery lightbox
     */
    $('.js-fb-gallery').on('click', function(e) {
      e.preventDefault();
      const galleryData = $(this).attr('data-gallery');
      $.fancybox.open(
        JSON.parse(galleryData),
        {
          loop : false,
        }
      );
    });

    /**
     * Google Map init
     */
    const $map = $('#js-map');
    const lat = parseFloat($map.attr('data-lat'));
    const lng = parseFloat($map.attr('data-lng'));
    const styles = [
      {
        featureType: 'poi.business',
        stylers: [{visibility: 'off'}],
      },
    ];

    const icon = {
        url: markerimage,
        scaledSize: new window.google.maps.Size(29, 44),
        origin: new window.google.maps.Point(0,0),
        anchor: new window.google.maps.Point(0, 0),
    };

    const map = new window.google.maps.Map($map.get(0), {
      center: {lat, lng},
      zoom: 16,
      styles: styles,
      mapTypeControl: false,
    });

    new window.google.maps.Marker({
      position: {lat, lng},
      map: map,
      icon,
    });
  },
  finalize() {},
};
