import {debounce} from 'lodash-es';
import loader from '../loader';

export default {
  init() {
    // Nav
    $('#js-categories').on('click', 'a', function(ev) {
      ev.preventDefault();
      loader.show();
      $('#js-categories').find('li').removeClass('nav__item--active');
      $(this).parent().addClass('nav__item--active');
      const id = $(this).attr('data-id');
      $.ajax({
        type : 'post',
        dataType : 'json',
        url : window.ysjs.ajaxurl,
        data : {action: 'get_faqs', id},
        success: function(response) {
          if(response.success) {
            loader.hide();
            $('#js-faq-group').html(response.data.results);
            window.faqIndex = response.data.index;
          } else {
            loader.hide();
            $('#js-faq-group').html('');
          }
        },
      });
    });

    // Search
    (() => {
      const $body = $('body');
      const resetSearch = () => {
        const $faqs = $('.js-faq');
        $body.removeClass('have-search-results');
        $faqs.show();
      }
      $('#js-filter').on('keyup', debounce(() => {
        const $faqs = $('.js-faq');
        const search = $('#js-filter').val();
        if (search !== '') {
          const results = window.faqIndex.filter(faq => search.split(' ').some(term => faq.title.toLowerCase().includes(term)));
          if (results.length > 0) {
            const ids = results.map(result => result.id).join(',');
            $faqs.not(ids).hide();
            $('#js-search-counter span').text(results.length);
            $body.addClass('have-search-results');
          }
        } else {
          resetSearch();
        }
      }, 400));
    })();
  },
};
