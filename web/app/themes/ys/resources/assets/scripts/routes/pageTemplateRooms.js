import {cardCarousel, gtkCarousel} from '../carousels';

export default {
  init() {
    // Init Card carousel
    cardCarousel()

    // Init Good To Know carousel
    gtkCarousel();
  },
};
