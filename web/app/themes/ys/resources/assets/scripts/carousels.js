import Swiper from 'swiper';

/**
 * Standard Carousel
 * - Navigation button outside Swiper container
 */
export const standardCarousel = () => {
  new Swiper('.js-carousel', {
    grabCursor: true,
    navigation: {
      nextEl: '#js-carousel-next',
      prevEl: '#js-carousel-prev',
      disabledClass: 'carousel__button--disabled',
    },
    pagination: {
      bulletActiveClass: 'carousel__bullets-item--active',
      bulletClass: 'carousel__bullets-item',
      clickable: true,
      el: '.carousel__bullets',
      type: 'bullets',
    },
  });
}

/**
 * Namespaced carousel
 * - Navigation button outside Swiper container
 */
export const namespacedCarousel = () => {
  $('[class*="js-carousel--"]').each(function() {
    const regex = /[^--]*$/;
    const found = this.classList.value.match(regex);
    new Swiper('.js-carousel--' + found, {
      grabCursor: true,
      observer: true,
      observeParents: true,
      navigation: {
        nextEl: '#js-carousel-next-' + found,
        prevEl: '#js-carousel-prev-' + found,
        disabledClass: 'carousel__button--disabled',
      },
      pagination: {
        bulletActiveClass: 'carousel__bullets-item--active',
        bulletClass: 'carousel__bullets-item',
        clickable: true,
        el: '#js-carousel-bullets-' + found,
        type: 'bullets',
      },
    });
  });
}

/**
 * Destinations carousel
 */
export const destinationsCarousel = () => {
  new Swiper('#js-carousel-destinations', {
    centerInsufficientSlides: true,
    slidesOffsetBefore: 16,
    slidesPerView: 1.4,
    spaceBetween: 8,
    breakpoints: {
      769: {
        slidesOffsetBefore: 0,
        slidesPerView: 3,
        watchOverflow: true,
      },
    },
  });
}

/**
 * Full Width carousel
 */
export const fullCarousel = () => {
  new Swiper('.js-carousel-full', {
    centeredSlides: true,
    centeredSlidesBounds: true,
    grabCursor: true,
    slidesOffsetAfter: 16,
    slidesOffsetBefore: 16,
    slidesPerView: 1.4,
    spaceBetween: 16,
    watchOverflow: true,
    observer: true,
    observeParents: true,
    breakpoints: {
      769: {
        slidesOffsetAfter: function() {
          return ($(window).innerWidth() - 1216 + 22) / 2;
        },
        slidesOffsetBefore: function() {
          return ($(window).innerWidth() - 1216 + 22) / 2;
        },
        slidesPerView: 3.5,
        spaceBetween: 22,
      },
      1408: {
        slidesPerView: 4.5,
      },
    },
    navigation: {
      nextEl: '.carousel__button--next',
      prevEl: '.carousel__button--prev',
      disabledClass: 'carousel__button--disabled',
    },
  });
}

/**
 * Good to know carousel
 * - All slides are visible on desktop
 * - Multiple slide on mobile
 */
export const gtkCarousel = () => {
  new Swiper('.js-carousel-gtk', {
    slidesPerView: 2,
    slidesPerGroup: 2,
    slidesPerColumn: 4,
    slidesPerColumnFill: 'row',
    loop: false,
    watchOverflow: true,
    observer: true,
    observeParents: true,
    breakpoints: {
      769: {
        slidesPerView: 3,
        slidesPerGroup: 3,
        slidesPerColumn: 5,
      },
    },
    pagination: {
      bulletActiveClass: 'carousel__bullets-item--active',
      bulletClass: 'carousel__bullets-item',
      clickable: true,
      el: '.carousel__bullets--gtk',
      type: 'bullets',
    },
  });
}

/**
 * Card carousel
 * - Navigation arrow on hover with custom cursor
 * - Fractional pagination
 */
export const cardCarousel = () => {
  new Swiper('.js-carousel-card', {
    preloadImages: false,
    lazy: true,
    allowTouchMove: true,
    grabCursor: false,
    parallax: true,
    slidesPerView: 'auto',
    watchSlidesVisibility: true,
    observer: true,
    observeParents: true,
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.carousel__button--next',
      prevEl: '.carousel__button--prev',
      disabledClass: 'carousel__button--disabled',
    },
    breakpoints: {
      769: {
        allowTouchMove: false,
      },
    },
  });
}

/**
 * Rooms carousel
 * - All slides are visible on desktop
 * - Multiple slide on mobile
 */
export const roomCarousel = () => {
  new Swiper('.js-carousel-room', {
    allowTouchMove: false,
    slidesPerView: 1,
    slidesPerColumn: 1,
    loop: false,
    watchOverflow: true,
    spaceBetween: 0,
    breakpoints: {
      769: {
        slidesPerColumn: 20,
        spaceBetween: 44,
      },
    },
    navigation: {
      nextEl: '.carousel__button--next',
      prevEl: '.carousel__button--prev',
      disabledClass: 'carousel__button--disabled',
    },
  });
}

/**
 * Authors carousel
 * - Four slides on Desktop
 * - Single slide on mobile
 */
export const authorsCarousel = () => {
  new Swiper('.js-carousel-authors', {
    grabCursor: false,
    slidesPerView: 1,
    spaceBetween: 0,
    breakpoints: {
      769: {
        slidesPerView: 4,
        spaceBetween: 22,
      },
    },
    navigation: {
      nextEl: '.carousel__button--next',
      prevEl: '.carousel__button--prev',
      disabledClass: 'carousel__button--disabled',
    },
    pagination: {
      bulletActiveClass: 'carousel__bullets-item--active',
      bulletClass: 'carousel__bullets-item',
      clickable: true,
      el: '.carousel__bullets',
      type: 'bullets',
    },
  });
}
