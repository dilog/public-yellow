export default {
  show: () => {
    $('body').addClass('loader-is-visible');
  },
  hide: () => {
    $('body').removeClass('loader-is-visible');
  },
}
