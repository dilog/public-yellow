import {getTransitionDurationFromElement} from './utils';

export default () => {
  const $el = $('.js-tabs');
  const $items = $el.find('.tabs__item');
  const $links = $('.js-tabs .tabs__nav').find('a');
  const $contents = $('.tabs-content');
  $links.on('click', function(e) {
    e.preventDefault();
    const selected = $(this).attr('href');
    const duration = getTransitionDurationFromElement($(selected).get(0));
    // - Hide current tab
    $items.removeClass('tabs__item--active');
    $contents.removeClass('tabs-content--active tabs-content--shown')
    // - Make new tab active but is still hidden
    $(this).parent().addClass('tabs__item--active');
    $el.trigger('ys:tabs:show', { selected });
    // - Start fade animation
    $(selected).addClass('tabs-content--active').delay(duration).queue(function(next){
      $(this).addClass('tabs-content--shown');
      $el.trigger('ys:tabs:shown', { selected });
      next();
    });
  })
}
