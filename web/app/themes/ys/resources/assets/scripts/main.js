// import external dependencies
import 'jquery';
import flatpickr from 'flatpickr';
import {Italian} from 'flatpickr/dist/l10n/it.js'

// Import everything from autoload
import './autoload/icons.js';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';

// Corportate
import home from './routes/home';
import pageTemplateGroupBooking from './routes/pageTemplateGroupBooking';
import postTypeArchiveEat from './routes/postTypeArchiveEat';
import pageTemplateTourAndExperience from './routes/pageTemplateTourAndExperience';
import pageTemplateAboutUs from './routes/pageTemplateAboutUs';
import pageTemplateFaq from './routes/pageTemplateFaq';
import jsBlog from './routes/jsBlog';
import singlePost from './routes/singlePost';

// Destination
import homeDestination from './routes/homeDestination';
import pageTemplateRooms from './routes/pageTemplateRooms';
import taxRoomCat from './routes/taxRoomCat';
import singleEat from './routes/singleEat';
import singleTour from './routes/singleTour';
import postTypeArchiveWork from './routes/postTypeArchiveWork';
import postTypeArchiveTour from './routes/postTypeArchiveTour';

import homeSmartsquare from './routes/homeSmartsquare';

flatpickr.localize(Italian);

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page corporate
  home,
  // Blog page
  // Category taxonomy archive
  // Destination taxonomy archive
  jsBlog,
  // About Us page template
  pageTemplateAboutUs,
  // Group Booking page template
  pageTemplateGroupBooking,
  // Eat and Party page template
  postTypeArchiveEat,
  // Tour and Experience page template
  pageTemplateTourAndExperience,
  // FAQ page template
  pageTemplateFaq,
  // Single Post
  singlePost,
  // Home page destination
  homeDestination,
  // Destination: Rooms page template
  pageTemplateRooms,
  // Destination: Tax "room-cat" archive page
  taxRoomCat,
  // Home page SmartSquare
  homeSmartsquare,
  // Single Eat Post
  singleEat,
  // Single Tour
  singleTour,
  // Tour Template
  postTypeArchiveTour,
  // Archive Work
  postTypeArchiveWork,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
