import lottie from 'lottie-web';
import * as shit from 'lottie/shit.json';
import * as sleep from 'lottie/sleep.json';

const shitAnimation = function() {
  const el = document.getElementById('js-lottie-shit');
  const animation = lottie.loadAnimation({
    container: el,
    renderer: 'svg',
    loop: true,
    autoplay: false,
    animationData: shit,
  });
  $(el).on({
    mouseenter: function() {
      animation.play();
    },
    mouseleave: function() {
      animation.pause();
    },
  });
}

const sleepAnimation = function() {
  const el = document.getElementById('js-lottie-sleep');
  const animation = lottie.loadAnimation({
    container: el,
    renderer: 'svg',
    loop: true,
    autoplay: false,
    animationData: sleep,
  });
  $(el).on({
    mouseenter: function() {
      animation.play();
    },
    mouseleave: function() {
      animation.pause();
    },
  });
}

export default function () {
  shitAnimation();
  sleepAnimation();
}
