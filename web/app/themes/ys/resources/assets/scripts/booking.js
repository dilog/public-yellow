import flatpickr from 'flatpickr';

export default () => {
  /**
   * Book Now form datepicker
   * - Corporate home
   * - Destination home
   */
  flatpickr('#checkin', {
    minDate: new Date(),
    static: true,
    mode: 'range',
    altInput: true,
    altFormat: 'j.m.y',
    dateFormat: 'Y-m-d',
    //dateFormat: 'j.m.y',
    locale: {
      rangeSeparator: ' - ',
    },
    onValueUpdate: function(selectedDates) {
      var _this=this;
      var dateArr=selectedDates.map(function(date){return _this.formatDate(date,'Y-m-d');});
      if (selectedDates.length > 1) {
        let nights = 0;
        while (selectedDates[1] > selectedDates[0]) {
          nights++
          selectedDates[0].setDate(selectedDates[0].getDate() + 1)
        }
        $('#nights').val(nights);
        $('.hidden-checkin').val(dateArr[0]);
        $('.hidden-checkout').val(dateArr[1]);
      }
    },
    showMonths: 2,
  });

  flatpickr('.sidebooking', {
    minDate: new Date(),
    static: true,
    mode: 'range',
    dateFormat: 'j.m.y',
    wrap: true,
    locale: {
      rangeSeparator: ' - ',
      locale: 'it',
    },
    onValueUpdate: function(selectedDates) {
      var _this=this;
      var dateArr=selectedDates.map(function(date){return _this.formatDate(date,'Y-m-d');});
      $('.hidden-checkin').val(dateArr[0]);
      $('.hidden-checkout').val(dateArr[1]);

      const months = ['Genn', 'Febbr', 'Mar', 'Apr', 'Magg', 'Giugno', 'Luglio', 'Ag', 'Sett', 'Ott', 'Nov', 'Dic'];
      const days = ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'];


      const startDay = days[selectedDates[0].getDay()];
      const startDate = selectedDates[0].getDate();
      const startMonth = months[selectedDates[0].getMonth()];
      const startYear = selectedDates[0].getFullYear();
      const endDay = selectedDates.length > 1 ? days[selectedDates[1].getDay()] : '';
      const endDate = selectedDates.length > 1 ? selectedDates[1].getDate() : '';
      const endMonth = selectedDates.length > 1 ? months[selectedDates[1].getMonth()] : '';
      const endYear = selectedDates.length > 1 ? selectedDates[1].getFullYear() : '';

      const checkinDatesLabel = $('#checkin-dates');
      const checkoutDatesLabel = $('#checkout-dates');

      checkinDatesLabel.find('.day-of-month').html(startDate);
      checkinDatesLabel.find('.month-and-year').html(`${startMonth} ${startYear}`);
      checkinDatesLabel.find('.day-of-week').html(startDay);

      checkoutDatesLabel.find('.day-of-month').html(endDate);
      checkoutDatesLabel.find('.month-and-year').html(`${endMonth} ${endYear}`);
      checkoutDatesLabel.find('.day-of-week').html(endDay);

    },
      onReady (_, __, fp) {
        fp.calendarContainer.classList.add('sidebar_flatpickr');
      },
    showMonths: 2,
  });


  /**
   * Coupon dropdown
   */
  $('#js-booking-coupon').on('click', 'button', function(){
    $(this).parent().toggleClass('booking__coupon-active');
  })
}

