@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <article class="{{ $classes }}">
      @component('components.8-columns')
        <header class="post-header">
          <div class="post-header__content">
            <h1 class="post-header__title entry-title"><span class="post-header__title-desc">{{ $category['title'] }}</span>{!! get_the_title() !!}</h1>
          </div><!-- /.post-header__content -->

        </header><!-- /.post-header -->
      @endcomponent

      @component('components.8-columns')
        <div class="post-meta entry-meta">
          <div class="byline author vcard">
            <img src="{{ $author['avatar'] }}" alt="Avatar {{ $author['fullname'] }}" class="author__avatar photo" />
            <div class="post-meta__content">
              <span class="fn">{{ $author['fullname'] }}</span>
              <time class="updated" datetime="{{ $datetime['iso'] }}">{{ $datetime['hr'] }}</time>
            </div>
          </div>
          @include('partials.share')
        </div><!-- /.post-meta -->
      @endcomponent

      @component('components.8-columns')
        <figure class="post-media">
          {!! $thumbnail !!}
        </figure><!-- /.post-media -->
      @endcomponent

      @component('components.6-columns')
        <div class="post-content entry-content">
          <div class="event-horizon">
            {!! $content !!}
          </div>
        </div><!-- /.post-content -->
      @endcomponent

      @component('components.6-columns')
        <footer class="post-footer">
          <div class="author">
            <img src="{{ $author['avatar'] }}" alt="Avatar {{ $author['fullname'] }}" class="author__avatar author__avatar--large" />
            <div class="author__content">
              <div class="author__name"><span>{{ __('Scritto da', 'ys') }}</span>{{ $author['fullname'] }}</div>
              <p>{{ $author['description']}}</p>
            </div>
          </div>
          @include('partials.share')
          {!!$tags!!}
        </footer><!-- /.post-footer -->
      @endcomponent
    </article>
  @endwhile

  @if ($related)
    <section class="related">
      <header class="related__header">
        <div class="related__container">
          <div class="related__title">{{ _x('Leggi altri articoli', 'corporate@blog', 'ys') }}</div>
        </div>
      </header><!-- /.related__header -->
      <div class="related__main">
        @component('components.carousel-full')
          @foreach ($related as $item)
            <div class="carousel__slide swiper-slide">
              @include('partials.cards/card-'.$item['post_type'])
            </div><!-- /.carousel__slide -->
          @endforeach
        @endcomponent
      </div><!-- /.related__main -->
    </section><!-- /.related -->
  @endif
@endsection
