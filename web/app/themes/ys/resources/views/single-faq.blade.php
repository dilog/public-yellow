@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <article class="{{ isset($classes) ? $classes : null }}">
      @component('components.8-columns')
        <header class="post-header">
          <div class="post-header__content">
            <h1 class="post-header__title entry-title">{!! get_the_title() !!}</h1>
          </div><!-- /.post-header__content -->
          <div class="post-header__patch">
            @include('partials.patch', ['type' => 'shit', 'animated' => true])
          </div>
        </header><!-- /.post-header -->
      @endcomponent

      @component('components.6-columns')
        <div class="post-content entry-content">
          <div class="event-horizon">
            {!! get_the_content() !!}
          </div>
        </div><!-- /.post-content -->
      @endcomponent

    </article>
  @endwhile

@endsection
