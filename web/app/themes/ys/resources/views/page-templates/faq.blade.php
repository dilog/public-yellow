{{--
  Template Name: Faq
  Template Post Type: page
--}}

@extends('layouts.app')

@section('head')
  <script>var faqIndex = {!! $faq_index !!}</script>
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    {{--
    //
    // Faq header
    //
    --}}
    <div
      class="faq-header">
      @component('components.6-columns')
        <div class="page-header__content">
          <h1 class="faq-header__title">{!! App::title() !!}</h1>
        </div>
      @endcomponent
      @component('components.4-columns')
        <div class="faq-search">
          <form class="faq-search__form">
            <div class="form__field form__field--has-submit-inside">
              <input id="js-filter" class="control" type="text" name="search" placeholder="{{ __('Cerca', 'ys') }}" />
              <button type="submit"><i class="icon ys-search"></i></button>
            </div><!-- /.form__field -->
          </form>
        </div>
      @endcomponent
    </div><!-- /.page-header -->
    {{--
    //
    // Main content
    //
    --}}
    <div class="faq-main container">
      <div class="columns is-centered">
        <div class="column is-3">
          @if ($categories)
            <div class="dropdown js-dropdown">
              <div class="dropdown__trigger">
                <button class="dropdown__button">
                  <span class="icon ys-menu"></span> <span class="dropdown__button-label">{!! $categories[0]['name'] !!}</span>
                </button>
              </div>
              <nav id="js-categories" class="dropdown__menu" role="menu">
                <ul class="dropdown__list">
                  @foreach ($categories as $category)
                    <li class="dropdown__list-item {{ isset($category['class']) ? $category['class'] : null }}">
                      <a data-id="{{ $category['id'] }}" href="#" role="button">{!! $category['name'] !!}</a>
                      </li>
                  @endforeach
                  </ul>
                </nav>
            </div>
          @endif
        </div>
        <div class="column is-7">
          <div id="js-search-counter" class="search-counter">
            {{ __('Risultati:', 'ys') }} <span></span>
          </div>
          <div id="js-faq-group">
            @include('partials.faq-group', ['groups' => $groups])
          </div>
        </div>
      </div>
    </div>
    {{--
    //
    // Ask Us
    //
    --}}
    <section class="tile tile--green tile--has-arrow">
      @component('components.8-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Non hai trovato quello che cercavi?', 'corporate@faq', 'ys') }}</h3>
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ _x('Contattaci e ti risponderemo appena possibile.', 'corporate@faq', 'ys') }}</p>
          <p>{{ __('Scrivi a', 'ys') }} <a href="mailto:{{$obfuscated_email['info']}}">{!! $obfuscated_email['info'] !!}</a></p>
        </div><!-- /.tile__main -->
      @endcomponent
    </section><!-- /.tile -->

  @endwhile
@endsection
{{-- @debug --}}
