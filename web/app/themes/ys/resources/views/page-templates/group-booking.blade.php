{{--
  Template Name: Group Booking
  Template Post Type: page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include(
      'partials.page-header',
      ['patch' => 'shit', 'position' => 'left']
    )
    {{--
    //
    // Fom
    //
    --}}
    <section class="section">
      <header class="is-hidden-tablet">
        <h3 class="tile__title text-center">{{ _x('Booka qui', 'corporate@groups', 'ys') }}</h3>
      </header>
      @component('components.10-columns')
        <form autocomplete="off" class="form form-group-booking" method="post" action="/booking-di-gruppo">
          <div class="form__fieldsets">
            <fieldset class="form__fieldset">
              <div class="form__fieldset-fields">
                <legend class="form__legend">{{ _x('Informazioni sulla prenotazione', 'corporate@groups', 'ys') }}</legend>
                <div class="form__row">
                  <div class="form__field">
                    <select class="control ys-select" name="destination" required>
                      <option value="">{{ __('Seleziona una destinazione', 'ys') }}</option>
                      <option value="rome">{{ __('Roma', 'ys') }}</option>
                      <option value="milan">{{ __('Milano', 'ys') }}</option>
                      <!--<option value="florence">{{ __('Firenze', 'ys') }}</option>-->
                    </select>
                  </div>
                </div><!-- /.form__row -->
                <div class="form__row">
                  <div class="form__field form__field--has-icon">
                    <input id="date-checkin" class="control" type="date" name="checkin" placeholder="Check in">
                  </div>
                  <div class="form__field form__field--has-icon">
                    <input id="date-checkout" class="control" type="date" name="checkout" placeholder="Check out" />
                  </div>
                </div><!-- /.form__row -->
                <div class="form__row form__row-stacked-mobile">
                  <div class="form__field form__field--4">
                    <label for="people" class="js-stepper stepper">
                      <div class="stepper__label">
                        <i class="icon ys-people"></i>
                        <span class="is-hidden-tablet">{{ __('Partecipanti', 'ys') }}</span>
                      </div>
                      <button type="button" data-step="-1">-</button>
                      <input id="people" type="number" min="21" max="99" name="people" value="20">
                      <button type="button" data-step="1">+</button>
                    </label>
                  </div>
                  <div class="form__field">
                    <select class="control ys-select" name="group_type" required>
                      <option value="">{{ __('Tipo di gruppo', 'ys') }}</option>
                      <option value="Option">{{ __('Option', 'ys') }}</option>
                    </select>
                  </div>
                </div><!-- /.form__row -->
                <div class="form__row form__row--stacked">
                  <div class="form__field mb-s">
                    <label class="checkbox">
                      <input type="checkbox" class="checkbox__input" />
                      <span class="checkbox__label">{{ __('Seleziona se ci sono minorenni nel gruppo', 'ys') }}</span>
                    </label>
                  </div>
                </div><!-- /.form__row -->
              </div><!-- /.form__fieldset-fields -->
            </fieldset><!-- /.form__fieldset -->
            <fieldset class="form__fieldset">
              <div class="form__fieldset-fields">
                <legend class="form__legend">{{ _x('Informazioni di contatto', 'corporate@groups', 'ys') }}</legend>
                <div class="form__row form__row-stacked-mobile">
                  <div class="form__field">
                    <input name="fname" class="control" placeholder="{{ __('Nome', 'ys') }}" type="text" required />
                    </select>
                  </div>
                  <div class="form__field">
                    <input name="lname" class="control" placeholder="{{ __('Cognome', 'ys') }}" type="text" required />
                  </div>
                </div><!-- /.form__row -->
                <div class="form__row form__row-stacked-mobile">
                  <div class="form__field">
                    <input name="phone" class="control" placeholder="{{ __('Telefono', 'ys') }}" type="text" required />
                    </select>
                  </div>
                  <div class="form__field">
                    <input name="email" class="control" placeholder="{{ __('Email', 'ys') }}" type="email" required />
                  </div>
                </div><!-- /.form__row -->
                <div class="form__row form__row--can-grow">
                  <div class="form__field">
                    <textarea name="note" class="control" placeholder="{{ __('Informazioni aggiuntive', 'ys') }}"></textarea>
                  </div>
                </div><!-- /.form__row -->
                <div class="form__row">
                  <div class="form__field">
                    <label class="checkbox">
                      <input type="checkbox" class="checkbox__input" />
                      <span class="checkbox__label">{{ __('Ho letto e accetto la', 'ys') }}&nbsp;<a href="#" target="_blank">{{ __('privacy policy', 'ys') }}</a></span>
                    </label>
                  </div>
                </div><!-- /.form__row -->
              </div><!-- /.form__fieldset-fields -->
            </fieldset><!-- /.form__fieldset -->
          </div>
          <div class="form__actions">
            <button class="button" value="submit_group_form" type="submit" name="submit_group_form">{{ __('Invia richiesta', 'ys') }}</button>
          </div>
          <div class="form__footer">
            <a href="#">{{ _x('Meno di 20 persone? Usa la prenotazione tradizionale.', 'corporate@groups', 'ys') }} </a>
          </div>
        </form>
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Plus
    //
    --}}
    <section class="section">
      @component('components.8-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('Perchè prenotare allo YellowSquare?', 'corporate@groups', 'ys') }}</h3>
          <p class="section__subtitle"><span class="is-hidden-mobile">{{ _x('Perché ci piace far star bene i nostri ospiti curando tutto nel minimo dettaglio.', 'corporate@groups', 'ys') }}<br></span><span>{{ _x('Ecco i principali vantaggi della nostra offerta.', 'corporate@groups', 'ys') }}</p>
        </header>
      @endcomponent
      <div class="section__main">
        @component('components.10-columns')
          <ul class="grid grid--5-desktop grid--2-mobile groups-plus">
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/events.svg')" alt="Events patch" aria-hidden="true">
              {{ _x('Eventi di benvenuto personalizzabili', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/deposit.svg')" alt="Luggage deposit patch" aria-hidden="true">
              {{ _x('Deposito bagagli gratutito', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/24h.svg')" alt="24H patch" aria-hidden="true">
              {{ _x('Reception 24h', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/fastcheck.svg')" alt="Express check patch" aria-hidden="true">
              {{ _x('Express check-in dedicato', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/room.svg')" alt="Room patch" aria-hidden="true">
              {{ _x('Assegnazione stanza personalizzata', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/bed-sheets.svg')" alt="Bed patch" aria-hidden="true">
              {{ _x('Lenzuola', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/discount.svg')" alt="Discount patch" aria-hidden="true">
              {{ _x('Sconti su tour & attività', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/priority-bar.svg')" alt="Priority Bar patch" aria-hidden="true">
              {{ _x('Accesso prioritario al bar', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/discount-bar.svg')" alt="Discount Bar patch" aria-hidden="true">
              {{ _x('Sconti sui drink', 'corporate@groups', 'ys') }}
            </li>
            <li class="group-plus__item">
              <img src="@asset('images/patches/60/discount-food.svg')" alt="Discount Bar patch" aria-hidden="true">
              {{ _x('Sconti sulle colazioni personalizzate', 'corporate@groups', 'ys') }}
            </li>
          </ul><!-- /.plus -->
        @endcomponent
      </div><!-- /.section__main -->
    </section><!-- /.section -->
    {{--
    //
    // Destinations
    //
    --}}
    <section class="section section--full-width-mobile">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('Scopri le nostre destinazioni', 'ys') }}</h3>
        </header><!-- /.section__header -->
      @endcomponent
      <div class="section__main">
        @component('components.container')
          @include('partials.carousel-destinations')
        @endcomponent
      </div><!-- /.section__main -->
    </section><!-- /.section -->
  @endwhile
@endsection
