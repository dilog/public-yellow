{{--
  Template Name: Responsibility
  Template Post Type: page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    {{--
    //
    // Ideas
    //
    --}}
    <section class="section section--marginless">
      @component('components.container')
        <div class="features">
          @foreach ($ideas as $item)
            <div class="{{ $item['class'] }}">
              <div class="feature__header-mobile">
                <h3 class="feature__title">{{ $item['title'] }}</h3>
              </div><!-- /.feature__header-mobile -->
              <div class="feature__media">
                <figure class="feature__figure">
                  <img src="@asset('images/placeholders/about-us-story.jpg')" alt="" />
                  <figcaption class="feature__figcaption"><span class="tag tag--position"><i class="icon ys-marker"></i> {{ $item['destination']->name}}</span></figcaption>
                </figure>
                <div class="idea-author bg-{{ $item['author']['color'] }}">
                  <img class="idea-author__avatar" alt="{{ $item['author']['avatar']['alt'] }}" src="{{ $item['author']['avatar']['src'] }}">
                  <div class="idea-author__main">
                    {!! __("Da un'idea di:") !!}<br>
                    {{ $item['author']['name'] }}<br>
                    {{ $item['author']['country'] }}
                  </div>
                </div>
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h3 class="feature__title">{{ $item['title'] }}</h3>
                </div>
                <div class="feature__content">
                  <p>{{ $item['excerpt'] }}</p>
                </div><!-- /.feature_content -->
                <div class="feature__action">
                  <a class="button button--s" href="{{ $item['permalink'] }}">{{ __('Scopri di più', 'ys') }}</a>
                </div><!-- /.feature__action -->
              </div><!-- /.feature_main -->
            </div><!-- /.feature -->
          @endforeach
        </div><!-- /.features -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Send Us your Idea
    //
    --}}
    <section class="tile tile--pink">
      @component('components.6-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Sei un esperto di viaggi?', 'corporate@about-us', 'ys') }}</h3>
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ _x('Entra a far parte del nostro team di blogger e pubblica i tuoi articoli con noi!', 'corporate@about-us', 'ys') }}</p>
          <p>{{ __('Scrivi a', 'ys') }} <a href="mailto:{{$obfuscated_email['pr']}}">{!! $obfuscated_email['pr'] !!}</a></p>
        </div><!-- /.tile__main -->
      @endcomponent
      @include('partials.patch', ['type' => 'change-the-world'])
    </section><!-- /.tile -->
  @endwhile
@endsection
