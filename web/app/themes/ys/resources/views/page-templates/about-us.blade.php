{{--
  Template Name: About Us
  Template Post Type: page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include(
      'partials.page-header',
      ['patch' => 'shit', 'position' => 'right']
    )
    {{--
    //
    // History
    //
    --}}
    <section class="section section--marginless">
      @component('components.container')
        <div class="feature feature--horz feature--reverse">
          <div class="feature__header-mobile">
            <h3 class="feature__title">{{ _x('Da un letto a una piazza, dallo Yellow allo YellowSquare.', 'corporate@about-us', 'ys') }}</h3>
          </div><!-- /.feature__header-mobile -->
          <div class="feature__media">
            @component('components.carousel-card', ['shadow' => true])
                <div class="carousel__slide swiper-slide">
                  <figure class="carousel__image">
                    <img src="@asset('images/placeholders/about-us-story.jpg')" alt="" />
                    <!--<figcaption class="carousel__image-caption" data-swiper-parallax="-50%">1999</figcaption>-->
                  </figure>
                </div><!-- /.carousel__slide -->
                <div class="carousel__slide swiper-slide">
                  <figure class="carousel__image">
                    <img src="/app/uploads/2021/08/others01-scaled-e1630277759828.jpg" alt="" />
                    <!--<figcaption class="carousel__image-caption" data-swiper-parallax="-50%">1999</figcaption>-->
                  </figure>
                </div><!-- /.carousel__slide -->
                <div class="carousel__slide swiper-slide">
                  <figure class="carousel__image">
                    <img src="/app/uploads/2021/08/IMG_2897-scaled-e1630277747307.jpg" alt="" />
                    <!--<figcaption class="carousel__image-caption" data-swiper-parallax="-50%">1999</figcaption>-->
                  </figure>
                </div><!-- /.carousel__slide -->
            @endcomponent
          </div><!-- /.feature__media -->
          <div class="feature__main">
            <div class="feature__header">
              <h3 class="feature__title">{{ _x('Da un letto a una piazza, dallo Yellow allo YellowSquare.', 'corporate@about-us', 'ys') }}</h3>
            </div>
            <div class="feature__content">
              <p>
                {!! _x('Nel 99’ abbiamo deciso di prendere un piccolo appartamento con qualche letto da affittare. Così nasce lo Yellow. Da allora non smettiamo di crescere e imparare.', 'corporate@about-us', 'ys') !!}
              </p>
            </div><!-- /.feature_content -->
          </div><!-- /.feature_main -->
        </div><!-- /.feature -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Features
    //
    --}}
    <section class="section">
      <header class="section__header">
        <h3 class="section__title">{{ _x('Cosa abbiamo imparato', 'corporate@about-us', 'ys') }}</h3>
      </header>
      <div class="section__main">
        @component('components.10-columns')
          <div class="about-us-features">
            <div class="feature feature--reverse feature-standard-flow">
              <div class="feature__media">
                <img class="feature__image" src="@asset('images/placeholders/about-us-1.png')" alt="" />
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h2 class="feature__title">{{ _x('I viaggi fanno le persone.', 'corporate@about-us', 'ys') }}</h2>
                </div>
                <div class="feature__content">
                  <p>
                    {{ _x('Per noi viaggiare è cambiare e tornare a casa con qualcosa in più.', 'corporate@about-us', 'ys') }}
                  </p>
                </div><!-- /.feature__content -->
              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
            <div class="feature feature-standard-flow">
              <div class="feature__media">
                <img class="feature__image" src="@asset('images/placeholders/about-us-2.png')" alt="" />
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h2 class="feature__title">{{ _x('Gli estranei non esistono.', 'corporate@about-us', 'ys') }}</h2>
                </div>
                <div class="feature__content">
                  <p>
                    {{ _x('Per noi bandiere e confini sono roba da cartine geografiche.', 'corporate@about-us', 'ys') }}
                  </p>
                </div><!-- /.feature__content -->
              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
            <div class="feature feature--reverse feature-standard-flow">
              <div class="feature__media">
                <img class="feature__image" src="@asset('images/placeholders/about-us-3.png')" alt="" />
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h2 class="feature__title">{{ _x('Aperto vuol dire migliore.', 'corporate@about-us', 'ys') }}</h2>
                </div>
                <div class="feature__content">
                  <p>
                    {{ _x('Per noi ogni nuovo incontro ha qualcosa da insegnare.', 'corporate@about-us', 'ys') }}
                  </p>
                </div><!-- /.feature__content -->
              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
          </div>
        @endcomponent
      </div><!-- /.section__main -->
    </section><!-- /.section -->
    {{--
    //
    // Testimonials
    //
    --}}
    <section class="tile tile--orange tile--has-shadow">
      @component('components.8-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Lo YellowSquare per amici e colleghi', 'corporate@about-us', 'ys') }}</h3>
        </header><!-- /.tile__header -->
      @endcomponent
      <div class="tile__main">
        @component('components.8-columns')
          @component('components.carousel', ['namespace' => 'quote'])
            @foreach ([1, 2, 3, 4] as $item)
              <div class="carousel__slide swiper-slide">
                <blockquote class="quote-slide">
                  <div class="avatar">
                    <div class="avatar__mask">
                      <img class="avatar__image" src="@asset('images/placeholders/quote.jpg')" alt="" />
                    </div>
                  </div>
                  <div class="quote-slide__main">
                    <p class="quote-slide__content">"Para mi Yellow
                      es una familia."</p>
                    <footer class="quote-slide__cite">footerablo, Argentina<br>
                      Direttore artistico</footer>
                  </div>
                </blockquote><!-- /.quote-slide -->
              </div><!-- /.carousel__slide -->
            @endforeach
          @endcomponent
        @endcomponent
      </div><!-- /.tile__main -->
    </section><!-- /.tile -->
    {{--
    //
    // Work with Us
    //
    --}}
    <section class="tile tile--green">
      @component('components.6-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Vuoi lavorare con noi?', 'corporate@about-us', 'ys') }}</h3>
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ _x('Abbiamo tante posizioni aperte e una promessa: sorridere ti verrà naturale.', 'corporate@about-us', 'ys') }}</p>
          <p><a class="button button--s" href="/lavora-con-noi/">{{ _x('Candidati ora', 'corporate@about-us', 'ys') }}</a></p>
        </div><!-- /.tile__main -->
      @endcomponent
    </section><!-- /.tile -->
    {{--
    //
    // Expert Traveler
    //
    --}}
    <section class="tile tile--blue">
      @component('components.6-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Sei un esperto di viaggi?', 'corporate@about-us', 'ys') }}</h3>
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ _x('Entra a far parte del nostro team di blogger e pubblica i tuoi articoli con noi!', 'corporate@about-us', 'ys') }}</p>
          <p>{{ __('Scrivi a', 'ys') }} <a href="mailto:{{$obfuscated_email['pr']}}">{!! $obfuscated_email['pr'] !!}</a></p>
        </div><!-- /.tile__main -->
      @endcomponent
    </section><!-- /.tile -->
    {{--
    //
    // Press
    //
    --}}
    <section class="section section--full-width-mobile section--press">
      @component('components.8-columns')
        <header class="section__header">
          <h4 class="section__title">{{ _x('Parlano di noi', 'corporate@about-us', 'ys') }}</h4>
        </header><!-- /.section__header -->
      @endcomponent
      <div class="marquee marquee--press">
        <div class="marquee__tape">
          @foreach ([1, 2] as $item)
            <div class="marquee__segment">
              <span><img class="marquee__item" src="@asset('images/press/4hotel.svg')" alt=""></span>
              <span><img class="marquee__item" src="@asset('images/press/theguardian.svg')" alt=""></span>
              <span><img class="marquee__item" src="@asset('images/press/thetelegraph.svg')" alt=""></span>
              <span><img class="marquee__item" src="@asset('images/press/tripadvisor.svg')" alt=""></span>
              <span><img class="marquee__item" src="@asset('images/press/vice.svg')" alt=""></span>
            </div>
          @endforeach
        </div>
      </div>
    </section>
  @endwhile
@endsection
