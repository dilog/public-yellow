{{--
  Template Name: Eat & Party
  Template Post Type: page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include(
      'partials.page-header',
      ['patch' => 'shit', 'position' => 'left']
    )
    {{--
    //
    // Intro Rome
    //
    --}}
    <section class="section section--marginless">
      @component('components.container')
        <div class="feature feature--horz feature--reverse">
          <div class="feature__header-mobile">
            <h3 class="feature__title">{{ _x('A Roma fare feste è una cosa seria.', 'corporate@eat-party', 'ys') }}</h3>
          </div>
          <div class="feature__media">
            @component('components.carousel-card', ['shadow' => true])
              @foreach ([1, 2] as $item)
                <div class="carousel__slide swiper-slide">
                  <figure class="carousel__image">
                    <img src="@asset('images/placeholders/eat-party-1.jpg')" alt="" />
                    <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> Rome - Tonika</figcaption></figcaption>
                  </figure>
                </div><!-- /.carousel__slide -->
              @endforeach
            @endcomponent
          </div><!-- /.feature__media -->
          <div class="feature__main">
            <div class="feature__header">
              <h3 class="feature__title">{{ _x('A Roma fare feste è una cosa seria.', 'corporate@eat-party', 'ys') }}</h3>
            </div>
            <div class="feature__content">
              <p>
                {{ _x("Creare la situazione perfetta è la nostra passione. Musica, party e ottimi drink: c'è tutto quello che ti serve per conoscere facce nuove.", 'corporate@eat-party', 'ys') }}
              </p>
            </div><!-- /.feature__content -->
            <div class="feature__action">
              <a class="button button--s" href="#">{{ __('Scopri Eat & Party Roma', 'ys') }}</a>
            </div><!-- /.feature__action -->
          </div><!-- /.feature__main -->
        </div><!-- /.feature -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Events carousel Rome
    //
    --}}
    <section class="section">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('Prossimi eventi @@Roma', 'corporate@eat-party', 'ys') }}</h3>
        </header><!-- /.section__header -->
      @endcomponent
      <div class="section__main">
        @if ($events_rome)
          @component('components.carousel-full', [
            'class' => 'carousel--events',
            'link' => '#',
            'label' => __('Guarda il calendario di Roma', 'ys'),
          ])
            @foreach ($events_rome as $item)
              <div class="carousel__slide swiper-slide">
                @include('partials.cards.card-event')
              </div><!-- /.carousel__slide -->
            @endforeach
          @endcomponent
        @endif
      </div><!-- /.section__main -->
    </section><!-- /.section -->
    {{--
    //
    // Intro Milan
    //
    --}}
    <section class="section section--marginless">
      @component('components.container')
        <div class="feature feature--horz">
          <div class="feature__header-mobile">
            <h3 class="feature__title">{{ _x('A Milano, vieni per lo stile, rimani per le vibes.', 'corporate@eat-party', 'ys') }}</h3>
          </div>
          <div class="feature__media">
            @component('components.carousel-card', ['shadow' => true])
              @foreach ([1, 2] as $item)
                <div class="carousel__slide swiper-slide">
                  <figure class="carousel__image">
                    <img src="@asset('images/placeholders/eat-party-1.jpg')" alt="" />
                    <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> Rome - Tonika</figcaption></figcaption>
                  </figure>
                </div><!-- /.carousel__slide -->
              @endforeach
            @endcomponent
          </div><!-- /.feature__media -->
          <div class="feature__main">
            <div class="feature__header">
              <h3 class="feature__title">{{ _x('A Milano, vieni per lo stile, rimani per le vibes.', 'corporate@eat-party', 'ys') }}</h3>
            </div>
            <div class="feature__content">
              <p>
                {{ _x("Installazioni in una cappella sconsacrata, party e aperitivi per celebrare moda, design e musica tra una cotoletta rivisitata e una brioche salata.", 'corporate@eat-party', 'ys') }}
              </p>
            </div><!-- /.feature__content -->
            <div class="feature__action">
              <a class="button button--s" href="#">{{ __('Scopri Eat & Party Milano', 'ys') }}</a>
            </div><!-- /.feature__action -->
          </div><!-- /.feature__main -->
        </div><!-- /.feature -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Events carousel Milan
    //
    --}}
    <section class="section">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('Prossimi eventi @@Milano', 'corporate@eat-party', 'ys') }}</h3>
        </header><!-- /.section__header -->
      @endcomponent
      <div class="section__main">
        @if ($events_milan)
          @component('components.carousel-full', [
            'class' => 'carousel--events',
            'link' => '#',
            'label' => __('Guarda il calendario di Milano', 'ys'),
          ])
            @foreach ($events_milan as $item)
              <div class="carousel__slide swiper-slide">
                @include('partials.cards.card-event')
              </div><!-- /.carousel__slide -->
            @endforeach
          @endcomponent
        @endif
      </div><!-- /.section__main -->
    </section><!-- /.section -->
    {{--
      //
      // Stay for Free promo
      //
      --}}
    <section class="tile tile--blue">
      @component('components.6-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Cerchi uno spazio per il tuo evento?', 'corporate@eat-party', 'ys') }}</h3>
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ _x("Allo YellowSquare puoi organizzare aperitivi, compleanni, feste di laurea e party galattici.", 'corporate@eat-party', 'ys') }}</p>
          <p>{{ __('Scrivi a', 'ys') }} <a href="mailto:{{$obfuscated_email['info']}}">{!! $obfuscated_email['info'] !!}</a></p>
        </div><!-- /.tile__main -->
      @endcomponent
    </section><!-- /.tile -->
  @endwhile
@endsection
