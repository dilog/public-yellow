{{--
  Template Name: Tour & Experience
  Template Post Type: page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include(
      'partials.page-header',
      ['patch' => 'shit', 'position' => 'left']
    )
    {{--
    //
    // Milan
    //
    --}}
    <section class="section">
      @component('components.container')
        <div class="feature feature--horz feature--reverse">
          <div class="feature__header-mobile">
            <h3 class="feature__title">{{ _x('Fai un giro tra i migliori spot di Milano.', 'corporate@tour-experience', 'ys') }}</h3>
          </div>
          <div class="feature__media">
            @component('components.carousel-card', ['shadow' => true])
              @foreach ([1, 2] as $item)
                <div class="carousel__slide swiper-slide">
                  <figure class="carousel__image">
                    <img src="@asset('images/placeholders/tour-1.jpg')" alt="" />
                    <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> Milano - Bike Tour</figcaption></figcaption>
                  </figure>
                </div><!-- /.carousel__slide -->
              @endforeach
            @endcomponent
          </div><!-- /.feature__media -->
          <div class="feature__main">
            <div class="feature__header">
              <h3 class="feature__title">{{ _x('Fai un giro tra i migliori spot di Milano.', 'corporate@tour-experience', 'ys') }}</h3>
            </div>
            <div class="feature__content">
              <p>
                {{ _x('Tutta la moda, l’architettura e l’artigianato della città in tanti imperdibili tour.', 'corporate@tour-experience', 'ys') }}
              </p>
            </div><!-- /.feature__content -->
            <div class="feature__action">
              <a class="button button--s" href="#">{{ __('Scopri Tour & Experience di Milano', 'ys') }}</a>
            </div><!-- /.feature__action -->
          </div><!-- /.feature__main -->
        </div><!-- /.feature -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Rome
    //
    --}}
    <section class="section">
      @component('components.container')
        <div class="feature feature--horz">
          <div class="feature__header-mobile">
            <h3 class="feature__title">{{ _x('Vieni con noi alla scoperta dei segreti della Città Eterna.', 'corporate@tour-experience', 'ys') }}</h3>
          </div>
          <div class="feature__media">
            @component('components.carousel-card', ['shadow' => true])
              @foreach ([1, 2] as $item)
                <div class="carousel__slide swiper-slide">
                  <figure class="carousel__image">
                    <img src="@asset('images/placeholders/tour-2.jpg')" alt="" />
                    <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> Roma - Cooking Class</figcaption></figcaption>
                  </figure>
                </div><!-- /.carousel__slide -->
              @endforeach
            @endcomponent
          </div><!-- /.feature__media -->
          <div class="feature__main">
            <div class="feature__header">
              <h3 class="feature__title">{{ _x('Vieni con noi alla scoperta dei segreti della Città Eterna.', 'corporate@tour-experience', 'ys') }}</h3>
            </div>
            <div class="feature__content">
              <p>
                {{ _x('Potrai imparare a fare la vera pasta seguendo l’antica ricetta della nonna.', 'corporate@tour-experience', 'ys') }}
              </p>
            </div><!-- /.feature__content -->
            <div class="feature__action">
              <a class="button button--s" href="#">{{ __('Scopri Tour & Experience di Roma', 'ys') }}</a>
            </div><!-- /.feature__action -->
          </div><!-- /.feature__main -->
        </div><!-- /.feature -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Staff tips
    //
    --}}
    <section class="tile tile--green tile--has-shadow">
      @component('components.8-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ __('Consigli dello staff', 'ys') }}</h3>
        </header><!-- /.tile__header -->
      @endcomponent
      <div class="tile__main">
        @component('components.8-columns')
          @component('components.carousel', ['namespace' => 'tips'])
            @foreach ([1, 2, 3, 4] as $item)
              <div class="carousel__slide swiper-slide">
                <blockquote class="quote-slide">
                  <div class="avatar">
                    <div class="avatar__mask">
                      <img class="avatar__image" src="@asset('images/placeholders/quote.jpg')" alt="" />
                    </div>
                  </div>
                  <div class="quote-slide__main">
                    <p class="quote-slide__content">"Grazia & Graziella
                      Trastevere."</p>
                    <footer class="quote-slide__cite">José Silva, Portugal<br>Receptionist</footer>
                  </div>
                </blockquote><!-- /.quote-slide -->
              </div><!-- /.carousel__slide -->
            @endforeach
          @endcomponent
        @endcomponent
      </div><!-- /.tile__main -->
    </section><!-- /.tile -->
    {{--
    //
    // New experience call to action
    //
    --}}
    <section class="tile tile--salmon">
      @component('components.6-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Hai nuove esperienze da proporci?', 'corporate@tour-experience', 'ys') }}</h3>
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ _x('Condividele con noi e aiutaci a migliorare la nostra offerta. Scrivi a:', 'corporate@tour-experience', 'ys') }}</p>
          <p>{{ __('Scrivi a', 'ys') }} <a href="mailto:{{$obfuscated_email['info']}}">{!! $obfuscated_email['info'] !!}</a></p>
        </div><!-- /.tile__main -->
      @endcomponent
    </section><!-- /.tile -->
    {{--
    //
    // Milan tours
    //
    --}}
    <section class="section is-hidden-mobile">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('I nostri Tour & Experience', 'corporate@tour-experience', 'ys') }}</h3>
        </header>
      @endcomponent
      <div class="section__main">
        @component('components.container')
          <div class="grid-with-cover grid-with-cover--right mb-l">
            <div class="cover">
              <div class="cover__media">
                <img src="@asset('images/placeholders/tour-3.jpg')" alt="" />
              </div>
              <div class="cover__layer">
                <h3 class="cover__title">{{ __('Roma', 'ys') }}</h3>
                <p class="cover__content">{{ _x('Non c’è hashtag per descrivere ciò che vivrai.', 'corporate@tour-experience', 'ys') }}</p>
                <p class="cover__action">
                  <a href="#" class="button">{{ __('Scopri di più', 'ys') }}</a>
                </p>
              </div>
            </div><!-- /.cover -->
            @foreach ([1, 2, 3, 4] as $item)
              @include('partials.cards.card-tour', ['class' => 'card--small'])
            @endforeach
          </div><!-- /.grid-with-cover -->

          <div class="grid-with-cover">
            <div class="cover">
              <div class="cover__media">
                <img src="@asset('images/placeholders/tour-3.jpg')" alt="" />
              </div>
              <div class="cover__layer">
                <h3 class="cover__title">{{ __('Milano', 'ys') }}</h3>
                <p class="cover__content">{{ _x('Una volta a casa potrai dire: «Oggi cucino italiano!».', 'corporate@tour-experience', 'ys') }}</p>
                <p class="cover__action">
                  <a href="#" class="button">{{ __('Scopri di più', 'ys') }}</a>
                </p>
              </div>
            </div><!-- /.cover -->
            @foreach ([1, 2, 3, 4] as $item)
              @include('partials.cards.card-tour', ['class' => 'card--small'])
            @endforeach
          </div><!-- /.grid-with-cover -->
        @endcomponent
      </div><!-- /section__main -->
    </section><!-- /section -->
    {{--
    //
    // Rome tours
    //
    --}}
    <section class="section section--full-width-mobile is-hidden-tablet">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('Tour & Experience', 'corporate@tour-experience', 'ys') }}</h3>
        </header>
      @endcomponent
      <div class="section__main">
        <div class="my h3 text-center">{{ __('Milano', 'ys') }}</div>
        @component('components.carousel-full', [
          'class' => 'carousel--tours',
          'link' => get_permalink(get_option('page_for_posts')),
          'label' => __('Scopri di più', 'ys'),
        ])
          @foreach ([1, 2, 3, 4, 5] as $item)
            <div class="carousel__slide swiper-slide">
              @include('partials.cards.card-tour', ['class' => 'card--small'])
            </div><!-- /.carousel__slide -->
          @endforeach
        @endcomponent
        <div class="my-l h3 text-center">{{ __('Roma', 'ys') }}</div>
        @component('components.carousel-full', [
          'class' => 'carousel--tours',
          'link' => get_permalink(get_option('page_for_posts')),
          'label' => __('Scopri di più', 'ys'),
        ])
          @foreach ([1, 2, 3, 4, 5] as $item)
            <div class="carousel__slide swiper-slide">
              @include('partials.cards.card-tour', ['class' => 'card--small'])
            </div><!-- /.carousel__slide -->
          @endforeach
        @endcomponent
      </div><!-- /.section__main -->
    </section><!-- /.section -->
  @endwhile
@endsection
