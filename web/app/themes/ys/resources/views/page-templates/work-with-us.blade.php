{{--
  Template Name: Work With Us
  Template Post Type: page
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include(
      'partials.page-header',
      ['patch' => 'shit', 'position' => 'left']
    )
    {{--
    //
    // Video
    //
    --}}
    {{--
    <section class="section">
      @component('components.8-columns')
        <div class="responsive-video">
          <iframe src="https://player.vimeo.com/video/297115013?title=0&byline=0&portrait=0" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
        </div>
      @endcomponent
    </section><!-- /.section -->
    --}}
    {{--
    //
    // Plus
    //
    --}}
    <section class="section">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('Cosa cerchiamo?', 'corporate@jobs', 'ys') }}</h3>
        </header>
      @endcomponent
      <div class="section__main">
        @component('components.10-columns')
          <ul class="plus">
            <li class="plus__item">
              <img src="@asset('images/patches/think-positive.svg')" alt="Think Positive patch" aria-hidden="true">
              {{ _x('Positività', 'corporate@jobs', 'ys') }}
            </li>
            <li class="plus__item">
              <img src="@asset('images/patches/be-proactive.svg')" alt="Be Proactive patch" aria-hidden="true">
              {{ _x('Proattività', 'corporate@jobs', 'ys') }}
            </li>
            <li class="plus__item">
              <img src="@asset('images/patches/open-your-mind.svg')" alt="Be Proactive patch" aria-hidden="true">
              {{ _x('Menti aperte', 'corporate@jobs', 'ys') }}
            </li>
            <li class="plus__item">
              <img src="@asset('images/patches/spread-new-ideas.svg')" alt="Be Proactive patch" aria-hidden="true">
              {{ _x('Nuove idee', 'corporate@jobs', 'ys') }}
            </li>
          </ul><!-- /.plus -->
        @endcomponent
      </div><!-- /.section__main -->
    </section><!-- /.section -->
    {{--
    //
    // Open positions
    //
    --}}
    <section class="section">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('Posizioni aperte', 'corporate@jobs', 'ys') }}</h3>
        </header>
      @endcomponent
      <div class="section__main">
        @component('components.8-columns')
          <div class="jobs">
          @foreach ($jobs as $item)
            <details class="details jobs__item">
              <summary class="details__summary">
                <div class="jobs__item-title">
                  <i class="icon ys-marker"></i> {{ $item['destination'] }} - {{ $item['title'] }}
                  <span>{{ __('Leggi la descrizione', 'ys') }}</span>
                  <span>{{ __('Chiudi la descrizione', 'ys') }}</span>
                </div>
              </summary>
              <div class="details__content event-horizon">
                {!! $item['content'] !!}
              </div>
            </details>
          @endforeach
          </div>
        @endcomponent
      </div>
    </section><!-- /.section -->
    {{--
    //
    // Stay for Free promo
    //
    --}}
    <section class="tile tile--blue">
      @component('components.6-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ _x('Sei un artista in viaggio?', 'corporate@tour-experience', 'ys') }}</h3>
        </header><!-- /.tile__header -->
        <div class="tile__main">
          <p>{{ _x("Con il programma Stay For Free stai da noi senza pagare nulla. In cambio ti chiediamo solo un po' delle tue speciali skills! Presentati scrivendo a:", 'corporate@tour-experience', 'ys') }}</p>
          <p>{{ __('Scrivi a', 'ys') }} <a href="mailto:{{$obfuscated_email['info']}}">{!! $obfuscated_email['info'] !!}</a></p>
        </div><!-- /.tile__main -->
      @endcomponent
    </section><!-- /.tile -->
  @endwhile
@endsection
