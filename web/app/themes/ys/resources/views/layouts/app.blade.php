<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  @yield('head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    @yield('below-header')
    <div class="wrap" role="document">
      <main class="main">
        @yield('content')
      </main>
    </div>
    <div class="loader">
      <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
        <g>
          <circle cx="50" cy="50" r="47.5"/>
        </g>
        <path d="M22.433 30.271a2.723 2.723 0 00-.368 2.07 2.73 2.73 0 001.208 1.722L46.27 48.71l-.24 24.518c-.013 1.511 1.217 2.754 2.752 2.772a2.758 2.758 0 002.754-2.724l.236-24.434L76.72 33.063a2.724 2.724 0 001.211-1.72 2.697 2.697 0 00-.361-2.067A2.735 2.735 0 0075.248 28c-.522 0-1.031.147-1.474.428L49.126 44.016 26.234 29.434a2.755 2.755 0 00-1.478-.43c-.946 0-1.814.474-2.323 1.267z" fill="#EFDE0C"/>
      </svg>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
