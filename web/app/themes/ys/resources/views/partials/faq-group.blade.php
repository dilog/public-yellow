@foreach ($groups as $group)
  <div class="group-faq">
    <h3 class="group-faq__title">{!! $group['title'] !!}</h3>
    @foreach ($group['items'] as $item)
      <details id="{{ $item['anchor'] }}" class="faq js-faq">
        <summary class="faq__question">{!! $item['title'] !!} <span class="icon ys-plus"></span><span class="icon ys-minus"></span></summary>
        <div class="faq__answer">{!! $item['content'] !!}</div>
      </details><!-- /.faq -->
    @endforeach
  </div><!-- /.group-faq -->
@endforeach
