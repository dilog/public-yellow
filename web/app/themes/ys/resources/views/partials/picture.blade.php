<picture>
  @foreach ($data as $source)
    <source data-srcset="{{ $source['src'] }}" media="{{ $source['media'] }}">
  @endforeach
  <img
    alt="{{ $alt }}"
    class="js-lazy {{ $class }}"
    data-src="{{ $data[0]['src'] }}"
    src="{{ $placeholder['src']}}"
  />
</picture>
