
<div class="patch{{ !isset($visible_mobile) ? ' is-hidden-mobile' : '' }}{{ isset($class) ? ' ' . $class : '' }}" aria-hidden="true">
  @switch($type)
    @case('be-proactive')
    <img src="@asset('images/patches/be-proactive.svg')" alt="be proactive patch" aria-hidden="true">
    @break
    @case('change-the-world')
    <img src="@asset('images/patches/change-the-world.svg')" alt="Change the world patch" aria-hidden="true">
    @break
    @case('dont-send-ass-pink')
    <img src="@asset('images/patches/dont-send-ass-pink.svg')" alt="Dont send ass patch" aria-hidden="true">
    @break
    @case('laundry')
    <img src="@asset('images/patches/laundry.svg')" alt="Laundry patch" aria-hidden="true">
    @break
    @case('nospam')
    <img src="@asset('images/patches/nospam.svg')" alt="No spam patch" aria-hidden="true">
    @break
    @case('open-your-mind')
    <img src="@asset('images/patches/open-your-mind.svg')" alt="Open your mind patch" aria-hidden="true">
    @break
    @case('sleep')
    <img src="@asset('images/patches/sleep.svg')" alt="Sleep patch" aria-hidden="true">
    @break
    @case('spread-new-ideas')
    <img src="@asset('images/patches/spread-new-ideas.svg')" alt="Spread new ideas patch" aria-hidden="true">
    @break
    @case('think-positive')
    <img src="@asset('images/patches/think-positive.svg')" alt="Think positive patch" aria-hidden="true">
    @break
    @case('together-is-better')
    <img src="@asset('images/patches/together-is-better.svg')" alt="Together is better patch" aria-hidden="true">
    @break
    @case('best-rate-here')
      <img src="@asset('images/patches/best-rate-here.svg')" alt="Best rate here patch" aria-hidden="true">
    @break
    @case('coming-soon')
      <img src="@asset('images/patches/coming-soon.svg')" alt="Coming soon patch" aria-hidden="true">
      @break
    @case('dont-send-ass')
      <img src="@asset('images/patches/dont-send-ass.svg')" alt="Dont send ass patch" aria-hidden="true">
      @break
    @case('free-glass-wine')
    <img src="@asset('images/patches/free-glass-wine.svg')" alt="Free glass of wine patch" aria-hidden="true">
    @break
    @case('too-good')
      <img src="@asset('images/patches/too-good.svg')" alt="Too Good patch" aria-hidden="true">
      @break
    @case('last-minute')
      <img src="@asset('images/patches/last-minute.svg')" alt="Last Minute patch" aria-hidden="true">
      @break
    @case('nospam')
      <img src="@asset('images/patches/nospam.svg')" alt="Shit patch" aria-hidden="true">
      @break
    @case('shit')
      @if (!$animated)
        <img src="@asset('images/patches/shit.svg')" alt="Shit patch" aria-hidden="true">
      @else
        <div id="js-lottie-shit"></div>
      @endif
      @break
    @case('sleep')
      @if (!$animated)
        <img src="@asset('images/patches/sleep.svg')" alt="Shit patch" aria-hidden="true">
      @else
        <div id="js-lottie-sleep"></div>
      @endif
      @break
    @case('together-is-better')
      <img src="@asset('images/patches/together-is-better.svg')" alt="Together is better patch" aria-hidden="true">
      @break
    @case('change-the-world')
      <img src="@asset('images/patches/change-the-world.svg')" alt="Together is better patch" aria-hidden="true">
      @break
    @case('laundry')
      <img src="@asset('images/patches/laundry.svg')" alt="Don't be dirty patch" aria-hidden="true">
      @break
    @default
  @endswitch
</div>
