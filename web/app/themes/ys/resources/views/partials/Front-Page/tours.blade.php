<section class="section section--tours">
  @component('components.6-columns')
    <header class="section__header">
      <h2 class="section__title"><span class="section__title-desc">{{ _x('Tour & Experience', 'corporate@front-page', 'ys') }}</span>{{ _x('Non fartelo raccontare!', 'corporate@front-page', 'ys') }}</h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">

    <div class="tabs js-tabs">
      @component('components.4-columns')
        <nav class="tabs__nav is-marginless">
          <ul class="tabs__list">
            <li class="tabs__item tabs__item--active">
              <a href="#rome-events" role="tab" aria-controls="rome-events" aria-selected="true">{{ __('Roma', 'ys') }}</a>
            </li>
            <li class="tabs__item">
              <a href="#milan-events" role="tab" aria-controls="milan-events" aria-selected="false">{{ __('Milano', 'ys') }}</a>
            </li>
            <!--
            <li class="tabs__item">
              <a href="#florence-events" role="tab" aria-controls="florence-events" aria-selected="false">{{ __('Firenze', 'ys') }}</a>
            </li>
          -->
          </ul>
        </nav><!-- /tabs__nav -->
      @endcomponent
    </div><!-- /.tabs -->
    {{--
      Rome
    --}}
    <div id="rome-events" class="tabs-content tabs-content--active tabs-content--shown" role="tabpanel">
      @component('components.carousel-full', [
      {{--
        'class' => 'carousel--tours carousel--has-patch',
        'link' => get_permalink(get_option('page_for_posts')),
        'label' => __('Guarda il calendario di Roma', 'ys'),
      --}}
      ])
        @foreach ($events_rome as $item)
          <div class="carousel__slide swiper-slide">
            @include('partials.cards.card-event')
          </div><!-- /.carousel__slide -->
        @endforeach
      @endcomponent
    </div><!-- /.tabs-content -->
    {{--
      Milan
    --}}
    <div id="milan-events" class="tabs-content" role="tabpanel">
      @component('components.carousel-full', [
        {{--
        'class' => 'carousel--tours carousel--has-patch',
        'link' => get_permalink(get_option('page_for_posts')),
        'label' => __('Guarda il calendario di Milano', 'ys'),
        --}}
      ])
        @foreach ($events_milan as $item)
          <div class="carousel__slide swiper-slide">
            @include('partials.cards.card-event')
          </div><!-- /.carousel__slide -->
        @endforeach
      @endcomponent
    </div><!-- /.tabs-content -->
    {{--
      Florence
    --}}
    <div id="florence-events" class="tabs-content" role="tabpanel">
      @component('components.carousel-full', [
        {{--
        'class' => 'carousel--tours carousel--has-patch',
        'link' => get_permalink(get_option('page_for_posts')),
        'label' => __('Guarda il calendario di Firenze', 'ys'),
        --}}
      ])
        @foreach ($events_rome as $item)
          <div class="carousel__slide swiper-slide">
            @include('partials.cards.card-event')
          </div><!-- /.carousel__slide -->
        @endforeach
      @endcomponent
    </div><!-- /.tabs-content -->

  </div><!-- /.section__main -->
</section><!-- /.section -->
