<section class="newsletter">
  @component('components.container')
    @include(
      'partials.patch',
      [
        'class' => 'patch--rotate-left',
        'type' => 'nospam',
      ]
    )
  @endcomponent
  @component('components.8-columns')
    <header class="newsletter__header">
      <div class="newsletter__title">{!! _x('Abbiamo offerte, news e party scandalosi che possono risolverti la giornata. Rimaniamo in contatto!', 'newsletter', 'ys') !!}</div>
    </header><!-- /.newsletter__header -->
  @endcomponent
  @component('components.4-columns')
    <form class="newsletter__form form">
      <fieldset class="form__fieldset">
        <legend>{{ _x('Voglio conoscere', 'newsletter', 'ys') }}</legend>
        <div class="newsletter__options">
          <label class="checkbox">
            <input type="checkbox" class="checkbox__input" />
            <span class="checkbox__label">{{ _x('News & Offerte', 'newsletter', 'ys') }}</span>
          </label>
          <label class="checkbox">
            <input type="checkbox" class="checkbox__input" />
            <span class="checkbox__label">{{ _x('Eventi & Attività', 'newsletter', 'ys') }}</span>
          </label>
        </div><!-- /.newsletter__options -->
        <div class="form__field form__field--has-submit-inside">
          <input class="control" type="email" name="email" placeholder="{{ __('La tua email', 'ys') }}" />
          <button type="submit"><i class="icon ys-arrowright"></i></button>
        </div><!-- /.form__field -->
      </fieldset>
    </form><!-- /.newsletter__form -->
  @endcomponent
</section><!-- /.newsletter -->
