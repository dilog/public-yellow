<section class="section section--full-width-mobile section--offers">
  @component('components.6-columns')
    <header class="section__header mb-0">
      <h2 class="section__title"><span class="section__title-desc is-hidden-mobile">{{ _x('Offerte speciali', 'corporate@front-page', 'ys') }}</span><span class="is-hidden-mobile">{{ _x('Cosa non puoi perderti', 'corporate@front-page', 'ys') }}</span><span class="is-hidden-tablet">{{ _x('Offerte speciali', 'corporate@front-page', 'ys') }}</span></h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="section__main">
    @component('components.carousel-full', [
      'class' => 'carousel--offers carousel--has-patch',
    ])
      @foreach ($offers as $item)
        <div class="carousel__slide swiper-slide">
          @include('partials.cards.card-offer-with-image')
        </div><!-- /.carousel__slide -->
      @endforeach
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section--offers -->
