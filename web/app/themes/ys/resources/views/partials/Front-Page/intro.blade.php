<section class="intro">

  <div class="intro__heading">
    <p class="is-hidden-mobile h5">{{ _x('Siamo viaggiatori e party lovers. Amici, artisti e aspiranti chef', 'corporate@front-page', 'ys') }}<br>{{ _x('che dormono poco e sognano tanto. Siamo una famiglia.', 'corporate@front-page', 'ys') }}<br>{{ _x('Siamo lo Yellow.', 'corporate@front-page', 'ys') }}</p>
    <p class="is-hidden-tablet h3">{{ _x('Siamo viaggiatori che dormono poco e sognano tanto. Siamo lo Yellow.', 'corporate@front-page', 'ys') }}</p>
  </div>
  <div class="intro__features">
    <div class="intro__features-items">

      <div class="feature">
        <div class="feature__header-mobile">
          Eat & Party
          <h2 class="feature__title">Il <span class="band">bar</span> cuore dello Yellow.</h2>
        </div>
        <div class="feature__media">
          @component('components.carousel-card', ['shadow' => true])
            @foreach ($ep_gallery as $picture)
              <div class="carousel__slide swiper-slide">
                <figure class="carousel__image carousel__image--portrait">
                  <picture>
                    @foreach ($picture['src'] as $src)
                      <source
                        data-srcset="{{$src['desktop']['1x']}}"
                        media="(min-width: 769px)"
                      >
                      <source
                        data-srcset="{{$src['mobile']['1x']}}"
                        media="(max-width: 768px)"
                      >
                      <img data-src="{{$src['desktop']['1x']}}" alt="" class="swiper-lazy" />
                    @endforeach
                  </picture>
                  <div class="swiper-lazy-preloader"></div>
                  <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> {{$picture['caption']}}</figcaption>
                </figure>
              </div><!-- /.carousel__slide -->
            @endforeach
          @endcomponent
        </div><!-- /.feature__media -->
        <div class="feature__main">
          <div class="feature__preheader is-hidden-mobile">Eat & Party</div>
          <div class="feature__header">
            <h2 class="feature__title">Il <span class="band">bar</span> cuore dello Yellow.</h2>
          </div>
          <div class="feature__content">
            <p class="is-hidden-tablet">
              {{ _x('Fai festa con noi e scopri musica da ogni parte del mondo.', 'corporate@front-page', 'ys') }}
            </p>
            <p class="is-hidden-mobile">
              {{ _x('Cantiamo a squarciagola, scopriamo musica da ogni parte del mondo e siamo sempre gli ultimi a salutare il barman.', 'corporate@front-page', 'ys') }}
            </p>
          </div>
          {{--
          <div class="feature__action">
            <a class="magic-button-outline" href="#">{{ __('Dimmi di più', 'ys') }}</a>
          </div>
          --}}
        </div><!-- /.feature__main -->
      </div><!-- /.feature -->

      <div class="feature feature--reverse">
        <div class="feature__header-mobile">
          Tour & Experience
          <h2 class="feature__title">Tour <span>turistici</span>?<br>Con noi conosci la città come un local.</h2>
        </div>
        <div class="feature__media">
          @component('components.carousel-card', ['shadow' => true])
            @foreach ($te_gallery as $picture)
            <div class="carousel__slide swiper-slide">
              <figure class="carousel__image carousel__image--portrait">
                <picture>
                  @foreach ($picture['src'] as $src)
                    <source
                      data-srcset="{{$src['desktop']['1x']}}"
                      media="(min-width: 769px)"
                    >
                    <source
                      data-srcset="{{$src['mobile']['1x']}}"
                      media="(max-width: 768px)"
                    >
                    <img data-src="{{$src['desktop']['1x']}}" alt="" class="swiper-lazy" />
                  @endforeach
                </picture>
                <div class="swiper-lazy-preloader"></div>
                <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> {{$picture['caption']}}</figcaption>
              </figure>
            </div><!-- /.carousel__slide -->
            @endforeach
          @endcomponent
        </div><!-- /.feature__media -->
        <div class="feature__main">
          <div class="feature__preheader is-hidden-mobile">Tour & Experience</div>
          <div class="feature__header">
            <h2 class="feature__title">Tour <span>turistici</span>?<br>Con noi conosci la città come un local.</h2>
          </div>
          <div class="feature__content">
            <p class="is-hidden-tablet">
              {{ _x('Ci piace esplorare la città come veri pionieri. Apriamo i chakra a colpi di yoga e impariamo a cucinare come le nonne italiane.', 'corporate@front-page', 'ys') }}
            </p>
            <p class="is-hidden-mobile">
              {{ _x('Ci piace esplorare la città come veri pionieri. Apriamo i chakra a colpi di yoga e impariamo a cucinare come le nonne italiane.', 'corporate@front-page', 'ys') }}
            </p>
          </div>
          {{--
          <div class="feature__action">
            <a class="magic-button-outline" href="#">{{ __('Scopri di più', 'ys') }}</a>
          </div>
          --}}
        </div><!-- /.feature__main -->
      </div><!-- /.feature -->

      <div class="feature">
        <div class="feature__header-mobile">
          Work & Meet
          <h2 class="feature__title"><span>Work?</span> Co-work! Uno spazio per lavorare creare connessioni.</h2>
        </div>
        <div class="feature__media">
          @component('components.carousel-card', ['shadow' => true])
            @foreach ($wm_gallery as $picture)
            <div class="carousel__slide swiper-slide">
              <figure class="carousel__image carousel__image--portrait">
                <picture>
                  @foreach ($picture['src'] as $src)
                    <source
                      data-srcset="{{$src['desktop']['1x']}}"
                      media="(min-width: 769px)"
                    >
                    <source
                      data-srcset="{{$src['mobile']['1x']}}"
                      media="(max-width: 768px)"
                    >
                    <img data-src="{{$src['desktop']['1x']}}" alt="Yellow Square" class="swiper-lazy" />
                  @endforeach
                </picture>
                <div class="swiper-lazy-preloader"></div>
                <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> {{$picture['caption']}}</figcaption>
              </figure>
            </div><!-- /.carousel__slide -->
            @endforeach
          @endcomponent
        </div><!-- /.feature__media -->
        <div class="feature__main">
          <div class="feature__header">
            <div class="feature__preheader is-hidden-mobile">Work & Meet</div>
            <h2 class="feature__title"><span>Work?</span> Co-work! Uno spazio per lavorare creare connessioni.
</h2>
          </div>
          <div class="feature__content">
            <p class="is-hidden-tablet">{{ _x('Incontra persone nuove ogni giorno e fai crescere le tue idee.', 'corporate@front-page', 'ys') }}</p>
            <p class="is-hidden-mobile">{{ _x('Crediamo che le migliori idee nascano dagli incontri più insoliti, connessi con il mondo e senza bisogno di translator.', 'corporate@front-page', 'ys') }}</p>
          </div>
          {{--
          <div class="feature__action">
            <a class="magic-button-outline" href="#">{{ __('Scopri di più', 'ys') }}</a>
          </div>
          --}}
        </div>
      </div><!-- /.feature -->

    </div><!-- /.intro__features-item -->
  </div><!-- /.intro__features -->
  <div class="heading heading--has-top-wobble-arrow">
    <div class="heading__container">
      <div class="columns is-centered is-gapless">
        <div class="column is-10">
          <span class="is-hidden-mobile">{{ _x('E poi dormiamo. Qualche volta. ', 'corporate@front-page', 'ys') }}</span>
          <span class="is-hidden-tablet">{{ _x('Ah dimenticavamo!', 'corporate@front-page', 'ys') }}<br>{{ _x('Si può anche dormire!', 'corporate@front-page', 'ys') }}</span>
        </div>
      </div>
    </div>
  </div><!-- /.heading -->

  <div class="destinations">
    <div class="destinations__container">
      @include('partials.patch', ['type' => 'sleep', 'animated' => true])
      @include('partials.carousel-destinations')
    </div><!-- /.destinations__container -->
  </div><!-- /.destinations -->

  <div class="marquee marquee--quotes">
    <div class="marquee__tape">
      @foreach ([1, 2] as $item)
        <div class="marquee__segment">
          <span>100K tacos mangiati</span>
          <span>5000 palloncini gonfiati</span>
          <span>250 match di beer pong giocati</span>
          <span>500 karaoke singalong</span>
          <span>10K selfie inguardabili</span>
          <span>4K band passate sul palco</span>
          <span>1 tonnellata di fettucine cucinate</span>
          <span>300 canzoni stonate</span>
          <span>Altrettante perse</span>
          <span>200 scarpe perse</span>
          <span>150K condom usati</span>
          <span>2 bambini concepiti</span>
          <span>12K abbracci di arrivederci</span>
          <span>5k ore di sonno perse</span>
          <span>500 posizioni di yoga</span>
          <span>350K di cannucce di plastica risparmiate</span>
          <span>1M di “Ciao Amo’ detti</span>
        </div>
      @endforeach
    </div>
  </div>

</section><!-- /.intro -->
