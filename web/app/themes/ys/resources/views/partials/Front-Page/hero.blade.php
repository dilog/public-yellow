<section class="hero hero--corporate">
  <video class="hero__video" autoplay loop muted playsinline>
    @if (isset($video['mp4']))
      <source src="{{$video['mp4']}}" type="video/mp4">
    @endif
    @if (isset($video['webm']))
      <source src="{{$video['webm']}}" type="video/webm">
    @endif
  </video><!-- /.hero__video -->
  <div class="hero__layer">
    <header class="hero__header">
      <div class="hero__title">
        <span>{{ _x('Ostello?', 'corporate@front-page', 'ys') }}</span><br>
        <span>{{ _x('Forse cercavi lo Yellow.', 'corporate@front-page', 'ys') }}</span>
      </div><!-- /.hero__title -->
      <div class="hero__subtitle">
        <a data-fancybox href="{{$video['mp4']}}" role="button" target="_blank">{{ _x('Guarda il video', 'corporate@front-page', 'ys') }}</a>
      </div><!-- /.hero__subtitle -->
    </header><!-- /.hero__header -->
    <div class="hero__content">
      @component('components.10-columns')
        @include('partials.booking')
      @endcomponent
    </div><!-- /.hero__content -->
  </div>
</section><!-- /.hero -->
