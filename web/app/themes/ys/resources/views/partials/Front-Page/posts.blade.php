<section class="section section--full-width-mobile section--posts">
  @component('components.6-columns')
    <header class="section__header">
      <h2 class="section__title"><span class="section__title-desc">{{ _x('Blog', 'corporate@front-page', 'ys') }}</span>{{ _x('Zitti tutti. Parlano i locals!', 'corporate@front-page', 'ys') }}</h2>
      @include('partials.patch', ['type' => 'shit', 'animated' => true])
    </header><!-- /.section__header -->
  @endcomponent

  <div class="section__main">
    @component('components.carousel-full', [
      'class' => 'carousel--posts',
      'link' => get_permalink(get_option('page_for_posts')),
      'label' => __('Leggi il blog', 'ys'),
    ])
      @foreach ($recent_posts as $item)
        <div class="carousel__slide swiper-slide">
          @include('partials.cards/card-'.$item['post_type'])
        </div><!-- /.carousel__slide -->
      @endforeach
    @endcomponent
  </div><!-- /.section__main -->
</section><!-- /.section--posts -->
