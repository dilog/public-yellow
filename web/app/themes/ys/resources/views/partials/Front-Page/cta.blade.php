<section class="cta cta--blue cta--has-patch">
  <div class="cta__container">
    <header class="cta__header">
      <h3 class="cta__title">{{ _x('Viaggi con piu di xx persone?', '', 'ys') }}<br>{{ _x('Ci sono offerte per te!', '', 'ys') }}</h3>
    </header><!-- /.cta__header -->
    <div class="cta__actions">
      <a href="{{ $groups['permalink'] }}" class="button">{{ __('Booka il tuo gruppo', 'ys') }}</a>
    </div><!-- /.cta__actions -->
    @include(
      'partials.patch',
      [
        'class' => 'patch--rotate-left',
        'type' => 'together-is-better',
      ]
    )
  </div><!-- /.cta__container -->
</section><!-- /.cta -->
