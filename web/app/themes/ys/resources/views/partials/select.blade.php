<select class="ys-select"{{ (isset($filter)) ? " data-filter=" . $filter : null}}>
  <option value="{{ $value }}">{{ $label }}</option>
  @foreach ($options as $option)
    <option @if ($selected === $option['value']) selected @endif value="{{ $option['value'] }}">{{ $option['name'] }}</option>
  @endforeach
</select>
