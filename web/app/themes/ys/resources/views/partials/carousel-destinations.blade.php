{{--
  //
  // Destinations carousel
  //
--}}
<div id="js-carousel-destinations" class="carousel carousel--destinations swiper-container">
  <div class="carousel__wrapper swiper-wrapper">
    {{-- Rome --}}
    <div class="carousel__slide swiper-slide">
      <a href="{{ $sites['rome'] }}" class="destination-cover">
        <figure class="destination-cover__media">
          <img class="destination-cover__image js-lazy" data-src="@asset('images/corporate/destinations/ys-rome.jpg')" data-srcset="@asset('images/corporate/destinations/ys-rome@2x.jpg') 2x, @asset('images/corporate/destinations/ys-rome.jpg') 1x" alt="{{_x('Yellow Square Roma', 'img-alt', 'ys')}}" />
        </figure>
        <div class="destination-cover__content">
          <span class="destination-cover__title">{{ __('Roma', 'ys') }}</span>
        </div>
      </a><!-- /.destination-cover -->
    </div><!-- /.carousel__slide -->
    {{-- Milan --}}
    <div class="carousel__slide swiper-slide">
      <a href="{{ $sites['milan'] }}" class="destination-cover">
        <figure class="destination-cover__media">
          <img class="destination-cover__image js-lazy" data-src="@asset('images/corporate/destinations/ys-milan.jpg')" data-srcset="@asset('images/corporate/destinations/ys-milan@2x.jpg') 2x, @asset('images/corporate/destinations/ys-milan.jpg') 1x" alt="{{_x('Yellow Square Milano', 'img-alt', 'ys')}}" />
        </figure>
        <div class="destination-cover__content">
          <span class="destination-cover__title">{{ __('Milano', 'ys') }}</span>
        </div>
      </a><!-- /.destination-cover -->
    </div><!-- /.carousel__slide -->
  </div>
</div><!-- /.carousel -->
