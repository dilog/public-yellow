<div class="carousel js-carousel-posts swiper-container {{$class}}">
  <div class="carousel__wrapper swiper-wrapper">
    @foreach ($data as $item)
      <div class="carousel__slide swiper-slide">
        @include('partials.cards/card-'.$item['post_type'])
      </div><!-- /.carousel__slide -->
    @endforeach
    @php wp_reset_postdata() @endphp
  </div><!-- /.carousel__wrapper -->
  <div class="carousel__footer">
    <div class="carousel__container">
      @if ($link)
        <a class="carousel__link" href="{{ $link ? $link : __('Clicca qui', 'ys') }}">{{ $label }}</a>
      @endif
      <nav class="carousel__nav">
        <button role="button" class="carousel__button carousel__button--prev">
          <span class="icon ys-arrowleft"></span>
        </button>
        <button role="button" class="carousel__button carousel__button--next">
          <span class="icon ys-arrowright"></span>
        </button>
      </nav>
    </div>
  </div><!-- /.carousel__footer -->

</div><!-- /.carousel -->
