<section class="section section--posts">
  <header class="section__header">
    <div class="section__container">
      <h2 class="section__title"><span class="section__title-desc">{{ _x('Blog', 'corporate@front-page', 'ys') }}</span>{{ _x('Zitti tutti. Parlano i locals!', 'corporate@front-page', 'ys') }}</h2>
    </div>
  </header><!-- /.section__header -->
  <div class="section__main">
    <div class="carousel carousel--posts js-carousel-posts swiper-container">
      <div class="carousel__wrapper swiper-wrapper">
        @foreach ([1, 2, 3, 4, 5] as $item)
          <div class="carousel__slide swiper-slide">
            <article @php post_class('card card--post') @endphp>
              <a href="#" >
                <figure class="card__media">
                  <img class="card__image" src="https://placehold.it/405x265" alt="" />
                </figure>
                <div class="card__main">
                  <header class="card__header">
                    <span class="card__title-desc">How to</span><h2 class="card__title entry-title">Come vedere gratis i migliori monumenti d’Italia.</h2>
                  </header>
                </div><!-- /.card__main -->
                <div class="card__footer">
                  <div class="byline author vcard"><img src="https://placehold.it/34x34" alt="" class="author__avatar">{{ get_the_author() }}</div>
                  <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
                </div>
              </a><!-- /.card -->
            </article>
          </div><!-- /.carousel__slide -->
        @endforeach
      </div><!-- /.carousel__wrapper -->
      <div class="carousel__footer">
        <div class="carousel__container">
          <a class="carousel__link" href="#">{{ __('Leggi il blog', 'ys') }}</a>
          <nav class="carousel__nav">
            <button role="button" class="carousel__button carousel__button--prev">
              <span class="icon ys-arrowleft"></span>
            </button>
            <button role="button" class="carousel__button carousel__button--next">
              <span class="icon ys-arrowright"></span>
            </button>
          </nav>
        </div>
      </div><!-- /.carousel__footer -->

    </div><!-- /.carousel -->
  </div><!-- /.section__main -->
</section><!-- /.section--offers -->
