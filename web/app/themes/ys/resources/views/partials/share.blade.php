<div class="share {{ isset($version) ? 'v' . $version : null }}">
  @if(!isset($version))
  <span class="share__label">{{ __('Share', 'ys') }}</span>
  @endif
  <ul class="share__list">
    <li class="share__item">
      <a href="#" rel="noopener" name="{{ __('Share by Facebook', 'ys-meta') }}"><i class="icon ys-facebook"></i></a>
    </li>
    <li class="share__item">
      <a href="#" rel="noopener" name="{{ __('Share by Messenger', 'ys-meta') }}"><i class="icon ys-messenger"></i></a>
    </li>
    <li class="share__item">
      <a href="#" rel="noopener" name="{{ __('Share by Mail', 'ys-meta') }}"><i class="icon ys-mail"></i></a>
    </li>
    <li class="share__item">
      <a href="#" rel="noopener" name="{{ __('Share by Linkedin', 'ys-meta') }}"><i class="icon ys-linkedin"></i></a>
    </li>
    <li class="share__item">
      <a href="#" rel="noopener" name="{{ __('Share by Twitter', 'ys-meta') }}"><i class="icon ys-twitter"></i></a>
    </li>
  </ul>
</div><!-- /.share -->
