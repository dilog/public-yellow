@if (isset($destination_site))
  <div class="topbar">
    <a href="/" title="">
      <img class="" src="@asset('images/logo-yellow-text.svg')" alt="Logo Yellow Square" />
    </a>
    @if (has_nav_menu('topbar_navigation'))
      <nav class="nav-topbar">
        {!! wp_nav_menu(['theme_location' => 'topbar_navigation', 'menu_class' => 'nav']) !!}
      </nav>
    @endif
  </div><!-- /.topbar -->
@endif
<header id="js-banner" class="banner">
  {{--
    Mobile Nav Toggle
  --}}
  <button id="nav-panel-toggle" class="nav-panel-toggle">
    <span class="nav-panel-toggle__close"></span>
    <span></span>
    <span class="nav-panel-toggle__close"></span>
  </button>
  {{--
    Brand
  --}}
  <a class="brand" href="{{ home_url('/') }}" aria-label="{{ _x('Vai alla Home', 'aria', 'ys') }}">
    @if (!isset($destination_site))
      <img class="brand__image" src="@asset('images/logo-yellow.svg')" alt="Logo Yellow Square" />
      <img class="brand__image-white" src="@asset('images/logo-yellow-white.svg')" alt="Logo Yellow Square" />
    @endif
    @if (isset($destination_site) && $destination_site === 'rome')
     <img class="brand__image" src="@asset('images/logo-yellow-rome.svg')" alt="Logo Yellow Square Roma" />
    @endif
    @if (isset($destination_site) && $destination_site === 'milan')
      <img class="brand__image" src="@asset('images/logo-yellow-milan.svg')" alt="Logo Yellow Square Milano" />
    @endif
  </a>
  {{--
    Desktop Destination Nav
  --}}
  @if (has_nav_menu('destination_navigation'))
    <nav class="nav-destination">
      {!! wp_nav_menu(['theme_location' => 'destination_navigation', 'menu_class' => 'nav']) !!}
    </nav>
  @endif
  {{--
    Primary Nav
  --}}
  @if (has_nav_menu('primary_navigation'))
    <nav class="nav-primary">
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav js-nav']) !!}
    </nav>
  @endif
  {{--
    Book Now action
  --}}
  <a id="js-book-now" data-open="book-now" class="book-now" href="#">
    <span class="is-hidden-mobile">{{ __('Prenota', 'ys') }}</span>
    <span class="is-hidden-tablet">{{ __('Prenota un soggiorno', 'ys') }}</span>
  </a>
  {{--
    Mobile Nav Panel
  --}}
  <div id="js-nav-panel" class="nav-panel">
    <div class="nav-panel__block">
      <div class="nav-panel__main">
        <nav class="nav-mobile js-nav">
        @if (has_nav_menu('mobile_navigation'))
          {!! wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'nav nav--vertical', 'container' => false]) !!}
        @endif
        </nav>
      </div>
      <div class="nav-panel__footer">
        @if (has_nav_menu('social_links'))
        {!! wp_nav_menu(['container' => false, 'theme_location' => 'social_links', 'menu_class' => 'nav-social']) !!}
        @endif
      </div>
    </div>
  </div>
</header><!-- /banner -->
{{--
  Mobile Destination Nav
  - Outside Header because is not sticky
--}}
<nav class="nav-destination-mobile">
  @if (has_nav_menu('destination_navigation'))
    {!! wp_nav_menu(['theme_location' => 'destination_navigation', 'menu_class' => 'nav']) !!}
  @endif
</nav><!-- /.nav-destination-mobile -->
