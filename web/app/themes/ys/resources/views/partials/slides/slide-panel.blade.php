<div class="book-now-menu">
  <div class="book-now-menu__close-panel"></div>
  <div class="book-now-menu__panel">
    <div class="book-now-menu__container">
      <div class="book-now-menu__top-panel">
        <span>Prenota</span> <button type="button" class="book-now-menu__close-button">Chiudi</button>
      </div>
      <div class="book-now-menu__body">
        <form action="" class="booking__form">
          <div class="form-group mb-xs">
            <select class="slide-menu__booking_control control" name="booking_destination" required>
              <option value="">Destinazione</option>
              <option value="rome" {{App\is_rome_site() ? 'selected' : ''}}>{{ __('Roma', 'ys') }}</option>
              <option value="milan" {{App\is_milan_site() ? 'selected' : ''}}>{{ __('Milano', 'ys') }}</option>
              <!--<option value="florence">{{ __('Firenze', 'ys') }}</option>-->
            </select>
          </div>
          <div class="form-group">
            <div class="form-group__header">
              <span>Checkin</span>
            </div>
            <input class="hidden-checkin control" min="{{ date('Y-m-d') }}" type="date" name="hidden-checkin" required/>
            <div class="form-group__header mt-s">
              <span>Checkout</span>
            </div>
            <input class="hidden-checkout control" type="date" min="{{ date('Y-m-d') }}" name="hidden-checkout" required/>
            <!--
            <div class="---sidebooking">
              <input id="checkin-slide" type="text" class="booking__control js-datepicker" name="checkin-out" required data-input />
              <a title="toggle" data-toggle class="sidebar-calendar-toggle">
                <div class="input-group">
                  <label for="booking_slide_checkin" class="date-input" id="checkin-dates">
                    <input type="text" name="booking_slide_checkin">
                    <span class="day-of-month"></span>
                    <span class="month-and-year"></span>
                    <span class="day-of-week"></span>
                  </label>
                  <label for="booking_slide_checkout" class="date-input" id="checkout-dates">
                    <input type="text" name="booking_slide_checkout">
                    <span class="day-of-month"></span>
                    <span class="month-and-year"></span>
                    <span class="day-of-week"></span>
                  </label>
                </div>
              </a>
            </div>
          -->
            <div class="input-group nights-input">
              <input id="sidebar-nights" type="text" class="booking__control" name="nights" readonly="">
            </div>
            <div class="input-tip">
              <span class="nights"></span>
            </div>
          </div>
          <div class="form-group">
            <input type="text" name="coupon" placeholder="+ CODICE PROMO" class="control">
          </div>
          <div class="form-group mt-s">
            <button type="submit" class="booking__submit booking__submit--white">Prenota</button>
          </div>
        </form>
      </div>
  </div>
  </div>
</div>
