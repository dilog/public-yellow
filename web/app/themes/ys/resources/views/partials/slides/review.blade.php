<div class="carousel__slide swiper-slide">
  <blockquote class="review-slide">
    <div class="star-rating">
      <span style="width: {{ $item['rating'] }}"></span>
    </div>
    <div class="review-slide__main">
      {{ $item['description'] }}
    </div>
    <footer class="review-slide__footer"><cite>{{ $item['reviewer'] }}</cite></footer>
  </blockquote><!-- /.review-slide -->
</div><!-- /.carousel__slide -->
