<div class="carousel__slide swiper-slide">
  <blockquote class="quote-slide">
    <div class="avatar">
      <div class="avatar__mask">
        {!! $item['image'] !!}
      </div>
    </div>
    <div class="quote-slide__main">
      <p class="quote-slide__content">
        @if ($item['context'])
          <span class="quote-slide__desc">{{ $item['context'] }}:</span>
        @endif
        {{$item['tip']}}
      </p>
      <footer class="quote-slide__cite">{!! $item['tipper'] !!}</footer>
    </div>
  </blockquote><!-- /.quote-slide -->
</div><!-- /.carousel__slide -->
