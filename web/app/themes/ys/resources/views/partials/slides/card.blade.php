<div class="carousel__slide swiper-slide">
  <figure class="carousel__image">
    <img class="swiper-lazy" data-src="{{ $image['src'] }}" alt="{{ $image['alt'] }}" />
    <div class="swiper-lazy-preloader"></div>
    @if (isset($image['caption']) && !empty($image['caption']))
      <figcaption class="carousel__image-caption" data-swiper-parallax="-50%"><i class="icon ys-marker"></i> {{ $image['caption'] }}</figcaption>
    @endif
  </figure>
</div><!-- /.carousel__slide -->
