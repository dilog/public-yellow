<div class="carousel__slide carousel__header swiper-slide">
  <figure>
    <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}" />
  </figure>
</div><!-- /.carousel__slide -->
