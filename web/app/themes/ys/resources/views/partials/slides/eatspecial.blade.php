<div class="carousel__slide carousel__eat-special swiper-slide">
  <div class="text">
    <h2>{!! $item['slide_title'] !!}</h2>
    <div class="wobble-arrow"></div>
    @if($item['button_text']!='')
    <a href="{{ $item['button_url'] }}" class="button button--s">{{ $item['button_text'] }}</a>
    @endif
  </div>
  <figure>
    <img src="{{ $item['slide_image']['url'] }}" alt="{{ $item['slide_image']['alt'] }}" />
  </figure>
</div><!-- /.carousel__slide -->
