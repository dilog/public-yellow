<div
  class="page-header @if (isset($patch) && $position === 'right') page-header--has-right-patch @endif @if (isset($patch) && $position === 'left') page-header--has-left-patch @endif">
  @component('components.6-columns')
    <div class="page-header__content">
      <h1 class="page-header__title">{!! App::title() !!}</h1>
      @if (App::subtitle())
        <p class="page-header__subtitle">{!! App::subtitle() !!}</p>
      @endif
    </div>
    @if (isset($patch))
      <div class="page-header__patch">
        @include('partials.patch', ['type' => $patch, 'animated' => true])
      </div>
    @endif
  @endcomponent
</div><!-- /.page-header -->
