@include(
  'partials.page-header',
  ['patch' => 'shit', 'position' => 'right']
)
{{--
//
// Toolbar
//
--}}
<section class="section section--marginless">
  @component('components.container')
    <div class="blog-toolbar">
      <div id="js-posts-filters" class="blog-toolbar__filters">
        @includeWhen(
          $destination,
          'partials.select',
          array_merge($destination, ['filter' => 'destination'])
        )
        @includeWhen(
          $category,
          'partials.select',
          array_merge($category, ['filter' => 'category'])
        )
      </div>
      <div class="blog-toolbar__nav">
        <div id="js-tabs-blog" class="tabs js-tabs">
          <nav class="tabs__nav">
            <ul class="tabs__list">
              <li class="tabs__item tabs__item--active">
                <a href="#recent-posts" role="tab" aria-controls="recent-posts" aria-selected="true">{!! __("What's new", 'ys') !!}</a>
              </li>
              <li class="tabs__item">
                <a href="#trending-posts" role="tab" aria-controls="trending-posts" aria-selected="false">{!! __("Trending", 'ys') !!}</a>
              </li>
              <li class="tabs__item">
                <a href="#best-of" role="tab" aria-controls="best-of" aria-selected="false">{!! __("Best of", 'ys') !!}</a>
              </li>
            </ul>
          </nav><!-- /tabs__nav -->
      </div>
    </div>
  @endcomponent
</section><!-- /.section -->
{{--
//
// Grid
//
--}}
<section class="section section--marginless">
  @component('components.container')
    {{--
    What's new tab
    --}}
    @if ($blog_posts)
      <div id="recent-posts" class="tabs-content tabs-content--active tabs-content--shown" role="tabpanel">
        <div id="js-grid-blog" class="grid-blog">
          @foreach ($blog_posts as $item)
            @include('partials.cards.card-post')
          @endforeach
        </div>
      </div><!-- /.tabs-content -->
    @endif
    {{--
    Trending tab
    --}}
    @if ($trending_posts)
      <div id="trending-posts" class="tabs-content" role="tabpanel">
        <div class="grid-blog">
          @foreach ($trending_posts as $item)
            @include('partials.cards.card-post')
          @endforeach
        </div>
      </div><!-- /.tabs-content -->
    @endif
    {{--
    Best of tab
    --}}
    @if ($best_of_posts)
      <div id="best-of" class="tabs-content" role="tabpanel">
        <div class="grid-blog">
          @foreach ($best_of_posts as $item)
            @include('partials.cards.card-post')
          @endforeach
        </div>
      </div><!-- /.tabs-content -->
    @endif
  @endcomponent
</section><!-- /.section -->
{{--
//
// Authors carousel
//
--}}
<section class="section section--authors">
  @component('components.container')
    <header class="section__header">
      <h4 class="section__title">{{ __('I nostri autori', 'ys') }}</h4>
    </header>
    <div class="section__main">
      <div class="carousel carousel--authors">
        <div class="carousel__container swiper-container js-carousel-authors">
          <div class="carousel__wrapper swiper-wrapper">
            @foreach ($authors as $author)
              <div class="carousel__slide swiper-slide">
                <div class="author-slide">
                  <div class="author-slide__avatar">
                    <div class="author-slide__avatar-mask">
                      <img class="author-slide__image" src="{{ $author['avatar']['thumb'] }}" alt="{{ $author['avatar']['alt'] }}" />
                    </div>
                  </div>
                  <p class="author-slide__description">{{ $author['description'] }}</p>
                  <p class="author-slide__fullname">{{ $author['fullname'] }}</p>
                </div>
              </div><!-- /.carousel__slide -->
            @endforeach
          </div><!-- /.carousel__wrapper -->
        </div><!-- /.carousel__container -->
        <nav class="carousel__nav">
          <button role="button" class="carousel__button carousel__button--prev">
            <span class="icon ys-arrowleft"></span>
          </button>
          <button role="button" class="carousel__button carousel__button--next">
            <span class="icon ys-arrowright"></span>
          </button>
        </nav>
        <div class="carousel__bullets"></div>
      </div><!-- /.carousel -->
    </div><!-- /.section__main -->
  @endcomponent
</section><!-- /.section--authors -->
