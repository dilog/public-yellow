{{--
//
// Offer card
//
--}}
<a href="{{ $item['permalink'] }}" class="{{ $item['class'] }}">
  <div class="card__main">
    <div class="card__top">
      <span class="tag tag--position"><i class="icon ys-marker"></i> {{ __($item['destination'], 'ys') }}</span>
    </div>
    <h3 class="card__title">{!! $item['title'] !!}</h3>
    <div class="card__content">
      {{ $item['subtitle'] }}
    </div>
  </div>
  @includeWhen(
    $item['patch'],
    'partials.patch',
    [
      'type' => $item['patch'],
      'visible_mobile' => true,
    ]
  )
</a><!-- /.card -->
