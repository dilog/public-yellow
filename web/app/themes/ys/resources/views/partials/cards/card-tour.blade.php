
@foreach ($tours as $tour)
<div class="column is-4">
  <article class="card card--event {{ $class }}">
    <a class="card__block" href="{{ $tour['permalink'] }}" >
      <figure class="card__media">
        <img class="card__image" src="{{ $tour['thumbnail'] }}" alt="" />
        @if($tour['address'])
        <figcaption class="carousel__image-caption" data-swiper-parallax="-50%" style="transform: translate3d(0%, 0px, 0px);"> <i class="icon ys-marker"></i> {{$tour['address']}}</figcaption>
        @endif
      </figure><!-- /.card__media -->
      <div class="card__main">
        <div class="card__top">
          <div class="event-category">{{ $tour['category'] }}</div>
          <div class="event-duration">
            <i class="icon ys-time"></i> {{ __('Durata', 'ys').' '. $tour['duration']}}
          </div>
        </div>
        <header class="card__header">
          <a href="{{ $tour['permalink'] }}"><h2 class="card__title entry-title">{{ $tour['title'] }}</h2></a>
        </header>
        <footer class="card__footer">
          <div class="event-days">{{ $tour['availability'] }}</div>
          <div class="event-duration">
            <i class="icon ys-time"></i> {{ sprintf('%s %s %s', __('Durata', 'ys'), $tour['duration'], _n('ora', 'ore', $item['duration'], 'ys')) }}
          </div>
        </footer><!-- /.card__footer -->
      </div><!-- /.card__main -->
      @includeWhen(
        $tour['patch'],
        'partials.patch',
        [
          'type' => $tour['patch']['patch'],
          'visible_mobile' => true,
        ]
      )
    </a><!-- /.card__block -->
  </article><!-- /.card -->
</div>
@endforeach
