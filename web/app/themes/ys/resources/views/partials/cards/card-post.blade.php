{{--
//
// Post card
//
--}}
<article class="{{ $item['class'] }}">
  <a class="card__block" href="{{ $item['permalink'] }}" rel="bookmark">
    <figure class="card__media">
      {!! $item['thumbnail'] !!}
    </figure>
    <div class="card__main">
      <header class="card__header">
        <span class="card__title-desc">{{ $item['category']['title']}}</span><h2 class="card__title entry-title">{{$item['title']}}</h2>
      </header>
      {{-- Show summary only on big post cards. --}}
      @if ($item['is_big'])
        <div class="card__content">
          <p class="entry-summary">{{ $item['excerpt']}}</p>
        </div>
      @endif
      <div class="card__footer">
        <div class="byline author vcard">
          <img src="{{ $item['author']['avatar'] }}" alt="Avatar {{ $item['author']['fullname'] }}" class="author__avatar author__avatar--small photo">
          <span class="fn">{{ $item['author']['shortname'] }}</span>
        </div>
        <time class="updated" datetime="{{ $item['datetime'] }}">{{ $item['datetime_hr'] }}</time>
      </div><!-- /.card__footer -->
    </div><!-- /.card__main -->
  </a>
</article><!-- /.card -->
