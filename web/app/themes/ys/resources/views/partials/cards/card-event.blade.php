{{--
//
// Events card
//
--}}
<article class="{{ $item['class'] }}">
  <a class="card__block" href="{{ $item['url'] }}" target="_blank">
    <figure class="card__media">
      {!! $item['thumbnail'] !!}
    </figure><!-- /.card__media -->
    <div class="card__main">
      <div class="card__top">
        <div class="event-category">{{ $item['category'] }}</div>
        @if($item['duration'])
          <div class="event-duration">
            <i class="icon ys-time"></i> {{ sprintf('%s %s %s', __('Durata', 'ys'), $item['duration'], _n('ora', 'ore', $item['duration'], 'ys')) }}
          </div>
        @endif
      </div><!-- /.card__top -->
      <header class="card__header">
        <h2 class="card__title p-name">{!! $item['title'] !!}</h2>
      </header><!-- /.card__header -->
      <footer class="card__footer">
        <div class="event-date">{{ $item['date']['start'] }}</div>
        <div class="event-time">
          <time class="dt-start" datetime="{{ $item['datetime']['start'] }}">{{ $item['time']['start'] }}</time>/<time class="dt-end" datetime="{{ $item['datetime']['end'] }}">{{ $item['time']['end'] }}</time>
        </div><!-- /.card__footer -->
      </footer><!-- /.card__footer -->
    </div><!-- /.card__main -->
  </a><!-- /.card__block -->
  @includeWhen(
    $item['patch'],
    'partials.patch',
    [
      'type' => $item['patch'],
      'visible_mobile' => true,
    ]
  )
</article><!-- /.card -->
