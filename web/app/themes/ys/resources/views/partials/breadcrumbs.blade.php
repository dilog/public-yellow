
@if (function_exists('yoast_breadcrumb'))
  <div class="breadcrumbs">
    @php yoast_breadcrumb() @endphp
  </div>
@endif
