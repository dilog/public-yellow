<div class="offices">
  @component('components.container')
    <div class="columns">
      <div class="column">
        <div class="offices__item">
          <img class="offices__city" width="90" src="@asset('images/elements/ys-rome.png')" alt="Yellow Square Rome">
          <p>Via Palestro, 51<br>00185 Rome<br>palestro@yellowsquare.it<br>+39 064463554</p>
        </div>
      </div>
      <div class="column">
        <div class="offices__item">
          <img class="offices__city" width="106" src="@asset('images/elements/ys-milan.png')" alt="Yellow Square Milan">
          <p>Via S. Lattuada, 14<br>20135 Milan<br>lattuada@yellowsquare.it<br>+39 0282396603</p>
        </div>
      </div>
      <div class="column">
        <div class="offices__item">
          <img class="offices__city" width="157" src="@asset('images/elements/ys-florence.png')" alt="Yellow Square Milan">
          <!--<p>Viale F. Redi, 19<br>50144 Florence<br>redi@yellowsquare.it<br>+39 064463554</p>-->
            <p>Coming Soon</p>
        </div>
      </div>
    </div><!-- /.columns -->
  @endcomponent
</div><!-- /.offices -->

<section class="whatsapp">
  <div class="whatsapp__container">
    <div class="patch patch--rotate-left" aria-hidden="true">
      <img src="@asset('images/patches/dont-send-ass-pink.svg')" alt="No Spam">
    </div>
    <span class="whatsapp__label">{{ __('Numero whatsapp', 'ys') }}</span> <a class="whatsapp__phone" href="https://wa.me/00390649382682" target="_blank" rel="noopener"><img src="@asset('images/elements/flag-ita.svg')" alt="">+39 06 49382682</a>
  </div>
</section>

<footer class="content-info">
  <div class="container">
    <div class="footer-navs">
      <div class="footer-nav">
        <p class="footer-nav__header is-hidden-mobile">
          {{ _x('Esplora', 'corporate@footer')}}
        </p>
        <button class="js-dropdown-footer footer-nav__header is-hidden-tablet">
          {{ _x('Esplora', 'corporate@footer')}}
          <i class="icon ys-caret"></i>
        </button>
        @if (has_nav_menu('footer_1_navigation'))
          {!! wp_nav_menu(['container' => false, 'theme_location' => 'footer_1_navigation', 'menu_class' => 'footer-nav__list']) !!}
        @endif
      </div><!-- /.footer__nav -->
      <div class="footer-nav">
        <p class="footer-nav__header is-hidden-mobile">
          {{ _x('Informazioni', 'corporate@footer')}}
        </p>
        <button class="js-dropdown-footer footer-nav__header is-hidden-tablet">
          {{ _x('Informazioni', 'corbuttonorate@footer')}}
          <i class="icon ys-caret"></i>
        </button>
        @if (has_nav_menu('footer_2_navigation'))
          {!! wp_nav_menu(['container' => false, 'theme_location' => 'footer_2_navigation', 'menu_class' => 'footer-nav__list']) !!}
        @endif
      </div><!-- /.footer__nav -->
      <div class="footer-nav">
        <p class="footer-nav__header is-hidden-mobile">
          {{ _x('Legal', 'corporate@footer')}}
        </p>
        <button class="js-dropdown-footer footer-nav__header is-hidden-tablet">
          {{ _x('Legal', 'corporate@footer')}}
          <i class="icon ys-caret"></i>
        </button>
        @if (has_nav_menu('footer_3_navigation'))
          {!! wp_nav_menu(['container' => false, 'theme_location' => 'footer_3_navigation', 'menu_class' => 'footer-nav__list']) !!}
        @endif
      </div><!-- /.footer__nav -->
      <div class="footer-nav">
        <p class="footer-nav__header is-hidden-mobile">
          {{ _x('Connettiti', 'corporate@footer')}}
        </p>
        @if (has_nav_menu('social_links'))
        {!! wp_nav_menu(['container' => false, 'theme_location' => 'social_links', 'menu_class' => 'footer-nav__list nav-social']) !!}
        @endif
      </div><!-- /.footer__nav -->
    </div>
    <div class="colophon">
      <p>Via Palestro,51 - 00185 Roma Tel. +39 064463554 - Fax 064463914 home@bc-hospitality.com C.F. - Part. IVA 11209161006
      </p>
    </div>
  </div>
</footer>

@include('partials.slides.slide-panel')
