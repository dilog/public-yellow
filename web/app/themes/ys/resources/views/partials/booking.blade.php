<div class="booking">
  <div class="booking__bar">
    <span class="booking__patch" aria-hidden="true">
      <img src="@asset('images/patches/best-rate-here.svg')" alt="Best Rate Here patch" />
    </span>
    <form class="booking__form">
      <div class="booking__field">
        <select id="destination" class="booking__control" required name="booking_destination">
          <option value=""></option>
          <option value="rome" {{App\is_rome_site() ? 'selected' : ''}}>{{ __('Roma', 'ys') }}</option>
          <option value="milan" {{App\is_milan_site() ? 'selected' : ''}}>{{ __('Milano', 'ys') }}</option>
          <!--<option value="florence">{{ __('Firenze', 'ys') }}</option>-->
        </select>
        <label class="booking__label" for="destination">{{ __('Destinazione', 'ys') }}</label>
      </div><!-- /.booking__field -->
      <div class="booking__field">
        <input id="checkin" type="text" class="booking__control js-datepicker" name="checkin-out" required />
        <input class="hidden-checkin" type="hidden" name="hidden-checkin"/>
        <input class="hidden-checkout" type="hidden" name="hidden-checkout"/>
        <label class="booking__label" for="checkin-out">{{ __('Check-In ~ Check-Out', 'ys') }}</label>
      </div><!-- /.booking__field -->
      <div class="booking__field">
        <input id="nights" type="text" class="booking__control" name="nights" readonly />
        <label class="booking__label" for="nights">{{ __('Notti', 'ys') }}</label>
      </div><!-- /.booking__field -->
      <button class="booking__submit" type="submit">
        {{ __('Prenota', 'ys') }}
      </button>
    </form><!-- /.booking__form -->
  </div><!-- booking__bar -->
  <div class="booking__options">
    <a class="booking__options-item" href="/booking-di-gruppo/">
      {{ __('Viaggi in gruppo', 'ys') }}
    </a>
    <div id="js-booking-coupon" class="booking__coupon">
      <button class="booking__coupon-trigger" type="button">
        <i class="icon ys-plus"></i> {{ __('Codice promo', '') }}
      </button>
      <div class="booking__coupon-panel">
        <div class="form__field form__field--has-submit-inside">
          <input class="control" type="text" name="coupon" placeholder="{{ __('Text here', 'ys') }}" />
          <button type="submit"><i class="icon ys-arrowright"></i></button>
        </div>
      </div>
    </div>
  </div><!-- booking__options -->
</div><!-- /.booking -->
