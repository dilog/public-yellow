{{--
  Full width Carousel
  - Full Width
  - Prev/Next navigation
  - Optional link
--}}

<div class="carousel carousel--full js-carousel-full swiper-container{{isset($class) ? ' '.$class : '' }}">
  <div class="carousel__wrapper swiper-wrapper">
    {{ $slot }}
  </div><!-- /.carousel__wrapper -->
  <div class="carousel__footer">
    <div class="carousel__container">
      @if (isset($link))
        <a class="carousel__link" href="{{ $link ? $link : __('Clicca qui', 'ys') }}">{{ $label }}</a>
      @endif
      <nav class="carousel__nav">
        <button role="button" class="carousel__button carousel__button--prev">
          <span class="icon ys-arrowleft"></span>
        </button>
        <button role="button" class="carousel__button carousel__button--next">
          <span class="icon ys-arrowright"></span>
        </button>
      </nav><!-- /.carousel__nav -->
    </div><!-- /.carousel__container -->
  </div><!-- /.carousel__footer -->
</div><!-- /.carousel -->
