<div class="carousel carousel--standard{{ isset($namespace) ? " carousel--$namespace" : '' }}">
  <div class="carousel__container swiper-container js-carousel{{ isset($namespace) ? "--$namespace" : '' }}">
    <div class="carousel__wrapper swiper-wrapper">
      {{ $slot }}
    </div><!-- /.carousel__wrapper -->
  </div><!-- /.carousel__container -->
  <nav class="carousel__nav">
    <button id="js-carousel-prev-{{ isset($namespace) ? "$namespace" : '' }}" role="button" class="carousel__button carousel__button--prev">
      <span class="icon ys-arrowleft"></span>
    </button>
    <button id="js-carousel-next-{{ isset($namespace) ? "$namespace" : '' }}" role="button" class="carousel__button carousel__button--next">
      <span class="icon ys-arrowright"></span>
    </button>
  </nav>
  <div id="js-carousel-bullets-{{ isset($namespace) ? "$namespace" : '' }}" class="carousel__bullets"></div>
</div><!-- /.carousel -->
