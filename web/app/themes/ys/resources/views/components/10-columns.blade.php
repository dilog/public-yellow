{{--
  10 Columns
--}}

<div class="container">
  <div class="columns is-gapless is-centered">
    <div class="column is-10">
      {{ $slot }}
    </div>
  </div>
</div>
