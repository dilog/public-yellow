{{--
  Card Carousel
  - Fraction pagination.
  - Prev/Next navigation on hover with custom cursor.
--}}

<div class="carousel carousel--card js-carousel-card swiper-container{{ isset($shadow) && $shadow ? ' carousel--has-shadow' : ''}}">
  <div class="carousel__wrapper swiper-wrapper">
    {{ $slot }}
  </div><!-- /.carousel__wrapper -->
  <div class="carousel__pagination swiper-pagination">
    {{-- Content managed by SwiperJs --}}
  </div>
  <nav class="carousel__nav">
    <button role="button" class="carousel__button carousel__button--prev"></button>
    <button role="button" class="carousel__button carousel__button--next"></button>
  </nav><!-- /.carousel__nav -->
</div><!-- /.carousel -->
