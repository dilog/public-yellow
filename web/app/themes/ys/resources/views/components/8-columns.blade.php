{{--
  8 Columns
--}}

<div class="container">
  <div class="columns is-gapless is-centered">
    <div class="column is-8">
      {{ $slot }}
    </div>
  </div>
</div>
