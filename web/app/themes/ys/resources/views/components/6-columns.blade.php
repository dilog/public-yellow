{{--
  6 Columns
--}}

<div class="container">
  <div class="columns is-gapless is-centered">
    <div class="column is-6">
      {{ $slot }}
    </div>
  </div>
</div>
