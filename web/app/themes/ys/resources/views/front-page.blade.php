@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.Front-Page.hero')
    @include('partials.Front-Page.intro')
    @include('partials.Front-Page.offers')
    @include('partials.Front-Page.cta')
    @include('partials.Front-Page.tours')
    @include('partials.Front-Page.posts')
    @include('partials.Front-Page.newsletter')
  @endwhile
@endsection
