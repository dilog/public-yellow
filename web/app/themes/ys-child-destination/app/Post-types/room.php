<?php

namespace App\PostType\Room;

use App;

function cpt_register()
{
    $svg = '<svg width="494" height="458" xmlns="http://www.w3.org/2000/svg"><path d="M468.6.7c-13.7 0-24.9 11.2-24.9 24.9v37.6l-25.3-12.6c-6.4-3.2-13.9-3.8-19-3.8-9.5 0-19.3 2.2-26.9 6L351 63.5c-2.7 1.4-4.4 4.1-4.4 7.2 0 3.1 1.7 5.8 4.4 7.2l.2.1H173.9V58.7c0-4.4-3.6-8-8-8s-8 3.6-8 8v16.5H50.3V25.6C50.3 11.9 39.1.7 25.4.7S.5 11.9.5 25.6v406.8c0 13.7 11.2 24.9 24.9 24.9s24.9-11.2 24.9-24.9v-49.3h393.5v49.3c0 13.7 11.2 24.9 24.9 24.9s24.9-11.2 24.9-24.9V25.6c0-13.7-11.2-24.9-25-24.9zm-74.5 302.8c-4.1-.2-7.7-.9-10.1-2.1l-11.5-5.8 7.2-3.6c5.4-2.7 12.8-4.3 19.7-4.3 4.8 0 9.1.8 11.9 2.2l11.5 5.8-7.2 3.6c-5 2.5-11.8 4.1-18.4 4.3l-3.1-.1zm49.6 12.9c-2.4-3.5-5.5-6.5-9.2-8.7l9.2-4.6v13.3zm0-28.2l-25.3-12.6c-6.4-3.2-13.9-3.9-19-3.9-9.5 0-19.3 2.2-26.9 6L351 288.5c-2.7 1.4-4.4 4.1-4.4 7.2 0 3.1 1.7 5.8 4.4 7.2l1.5.7H173.9V157.4h269.8v130.8zm-285.8-29.6H50.3v-32.9H158v32.9h-.1zm0-49H50.3v-24.5H158v24.5h-.1zM50.3 303.5v-29H158v29H50.3zm393.4-167.3v5.2H173.9v-5.2h269.8zm-285.8 33H50.3v-32.9H158v32.9h-.1zM61.3 332.7c0-7.3 5.9-13.2 13.2-13.2H158V336c0 4.4 3.6 8 8 8s8-3.6 8-8v-16.5H419.5c7.3 0 13.2 5.9 13.2 13.2v13.2H61.3v-13.2zm382.4-242c-2.3-3.3-5.2-6.2-8.6-8.4l8.6-4.3v12.7zM384 76.4l-11.5-5.8 7.2-3.6c5.4-2.7 12.8-4.3 19.7-4.3 4.8 0 9.1.8 11.9 2.2l11.5 5.8-7.2 3.6c-3.4 1.7-7.5 2.9-11.8 3.6h-15.1c-1.8-.4-3.4-.9-4.7-1.5zm3.2 17.5c3.2.5 6.2.7 8.6.7 3 0 6-.2 9-.7h14.6c7.3 0 13.2 5.9 13.2 13.2v13.2H173.9V93.9h213.3zm-229.3-2.6v29H50.3v-29h107.6zM34.3 432.4c0 4.9-4 8.9-8.9 8.9s-8.9-4-8.9-8.9V25.6c0-4.9 4-8.9 8.9-8.9s8.9 4 8.9 8.9V432.4zm409.4-65.3H50.3v-5.2h393.5v5.2h-.1zm33.9 65.3c0 4.9-4 8.9-8.9 8.9s-8.9-4-8.9-8.9V25.6c0-4.9 4-8.9 8.9-8.9s8.9 4 8.9 8.9v406.8z" fill="#eee" fill-rule="nonzero"/></svg>';

    $labels = [
        'singular_name' => __('Room', 'ys-backend'),
        'menu_name' => __('Rooms', 'ys-backend'),
    ];
    $args = [
        'label' => __('Rooms', 'ys-backend'),
        'description' => __('Yellow Square Rooms', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'excerpt', 'author'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => App\is_destinations(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'data:image/svg+xml;base64,' . base64_encode($svg),
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => false,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => [ 'slug' => 'room' ]
    ];
    register_post_type('room', $args);
}
add_action('init', __NAMESPACE__ . '\\cpt_register', 0);

/**
 * Custom post type columns
 */
function cpt_columns($columns)
{
    // We don't need published date for rooms
    unset($columns['date']);
    return $columns;
}
add_action('manage_edit-room_columns', __NAMESPACE__ . '\\cpt_columns');
