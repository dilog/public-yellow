<?php

namespace App\PostType\Eat;

use App;

function cpt_register()
{
    $svg = '<svg width="1792" height="1792" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M832 64v640q0 61-35.5 111t-92.5 70v779q0 52-38 90t-90 38h-128q-52 0-90-38t-38-90v-779q-57-20-92.5-70t-35.5-111v-640q0-26 19-45t45-19 45 19 19 45v416q0 26 19 45t45 19 45-19 19-45v-416q0-26 19-45t45-19 45 19 19 45v416q0 26 19 45t45 19 45-19 19-45v-416q0-26 19-45t45-19 45 19 19 45zm768 0v1600q0 52-38 90t-90 38h-128q-52 0-90-38t-38-90v-512h-224q-13 0-22.5-9.5t-9.5-22.5v-800q0-132 94-226t226-94h256q26 0 45 19t19 45z" fill="#eee" fill-rule="nonzero"/></svg>

';

    $labels = [
        'singular_name' => __('Eat & Party', 'ys-backend'),
        'menu_name' => __('Eat & Party', 'ys-backend'),
    ];
    $args = [
        'label' => __('Eat & Party', 'ys-backend'),
        'description' => __('Yellow Square Eat & Party', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'excerpt', 'author', 'page-attributes'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => App\is_destinations(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'data:image/svg+xml;base64,' . base64_encode($svg),
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => ['slug' => 'eat-party'],
    ];
    register_post_type('eat', $args);
}
add_action('init', __NAMESPACE__ . '\\cpt_register', 0);
