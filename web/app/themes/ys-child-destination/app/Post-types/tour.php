<?php

namespace App\PostType\Tour;

use App;

function cpt_register()
{
    $svg = '<svg width="2048" height="1792" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg"><path d="M2020 11q28 20 28 53v1408q0 20-11 36t-29 23l-640 256q-24 11-48 0l-616-246-616 246q-10 5-24 5-19 0-36-11-28-20-28-53v-1408q0-20 11-36t29-23l640-256q24-11 48 0l616 246 616-246q32-13 60 6zm-1284 135v1270l576 230v-1270zm-608 217v1270l544-217v-1270zm1792 1066v-1270l-544 217v1270z" fill="#eee" fill-rule="nonzero"/></svg>
';

    $labels = [
        'singular_name' => __('Tour', 'ys-backend'),
        'menu_name' => __('Tours', 'ys-backend'),
    ];
    $args = [
        'label' => __('Tours', 'ys-backend'),
        'description' => __('Yellow Square Tours', 'ys-backend'),
        'labels' => $labels,
        'supports' => ['title', 'thumbnail', 'editor', 'excerpt', 'author'],
        'hierarchical' => false,
        'public' => false,
        'show_ui' => App\is_destinations(),
        'show_in_menu' => true,
        'menu_position' => null,
        'menu_icon' => 'data:image/svg+xml;base64,' . base64_encode($svg),
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'taxonomies' => array('tour-cat'),
        'rewrite' => [ 'slug' => 'tour' ]
    ];
    register_post_type('tour', $args);
}
add_action('init', __NAMESPACE__ . '\\cpt_register', 0);

