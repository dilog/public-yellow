<?php

namespace App\Taxonomy\RoomCat;

add_action('init', function () {
    $labels = array(
        'name' => _x('Room Categories', 'Taxonomy General Name', 'ys-backend'),
        'singular_name' => _x('Category', 'Taxonomy Singular Name', 'ys-backend'),
        'add_new_item' => __('Add new category', 'ys-backend')
    );

    $args = [
        'labels' => $labels,
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_admin_column' => false,
        'show_in_nav_menus' => false,
        'show_in_rest' => false,
        'show_tagcloud' => false,
        'show_in_quick_edit' => false
    ];

    register_taxonomy('room-cat', ['room'], $args);
}, 0);
