<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use Livy\Plumbing\Templates;

/**
 * Wordpress page templates
 */
Templates\register_template_directory('views/page-templates');
Templates\register_template_directory('views/eat-custom-templates', 'eat');

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(
            str_replace(
                'ys-child-destination',
                'ys',
                get_theme_file_path().'/dist/assets.json'
            ),
            str_replace(
                'ys-child-destination',
                'ys',
                get_theme_file_uri().'/dist'
            )
        );
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
    * Create @asset() Blade directive
    */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Enqueue Google Maps SDK
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBEn8xh46Gjw27NisuQyE1sZ33BciyjVxQ');
}, 100);

/**
 * WP Stage Switcher setup
 */
$envs = [
    'development' => 'https://yellow-square.test',
    'staging'     => 'https://yellow-square.test',
    'production'  => ''
];
define('ENVIRONMENTS', $envs);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'topbar_navigation' => __('Topbar Navigation', 'sage'),
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'destination_navigation' => __('Destination Navigation', 'sage'),
        'footer_1_navigation' => __('Footer 1 Navigation', 'sage'),
        'footer_2_navigation' => __('Footer 2 Navigation', 'sage'),
        'footer_3_navigation' => __('Footer 3 Navigation', 'sage'),
        'social_links' => __('Social Links', 'sage'),
        'mobile_navigation' => __('Mobile Navigation', 'sage'),
    ]);
}, 20);
