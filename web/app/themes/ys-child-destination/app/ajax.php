<?php

namespace App;

add_action("wp_ajax_get_tours", [\App\Controllers\ArchiveTour::class, 'getTours']);
add_action("wp_ajax_nopriv_get_tours", [\App\Controllers\ArchiveTour::class, 'getTours']);