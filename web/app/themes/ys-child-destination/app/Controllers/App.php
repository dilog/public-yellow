<?php

namespace App\Controllers;

use App as Theme;
use Sober\Controller\Controller;

class App extends Controller
{
    use Partials\Common;

    public static function title()
    {
        if (is_front_page()) {
            return get_field('hero_title');
        }
        if (is_tax('room-cat')) {
            return single_term_title('', false);
        }
        return get_the_title();
    }

    public static function subtitle()
    {
        if (is_tax('room-cat')) {
            return false;
        }
        return get_the_excerpt();
    }

    public function destinationSite()
    {
        return array_search(get_current_blog_id(), Theme\config('theme.destinations'));
    }

    public function offers()
    {
        $args = [
            'post_type' => 'offer',
            'posts_per_page' => 6,
            'no_found_rows' => true,
            'update_post_term_cache' => false
        ];
        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $offers = collect($the_query->posts)->map(function ($item) {
                return Theme\cardOfferData(
                    Theme\config('theme.destinations.rome'),
                    $item,
                    ['card', 'card--offer']
                );
            });
            return $offers->all();
        }
        return false;
    }
}
