<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class ArchiveEat extends Controller
{
    use Partials\Facilities;

    public function eatAndPartyPosts()
    {
        $args = [
            'post_type' => 'eat',
            'posts_per_page' => 6,
            'no_found_rows' => true,
            'update_post_term_cache' => false,
        ];
        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $offers = collect($the_query->posts)->map(function ($item) {
                return App\cardEatPartyData(
                    App\config('theme.destinations.rome'),
                    $item,
                );
            });
            return $offers->all();
        }
        return false;
    }
}
