<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Rooms extends Controller
{
    use Partials\Facilities;

    public function roomsCategories()
    {
        $terms = get_terms([
            'taxonomy' => 'room-cat',
            'hide_empty' => false,
        ]);
        if (!is_wp_error($terms) && !empty($terms)) {
            return collect($terms)->map(function ($item) {
                $gallery = get_field('room_cat_gallery', $item);
                $price = get_field('patch_value', $item);
                $data = [
                    'id' => $item->term_id,
                    'title' => $item->name,
                    'description' => $item->description,
                    'capacity' => get_field('room_cat_capacity', $item),
                    'action' => [
                        'label' => __('Scopri di più', 'ys'),
                        'link' => get_term_link($item)
                    ]
                ];

                if ($price) {
                    $data['patch'] = [
                        'value' => $price,
                        'color' => get_field('patch_color', $item) ?: 'yellow',
                    ];
                }

                if ($gallery) {
                    foreach ($gallery as $image_id) {
                        $image_src = fly_get_attachment_image_src(
                            $image_id,
                            [620, 413],
                            true
                        );
                        $data['gallery'][] = [
                            'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
                            'caption' => wp_get_attachment_caption($image_id),
                            'src' => $image_src['src']
                        ];
                    }
                }
                return $data;
            })->all();
        }
        return false;
    }
}
