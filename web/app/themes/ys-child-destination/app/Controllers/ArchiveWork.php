<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class ArchiveWork extends Controller
{
    static function posts($cat)
    {
        $args = [
            'post_type' => 'work',
            'posts_per_page' => 6,
            'no_found_rows' => true,
            'update_post_term_cache' => false,
            'tax_query' => array(
                array(
                    'taxonomy' => 'work-cat',
                    'terms' => $cat,
                    'field' => 'slug',
                    'operator' => 'IN',
                ),
            ),
        ];
        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item) {
                return App\workAndMeetData(
                    App\config('theme.destinations.rome'),
                    $item,
                );
            });
            return $data->all();
        }
        return false;
    }

    public function workAndMeetCats() {
        $args = [
            'taxonomy' => 'work-cat', 
            'order'   => 'DESC', 
        ];

        $cats = get_categories($args);

        return $cats;
    }
}
