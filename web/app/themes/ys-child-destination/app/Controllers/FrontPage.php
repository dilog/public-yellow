<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class FrontPage extends Controller
{
    use Partials\Facilities;

    public function heroImage()
    {
        $gallery = get_field('gallery');

        // return App\responsiveImage(
        //     get_field('hero_image'),
        //     App\config('theme.hero_figure_sizes'),
        //     'hero__image'
        // );

        return App\responsiveImage(
            $gallery[array_rand($gallery)]['id'],
            App\config('theme.hero_figure_sizes'),
            'hero__image'
        );

    }

    public function gallery()
    {
        $gallery = get_field('gallery');
        return collect($gallery)->map(function ($image) {
            return [
                'src' => $image['url'],
                'opts' => [
                    'caption' => $image['title']
                ]
            ];
        })->toJson();
    }

    public function room_link()
    {
        if(App\is_milan_site()) {
            return [
                'destination' => 'milan'
            ];
        }
        if(App\is_rome_site()) {
            return [
                'destination' => 'roma'
            ];
        }
    }

    public function infographic()
    {
        $data = false;
        $image_id = get_field('infographic');
        if ($image_id) {
            $data = \App\responsiveImage(
                $image_id,
                [
                    [
                        'size' => [968, 572],
                        'media' => '(max-width: 768px)'
                    ],
                    [
                        'size' => [2000, 1060],
                        'media' => '(min-width: 769px)'
                    ],
                ],
                ''
            );
        }
        return $data;
    }

    public function tips()
    {
        $data = false;
        $args = [
            'post_type' => 'tip',
            'posts_per_page' => -1,
            'no_found_rows' => true,
            'update_post_term_cache' => false,
        ];
        $the_query = new \WP_Query($args);
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($item) use ($the_query) {
                $the_query->the_post();
                return [
                    'context' => get_field('tip_context'),
                    'image' => \App\responsiveImage(
                        get_post_thumbnail_id(),
                        [
                            [
                                'size' => [103, 120],
                                'media' => '(max-width: 768px)'
                            ],
                            [
                                'size' => [253, 294],
                                'media' => '(min-width: 769px)'
                            ],
                        ],
                        'avatar__image'
                    ),
                    'tip' => get_the_title(),
                    'tipper' => get_field('tip_tipper')
                ];
            })->all();
        }
        wp_reset_postdata();
        return $data;
    }

    public function offers()
    {
        $args = [
            'post_type' => 'offer',
            'posts_per_page' => 6,
            'no_found_rows' => true,
            'update_post_term_cache' => false
        ];
        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $offers = collect($the_query->posts)->map(function ($item) {
                return App\cardOfferData(
                    App\config('theme.destinations.rome'),
                    $item,
                    ['card', 'card--offer']
                );
            });
            return $offers->all();
        }
        return false;
    }

    public function features()
    {
        $data = false;
        $i = 0;
        if (have_rows('features')) :
            while (have_rows('features')) :
                the_row();
                $data[$i] = [
                    'over_title' => apply_shortcodes(get_sub_field('over_title')),
                    'title' => apply_shortcodes(get_sub_field('title')),
                    'description' => get_sub_field('description'),
                ];

                if (get_sub_field('link')) {
                    $data[$i]['action'] = [
                        'label' => get_sub_field('label'),
                        'link' => get_sub_field('link')
                    ];
                }

                if (get_sub_field('gallery')) {
                    foreach (get_sub_field('gallery') as $image_id) {
                        $image_src = fly_get_attachment_image_src(
                            $image_id,
                            [620, 413],
                            true
                        );
                        $data[$i]['gallery'][] = [
                            'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
                            'caption' => wp_get_attachment_caption($image_id),
                            'src' => $image_src['src']
                        ];
                    }
                }

                $i += 1;
            endwhile;
        endif;
        App\__log($data);
        return $data;
    }

    public function reviews()
    {
        if (get_current_blog_id() === App\config('theme.destinations.rome')) {
            return [
                [
                    'rating' => '100%',
                    'title' => 'Guide was very good!',
                    'description' => 'it was like back to college parties but with very cool principle and staff. The mood of the hostel , the people you meet is a part of the hostel experience itself. The staff are great . more like the older caring friend rather than staff. the rooms were clean and bright I suppose all that one would need from a hostel. it might not be the most silent , chill relaxing hostel if you wish to stay in one. I found it more like a hype stay. or maybe it was just me.',
                    'reviewer' => 'Mahi Siah'
                ],
                [
                    'rating' => '100%',
                    'title' => 'Guide was very good!',
                    'description' => 'Es un lugar fantástico. En primer lugar, es realmente asequible y económico. La habitación estaba limpia y era acogedora, cada litera cuenta con una cesta con un candado 100% seguro para guardar las maletas. A mí me tocaron unos compañeros de habitación muy majos y buena onda, además, justo al lado disponen de un bar que en la noche se convierte en Disco, dónde puedes beber y pasarlo bien conociendo gente nueva. Alojarme en el Yellow fue la mejor decisión de mi viaje a Roma, sin duda. ¡Y estoy deseando volver! Por último decir que el Staff es maravilloso, estoy muy agradecido por mi estancia.',
                    'reviewer' => 'M. López'
                ],
                [
                    'rating' => '100%',
                    'title' => 'Guide was very good!',
                    'description' => 'Great hostel for its price. Great location as it is near Termini station.',
                    'reviewer' => 'Stuart Strang'
                ],
                [
                    'rating' => '100%',
                    'title' => 'Guide was very good!',
                    'description' => 'Increíble el trato, él también te, la limpieza, las habitaciones, el bar y sobre todo la buena onda con la que se maneja toda la gente.',
                    'reviewer' => 'Lucre Leiva muzzioli'
                ]
            ];
        };
        if (get_current_blog_id() === App\config('theme.destinations.milan')) {
            return [
                [
                    'rating' => '100%',
                    'title' => 'Guide was very good!',
                    'description' => 'it was like back to college parties but with very cool principle and staff. The mood of the hostel , the people you meet is a part of the hostel experience itself. The staff are great . more like the older caring friend rather than staff. the rooms were clean and bright I suppose all that one would need from a hostel. it might not be the most silent , chill relaxing hostel if you wish to stay in one. I found it more like a hype stay. or maybe it was just me.',
                    'reviewer' => 'Mahi Siah'
                ],
                [
                    'rating' => '100%',
                    'title' => 'Guide was very good!',
                    'description' => 'Es un lugar fantástico. En primer lugar, es realmente asequible y económico. La habitación estaba limpia y era acogedora, cada litera cuenta con una cesta con un candado 100% seguro para guardar las maletas. A mí me tocaron unos compañeros de habitación muy majos y buena onda, además, justo al lado disponen de un bar que en la noche se convierte en Disco, dónde puedes beber y pasarlo bien conociendo gente nueva. Alojarme en el Yellow fue la mejor decisión de mi viaje a Roma, sin duda. ¡Y estoy deseando volver! Por último decir que el Staff es maravilloso, estoy muy agradecido por mi estancia.',
                    'reviewer' => 'M. López'
                ],
                [
                    'rating' => '100%',
                    'title' => 'Guide was very good!',
                    'description' => 'Increíble el trato, él también te, la limpieza, las habitaciones, el bar y sobre todo la buena onda con la que se maneja toda la gente.',
                    'reviewer' => 'Lucre Leiva muzzioli'
                ]
            ];
        }
    }

    public function contacts()
    {
        if (get_current_blog_id() === App\config('theme.destinations.rome')) {
            return [
                'map' => [
                    'lat' => '41.904716',
                    'lng' => '12.504365'
                ],
                'title' => __('YellowSquare Roma', 'ys'),
                'street' => 'Via Palestro 51',
                'zip' => '00185',
                'city' => __('Roma', 'ys'),
                'country' => __('Italia', 'ys'),
                'phone' => __('+39 064463554', 'ys'),
                'mail' => __('palestro@yellowsquare.it', 'ys')
            ];
        }
    }
}
