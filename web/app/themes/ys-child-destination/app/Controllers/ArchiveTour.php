<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class ArchiveTour extends Controller
{
    use Partials\Facilities;

    public function tours()
    {
        $args = [
            'post_type' => 'tour',
            'posts_per_page' => 9,
            'no_found_rows' => true,
            'update_post_term_cache' => false,
        ];
        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $index = [];
            $tours = collect($the_query->posts)
            ->map(function ($item) use (&$index) {
                global $post;
                $post = $item;
                setup_postdata($post);

                $index[] = [
                    'id' => '#tour-' . get_the_id(),
                    'title' => strtolower(get_the_title()),
                ];

                return [
                    'id' => get_the_id(),
                    'patch' => get_field('tour_patch'),
                    'permalink' => get_permalink(),
                    'subtitle' => get_field('card_summary'),
                    'title' => get_the_title(),
                    'category' => get_the_terms($post->ID, 'tour-cat')[0]->name,
                    'thumbnail' => get_the_post_thumbnail_url(),
                    'content' => get_the_content(),
                    'place' => get_field('place'),
                    'opening_hours' => __('Opening Hours from ', 'ys') . get_field('opening_hours'),
                    'capacity' => __('Capacity ', 'ys') . get_field('capacity'),
                    'action' => array(
                        'label' => get_field('button_label'),
                        'link' => get_field('button_link'),
                    ),
                    'facilities' => get_field('place_facilities'),
                    'duration' => get_field('duration'),
                    'address' => get_field('address'),
                    'availability' => get_field('Availability')
                ];
            });
            return $tours->all();
        }
        return false;
    }

    public function categories()
    {
        $terms = get_terms([
            'taxonomy' => 'tour-cat',
            'hide_empty' => false,
        ]);

        if (!is_wp_error($terms) && !empty($terms)) {
            return collect($terms)->map(function ($term, $key) {
                return [
                    'id' => $term->term_id,
                    'class' => '',
                    'name' => $term->name,
                ];
            })->all();
        }

        return false;
    }

    public static function getTours()
    {
        // $ids = \json_decode('["17","20"]');
        $ids = array_map('intval', explode(',', $_POST['ids']));

        // var_dump($id);

        $args = [
            'post_type' => 'tour',
            'posts_per_page' => -1,
            'no_found_rows' => true,
            'update_post_term_cache' => false
        ];

        if (count($ids) > 0) {
            $args['tax_query'] = [
                [
                    'taxonomy' => 'tour-cat',
                    'terms' => $ids

                ],
            ];
        }

        if (count($ids) == 1 && $ids[0] == 0) {
            $args['tax_query'] = [];
        }

        $the_query = new \Wp_Query($args);
        if ($the_query->have_posts()) {
            $index = [];
            $tours = collect($the_query->posts)
            ->map(function ($item) use (&$index) {
                global $post;
                $post = $item;
                setup_postdata($post);

                $index[] = [
                    'id' => '#tour-' . get_the_id(),
                    'title' => strtolower(get_the_title()),
                ];

                return [
                    'id' => get_the_id(),
                    'patch' => get_field('tour_patch'),
                    'permalink' => get_permalink(),
                    'subtitle' => get_field('card_summary'),
                    'title' => get_the_title(),
                    'category' => get_the_terms($post->ID, 'tour-cat')[0]->name,
                    'thumbnail' => get_the_post_thumbnail_url(),
                    'content' => get_the_content(),
                    'place' => get_field('place'),
                    'opening_hours' => __('Opening Hours from ', 'ys') . get_field('opening_hours'),
                    'capacity' => __('Capacity ', 'ys') . get_field('capacity'),
                    'action' => array(
                        'label' => get_field('button_label'),
                        'link' => get_field('button_link'),
                    ),
                    'facilities' => get_field('place_facilities'),
                    'duration' => get_field('duration'),
                    'address' => get_field('address'),
                    'availability' => get_field('Availability')
                ];
            })->all();
            wp_reset_postdata();
            $data = App\template('partials.cards.card-tour', ['tours' => $tours]);
            wp_send_json_success(['results' => $data, 'index' => $index]);
        }
        wp_send_json_success(['results' => $ids, 'index' => 0]);
        // wp_send_json_error();
    }
}
