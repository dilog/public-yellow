<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TaxonomyRoomCat extends Controller
{
    use Partials\Facilities;

    public function rooms()
    {
        $args = [
            'post_type' => 'room',
            'posts_per_page' => -1
        ];
        $the_query = new \WP_Query($args);
        if ($the_query->have_posts()) {
            return collect($the_query->posts)->map(function ($item) {
            global $post;
            $post = $item;
            setup_postdata($post);

            $gallery = get_field('room_gallery');
            $data = [
                'title' => get_the_title(),
                'content' => get_the_content(),
                'facilities' => get_field('room_facilities'),
                'action' => [
                    'label' => __('Scopri disponibilità', 'ys'),
                    'link' => '#',
                ],
                'category' => get_field('room_cat'),
            ];
            if ($gallery) {
                foreach ($gallery as $image_id) {
                    $image_src = fly_get_attachment_image_src(
                        $image_id,
                        [620, 413],
                        true
                    );
                    $data['gallery'][] = [
                        'alt' => get_post_meta($image_id, '_wp_attachment_image_alt', true),
                        'caption' => wp_get_attachment_caption($image_id),
                        'src' => $image_src['src']
                    ];
                }
            }
            wp_reset_postdata();
            return $data;
        })->all();
        }
    }
}
