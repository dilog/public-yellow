<?php

namespace App\Controllers\Partials;

use App;

trait Common
{
    public function groups()
    {
        return [
            'permalink' => get_permalink(App\config('theme.pages.groups'))
        ];
    }

    public function obfuscatedEmail()
    {
        return [
            'info' => antispambot('info@yellowsquare.it'),
            'paolacaridi' => antispambot('paolacaridi@yellowsquare.it'),
            'pr' => antispambot('pr@yellowsquare.it')
        ];
    }
}
