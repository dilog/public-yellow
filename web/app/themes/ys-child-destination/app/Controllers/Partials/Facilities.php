<?php

namespace App\Controllers\Partials;

use App;

trait Facilities
{
    public function facilities()
    {
        if (get_current_blog_id() === App\config('theme.destinations.rome')) {
            return [
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-wifi',
                    'icon' => \App\asset_path('images/facilities/wifi.svg'),
                    'title' => _x('Wi-Fi gratis', 'facilities@destination', 'ys'),

                ],
                [
                    'color' => 'blue',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-map',
                    'icon' => \App\asset_path('images/facilities/city-map.svg'),
                    'title' => _x('Mappa gratuita della città', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'orange',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-luggage',
                    'icon' => \App\asset_path('images/facilities/luggage.svg'),
                    'title' => _x('Deposito bagagli (gratuito al check in)', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'white',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-h24',
                    'icon' => \App\asset_path('images/facilities/h24.svg'),
                    'title' => _x('Reception h24', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-money',
                    'icon' => \App\asset_path('images/facilities/money-exchange.svg'),
                    'title' => _x('Bancomat/Cambio Valuta', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'green',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-hair',
                    'icon' => \App\asset_path('images/facilities/hair.svg'),
                    'title' => _x('Parrucchiere', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'white',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-laundry',
                    'icon' => \App\asset_path('images/facilities/laundry.svg'),
                    'title' => _x('Lavanderia', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'violet',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-play',
                    'icon' => \App\asset_path('images/facilities/play-area.svg'),
                    'title' => _x('Area giochi', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'blue',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-star',
                    'icon' => \App\asset_path('images/facilities/star.svg'),
                    'title' => _x('Tour Gratuiti e Attività', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'orange',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-linen',
                    'icon' => \App\asset_path('images/facilities/linen.svg'),
                    'title' => _x('Biancheria inclusa', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-housekeeping',
                    'icon' => \App\asset_path('images/facilities/housekeeping.svg'),
                    'title' => _x('Servizio Pulizia', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-rent',
                    'icon' => \App\asset_path('images/facilities/towel.svg'),
                    'title' => _x('Asciugamani a noleggio per i dormitori', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/towel.svg'),
                    'title' => _x('Asciugamani inclusi per le stanze private', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/gel.svg'),
                    'title' => _x('Gel igienizzanti a disposizione', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/drink.svg'),
                    'title' => _x('Bar / Cucina in comune', 'facilities@destination', 'ys')
                ]
            ];
        } else if (get_current_blog_id() === App\config('theme.destinations.milan')) {
            return [
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-wifi',
                    'icon' => \App\asset_path('images/facilities/wifi.svg'),
                    'title' => _x('Wi-Fi gratis', 'facilities@destination', 'ys'),

                ],
                [
                    'color' => 'blue',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-map',
                    'icon' => \App\asset_path('images/facilities/city-map.svg'),
                    'title' => _x('Mappa gratuita della città', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'orange',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-luggage',
                    'icon' => \App\asset_path('images/facilities/luggage.svg'),
                    'title' => _x('Deposito bagagli (gratuito al check in)', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'white',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-h24',
                    'icon' => \App\asset_path('images/facilities/h24.svg'),
                    'title' => _x('Reception h24', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-money',
                    'icon' => \App\asset_path('images/facilities/money-exchange.svg'),
                    'title' => _x('Bancomat/Cambio Valuta', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'green',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-hair',
                    'icon' => \App\asset_path('images/facilities/hair.svg'),
                    'title' => _x('Parrucchiere', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'white',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-laundry',
                    'icon' => \App\asset_path('images/facilities/laundry.svg'),
                    'title' => _x('Lavanderia', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'violet',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-play',
                    'icon' => \App\asset_path('images/facilities/play-area.svg'),
                    'title' => _x('Area giochi', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'blue',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-star',
                    'icon' => \App\asset_path('images/facilities/star.svg'),
                    'title' => _x('Tour Gratuiti e Attività', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'orange',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-linen',
                    'icon' => \App\asset_path('images/facilities/linen.svg'),
                    'title' => _x('Biancheria inclusa', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-housekeeping',
                    'icon' => \App\asset_path('images/facilities/housekeeping.svg'),
                    'title' => _x('Servizio Pulizia', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-rent',
                    'icon' => \App\asset_path('images/facilities/towel.svg'),
                    'title' => _x('Asciugamani a noleggio per i dormitori', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/towel.svg'),
                    'title' => _x('Asciugamani inclusi per le stanze private', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/gel.svg'),
                    'title' => _x('Gel igienizzanti a disposizione', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/drink.svg'),
                    'title' => _x('Bar / Cucina in comune', 'facilities@destination', 'ys')
                ]
            ];
        } else {
            return [
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-wifi',
                    'icon' => \App\asset_path('images/facilities/wifi.svg'),
                    'title' => _x('Wi-Fi gratis', 'facilities@destination', 'ys'),

                ],
                [
                    'color' => 'blue',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-map',
                    'icon' => \App\asset_path('images/facilities/city-map.svg'),
                    'title' => _x('Mappa gratuita della città', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'orange',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-luggage',
                    'icon' => \App\asset_path('images/facilities/luggage.svg'),
                    'title' => _x('Deposito bagagli (gratuito al check in)', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'white',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-h24',
                    'icon' => \App\asset_path('images/facilities/h24.svg'),
                    'title' => _x('Reception h24', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-money',
                    'icon' => \App\asset_path('images/facilities/money-exchange.svg'),
                    'title' => _x('Bancomat/Cambio Valuta', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'green',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-hair',
                    'icon' => \App\asset_path('images/facilities/hair.svg'),
                    'title' => _x('Parrucchiere', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'white',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-laundry',
                    'icon' => \App\asset_path('images/facilities/laundry.svg'),
                    'title' => _x('Lavanderia', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'violet',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-play',
                    'icon' => \App\asset_path('images/facilities/play-area.svg'),
                    'title' => _x('Area giochi', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'blue',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-star',
                    'icon' => \App\asset_path('images/facilities/star.svg'),
                    'title' => _x('Tour Gratuiti e Attività', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'orange',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-linen',
                    'icon' => \App\asset_path('images/facilities/linen.svg'),
                    'title' => _x('Biancheria inclusa', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'salmon',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-housekeeping',
                    'icon' => \App\asset_path('images/facilities/housekeeping.svg'),
                    'title' => _x('Servizio Pulizia', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-rent',
                    'icon' => \App\asset_path('images/facilities/towel.svg'),
                    'title' => _x('Asciugamani a noleggio per i dormitori', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/towel.svg'),
                    'title' => _x('Asciugamani inclusi per le stanze private', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/gel.svg'),
                    'title' => _x('Gel igienizzanti a disposizione', 'facilities@destination', 'ys')
                ],
                [
                    'color' => 'pink',
                    'description' => _x('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'facilities@destination', 'ys'),
                    'id' => 'facility-towel-free',
                    'icon' => \App\asset_path('images/facilities/drink.svg'),
                    'title' => _x('Bar / Cucina in comune', 'facilities@destination', 'ys')
                ]
            ];
        }
    }
}
