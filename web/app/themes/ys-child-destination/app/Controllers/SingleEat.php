<?php

namespace App\Controllers;

use App;
use Sober\Controller\Controller;

class SingleEat extends Controller
{
    public function __constructor()
    {
        $this->id = get_the_id();
    }

    public function author()
    {
        $author_id = get_the_author_meta('ID');
        return [
            'avatar' => get_avatar_url($author_id, [
                'size' => 150
            ]),
            'fullname' => App\format_author_name($author_id, 'full'),
            'shortname' => App\format_author_name($author_id, 'short'),
            'description' => get_the_author_meta('description')
        ];
    }

    public function category()
    {
        return App\get_primary_category($this->id);
    }

    public function classes()
    {
        return implode(' ', get_post_class());
    }

    public function content()
    {
        return apply_filters('the_content', get_the_content());
    }

    public function gallery()
    {
        return get_field('gallery');
    }

    public function specialslider() {
        return get_field('eat_party_slide');
    }

    public function promoBanner() {
        return array(
            'title' => get_field('promo_banner_title'),
            'subtitle' => get_field('promo_banner_subtitle'),
            'link_label' => get_field('promo_banner_link_label'),
            'link_url' => get_field('promo_banner_link_url'),
            'background_image' => get_field('promo_banner_background_image'),
        );
    }

    public function location() {
        return array(
            'address' => get_field('location'),
            'map' => get_field('map'),
            'gmap_url' => 'https://www.google.com/maps/embed/v1/place?key='.env('GOOGLE_API_KEY').'&q=place_id:'.get_field('map')['place_id'],
        );
    }

    public function availability() {
        return array(
            'availability_label' => get_field('availability_label'),
            'availability_link' => get_field('availability_link'),
        );
    }

    public function openingHours() {
        return get_field('opening-hours');
    }

    public function datetime()
    {
        return [
            'iso' => get_post_time('c', true),
            'hr' => get_the_date()
        ];
    }

    public function id()
    {
        return $this->id;
    }

    public function related()
    {
        $args = [
            'post_type' => 'post',
            'posts_per_page' => 6,
            'post__not_in' => [$this->id]
        ];
        $the_query = new \WP_Query($args);
        if ($the_query->have_posts()) {
            $data = collect($the_query->posts)->map(function ($post) {
                return App\cardPostData(
                    $post,
                    [
                        'card',
                        'card--post',
                    ]
                );
            })->all();
            wp_reset_postdata();
            return $data;
        }
        return false;
    }

    public function tags()
    {
        return get_the_tag_list('<ul class="tags-list"><li class="tags-list__item">', '</li><li>', '</li></ul>');
    }

    public function thumbnail()
    {
        return App\responsiveImage(
            get_post_thumbnail_id(),
            App\config('theme.single_figure_sizes'),
            'post-media__image'
        );
    }
}

// function cardPostData($item = null, $classes = [])
// {
//     if (!is_null($item)) {
//         global $post;
//         $post = $item;
//         setup_postdata($post);
//     }

//     $author_id = get_the_author_meta('ID');
//     $class = implode(' ', [...$classes]);
//     $data = [
//         'author' => [
//             'avatar' => get_avatar_url($author_id, [
//                 'size' => 72
//             ]),
//             'fullname' => get_the_author(),
//             'shortname' => format_author_name($author_id),
//         ],
//         'category' => get_primary_category(get_the_id()),
//         'class' => join(' ', get_post_class($class)),
//         'datetime' => get_post_time('c', true),
//         'datetime_hr' => get_the_date(),
//         'excerpt' => get_the_excerpt(),
//         'id' => get_the_id(),
//         'is_big' => in_array('card--big', $classes),
//         'patch' => get_field('patch_type'),
//         'permalink' => get_permalink(),
//         'post_type' => get_post_type(),
//         'thumbnail' => get_post_thumbnail_id(),
//         'title' => get_the_title()
//     ];
//     return $data;
// }
