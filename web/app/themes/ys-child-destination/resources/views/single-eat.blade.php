@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <article class="{{ $classes }}">
      <header class="post-header">
        <div class="post-header__content">
          <h1 class="post-header__title entry-title"><span class="post-header__title-desc">{{ $category['title'] }}</span>{!! get_the_title() !!}</h1>
        </div><!-- /.post-header__content -->
        <div class="post-header__patch">
          @include('partials.patch', ['type' => 'shit', 'animated' => true])
        </div>
      </header><!-- /.post-header -->
      {{--
      //
      // Header Slider
      //
      --}}
      <section class="tile tile__header-banner">
        <div class="tile__main">
          @component('components.carousel', ['namespace' => 'banner'])
            @foreach ($gallery as $image)
              @include('partials.slides.header')
            @endforeach
          @endcomponent
        </div><!-- /.tile__main -->
      </section><!-- /.tile -->

      <div class="container post-opening-section">
        @if ($opening_hours)
          <div class="opening-hours">
            <h4>Orario di Apertura</h4>
            {!! $opening_hours !!}
          </div>
        @endif
        @if ($availability['availability_link'])
        <a class="button button--s" href="{{ $availability['availability_link'] }}" target="_blank">{{ __($availability['availability_label'], 'ys') }}</a>
        @endif
      </div>

      <section class="section--m-xs">
        <div class="container">
          <div class="columns is-gapless">
            <div class="column is-7">
              {!! $content !!}
            </div>
          </div>
        </div>
      </section>

      <div class="container where">
        <h4>Dove</h4>
        {!! $location['address'] !!}



        <div class="responsive-map">
          <iframe src="{{ $location['gmap_url'] }}" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>



      </div>

      @if(!empty($promo_banner['title']))
      <section class="section--m-small">
        <div class="container">
          <div class="bottom-banner">
            <div class="bottom-banner__text">
              <h3 class="bottom-banner__title">{{ _x($promo_banner['title'], 'corporate@about-us', 'ys') }}</h3>
              <p class="bottom-banner__subtitle">{{ _x($promo_banner['subtitle'], 'corporate@about-us', 'ys') }}</p>
              <a class="button button--l" href="{{ $promo_banner['link_url'] }}">{{ __($promo_banner['link_label'], 'ys') }}</a>
            </div>
            <img src="{{ $promo_banner['background_image']['url'] }}" alt="{{ $promo_banner['background_image']['alt'] }}">
          </div>
        </div>
      </section>
      @endif

      {{--
      //
      // Stay for Free promo
      //
      --}}
      <section class="tile tile--blue">
        @component('components.6-columns')
          <header class="tile__header">
            <h3 class="tile__title">{{ _x('Cerchi uno spazio per il tuo evento?', 'corporate@eat-party', 'ys') }}</h3>
          </header><!-- /.tile__header -->
          <div class="tile__main">
            <p>{{ _x("Allo YellowSquare puoi organizzare aperitivi, compleanni, feste di laurea e party galattici.", 'corporate@eat-party', 'ys') }}</p>
            <p><a href="https://docs.google.com/forms/d/e/1FAIpQLScBotD_OidmN-RxGTpK8fKT9QkfjhjTIGOiEylvWZH_w9Lt7A/viewform" target="_blank">{{ __('Contattaci', 'ys') }}</a></p>
          </div><!-- /.tile__main -->
        @endcomponent
      </section><!-- /.tile -->
    </article>
  @endwhile

  {{-- @if ($related)
    <section class="related">
      <header class="related__header">
        <div class="related__container">
          <div class="related__title">{{ _x('Leggi altri articoli', 'corporate@blog', 'ys') }}</div>
        </div>
      </header><!-- /.related__header -->
      <div class="related__main">
        @component('components.carousel-full')
          @foreach ($related as $item)
            <div class="carousel__slide swiper-slide">
              @include('partials.cards/card-'.$item['post_type'])
            </div><!-- /.carousel__slide -->
          @endforeach
        @endcomponent
      </div><!-- /.related__main -->
    </section><!-- /.related -->
  @endif --}}
@endsection
