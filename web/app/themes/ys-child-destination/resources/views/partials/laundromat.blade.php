{{--
//
// LaundroMat promotion
//
--}}
<section class="tile tile--green{{ isset($class) ? ' ' . $class : null }}">
  @component('components.6-columns')
    <header class="tile__header">
      <h3 class="tile__title">{{ __('LaundroMat', 'ys') }}</h3>
    </header><!-- /.tile__header -->
    <div class="tile__main">
      <p>{{ __('Torni in stanza e i tuoi vestiti sono irriconoscibili? Nessun problema: la nostra lavanderia li farà tornare come nuovi.', 'ys') }}</p>
      @if (get_current_blog_id() != App\config('theme.destinations.milan'))
      <p class="h6">{{ __('aperta dalle ore 6:00 alle ore 23:00', 'ys') }}</p>
      @endif
    </div><!-- /.tile__main -->
  @endcomponent
  @include('partials.patch', ['type' => 'laundry', 'class' => ''])
</section><!-- /.tile -->
