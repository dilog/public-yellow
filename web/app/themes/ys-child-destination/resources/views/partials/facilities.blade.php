{{--
//
// Facilities
//
--}}
<section class="tile tile--yellow tile--has-shadow tile--gtk">
  @component('components.8-columns')
    <header class="tile__header">
      <h3 class="tile__title"><span>{{ __('Buono a sapersi', 'facilities@destination', 'ys') }}</span><span>{{ __('Check in 15:00 - Check out 10:00', 'facilities@destination', 'ys') }}</span><span>({{ _x('11:00 per le camere private', 'facilities@destination', 'ys') }})</span></h3>
    </header><!-- /.tile__header -->
  @endcomponent
    <div class="tile__main">
      @component('components.10-columns')
      <div class="carousel carousel--gtk swiper-container js-carousel-gtk">
        <div class="swiper-wrapper">
          @foreach ($facilities as $item)
            <div class="swiper-slide">
              {{-- <div class="facility">
                <a data-fancybox data-animation-duration="500" data-src="#{{ $item['id'] }}" class="facility__button" role="button" href="#">
                  <img class="facility__icon" src="{{ $item['icon'] }}" aria-hidden="true" alt="" />
                  <div class="facility__title">{{ $item['title'] }}</div>
                </a>
                <div id="{{ $item['id'] }}" class="modal modal--{{ $item['color'] }} modal--animated" role="dialog">
                  <div class="modal__content">
                    <div class="modal__header">
                      <img class="facility__icon" src="{{ $item['icon'] }}" aria-hidden="true" alt="" />
                      <div class="modal__title">{{ $item['title'] }}</div>
                    </div>
                    <div class="modal__main">
                      {{ $item['description'] }}
                    </div>
                  </div>
                </div>
              </div> --}}
              <div class="facility">
                <span class="facility__button">
                  <img class="facility__icon" src="{{ $item['icon'] }}" aria-hidden="true" alt="" />
                  <div class="facility__title">{{ $item['title'] }}</div>
                </span>
              </div>
            </div>
          @endforeach
        </div>
        <div class="carousel__bullets--gtk"></div>
      </div>
    @endcomponent
  </div><!-- /.tile__main -->
  <div class="tile__footer">
    <p>{{ _x('Cibo ed alcolici non sono ammessi nelle stanze', 'facilities@destination', 'ys') }}</p>
  </div>
</section><!-- /.tile -->
