{{--
  Template Name: Tour
  Template Post Type: page
--}}

@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
<section class="section">
  @component('components.6-columns')
    <header class="section__header mb-0">
      <h2 class="section__title h3">{{ _x('Tour & Experience', 'corporate@front-page', 'ys') }}</h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="container offer-archive">
    @if ($categories)
      <div class="filter-checkboxes">
        <h6>Filtra per tipologia</h6>
        <nav id="js-categories"role="menu" class="tour-checkboxes">
          <form style="margin-bottom: 50px" data-reset="reset">
            @foreach ($categories as $category)
            <label>
              <input name="catid" type="checkbox" checked value="{{ $category['id'] }}">
              <span></span>
              {!! $category['name'] !!}
            </label>
            @endforeach
          </form>
        </nav>
      </div>
    @endif
    <div class="columns is-multiline tour-cards" id="filtered-posts">
      @include('partials.cards.card-tour')
    </div>
  </div>
</section>
@endsection
