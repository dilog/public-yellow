{{--
  Template Name: Eat And Party Custom
  Template Post Type: eat
--}}

@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <section class="tile tile__eat-special-banner">
      <div class="tile__main">
        @component('components.carousel', ['namespace' => 'eatspecial'])
          @foreach ($specialslider as $item)
            @include('partials.slides.eatspecial')
          @endforeach
        @endcomponent
      </div><!-- /.tile__main -->
    </section><!-- /.tile -->
    {{--
    //
    // Bottom Slider
    //
    --}}
    <section class="tile tile__header-banner">
      <div class="tile__main">
        @component('components.carousel', ['namespace' => 'banner'])
          @foreach ($gallery as $image)
            @include('partials.slides.header')
          @endforeach
        @endcomponent
      </div><!-- /.tile__main -->
    </section><!-- /.tile -->
    
  @endwhile
@endsection
