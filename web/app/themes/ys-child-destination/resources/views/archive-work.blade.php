@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  <div
  class="page-header @if (isset($patch) && $position === 'right') page-header--has-right-patch @endif @if (isset($patch) && $position === 'left') page-header--has-left-patch @endif">
  @component('components.6-columns')
    <div class="page-header__content">
      <h1 class="page-header__title">Work & Meet</h1>
    </div>
    @if (isset($patch))
      <div class="page-header__patch">
        @include('partials.patch', ['type' => $patch, 'animated' => true])
      </div>
    @endif
  @endcomponent
  </div><!-- /.page-header -->

  <div class="section__main">
    <div class="tabs js-tabs">
      @component('components.10-columns')
        <nav class="tabs__nav">
          <ul class="tabs__list">
            @foreach ($work_and_meet_cats as $key => $cat)
            <li class="tabs__item {{ $key == 0 ? 'tabs__item--active' : '' }}">
              <a class="h5" href="#{{ $cat->slug }}" role="tab" aria-controls="{{ $cat->slug }}" aria-selected="{{ $key == 0 ? 'true' : 'false' }}">{{ __($cat->name, 'ys') }}</a>
            </li>
            @endforeach
          </ul>
        </nav><!-- /tabs__nav -->
      @endcomponent
    </div><!-- /.tabs -->
    {{--
      Private
    --}}
    @foreach ($work_and_meet_cats as $key => $cat)
    <section id="{{ $cat->slug }}" class="section tabs-content {{ $key == 0 ? 'tabs-content--active tabs-content--shown' : '' }} section--m-small"  role="tabpanel">
      <p class="text-center section--m-small">{!! $cat->description !!}</p>
      @component('components/container')
        @foreach (ArchiveWork::posts($cat->slug) as $key => $post)
          <div class="swiper-slide">
            <div class="feature feature--room {{ (($key + 1) % 2) == 0 ? 'feature--reverse' : '' }}">
              <div class="feature__header-mobile">
                <h3 class="feature__title">{!! $post['title'] !!}</h3>
              </div>
              <div class="feature__media">
                @if ($post['gallery'])
                  @component('components.carousel-card', ['shadow' => true])
                    @foreach ($post['gallery'] as $image)
                      @include('partials.slides.card')
                    @endforeach
                  @endcomponent
                @endif
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h2 class="feature__title">{!! $post['title'] !!}</h2>
                </div>
                <div class="general-details">
                  <ul>
                    <!--todo-->
                    @if(!empty($post['place']))
                      <li>
                        <img src="@asset('images/icons/place-ok.svg')" style="margin-right:10px"> {{ $post['place'] }}
                      </li>
                    @endif
                    @if(!empty($post['opening_hours']))
                      <li>
                        <img src="@asset('images/icons/clock-ok.svg')" style="margin-right:10px"> {{ $post['opening_hours'] }}
                      </li>
                    @endif
                    @if(!empty($post['capacity']))
                      <li>
                        <img src="@asset('images/icons/persons-ok.svg')" style="margin-right:10px"> {{ $post['capacity'] }}
                      </li>
                    @endif
                  </ul>
                </div>
                <div class="feature__content">
                  <p>{!! $post['content'] !!}</p>
                  @if (isset($post['facilities']))
                    <ul class="facilities facilities--work">
                      @foreach ($post['facilities'] as $facility)
                        <li>
                          <img src="@asset('images/icons/check-ok.svg')" style="margin-right:10px"> {{ $facility['place_facility'] }}
                        </li>
                      @endforeach
                    </ul>
                  @endif
                </div>
                  <div class="feature__action">
                    @if (isset($post['action']))
                    <a class="button button--s" href="{{ $post['action']['link'] }}" target="_blank">{{ $post['action']['label'] }}</a>
                    @endif
                    @if (isset($post['spec_sheet']))
                    <a class="button button--s" href="{{ $post['spec_sheet'] }}" target="_blank">Scheda Tecnica</a>
                    @endif
                  </div>

              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
          </div><!-- /.swiper-wrapper -->
        @endforeach
      @endcomponent
    </section><!-- /.section -->
    @endforeach
  </div><!-- /.section__main -->
@endsection
