@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection
@php
$term = get_queried_object();
@endphp
@section('content')
  @include('partials.page-header')
  <section class="section">
    @component('components/container')
      @foreach ($rooms as $room)
      @if ($room['category'] == $term->term_id)
      <div class="feature feature--room">
        <div class="feature__header-mobile">
          <h3 class="feature__title">{!! $room['title'] !!}</h3>
        </div>
        <div class="feature__media">
          @if ($room['gallery'])
            @component('components.carousel-card', ['shadow' => true])
              @foreach ($room['gallery'] as $image)
                @include('partials.slides.card')
              @endforeach
            @endcomponent
          @endif
        </div><!-- /.feature__media -->
        <div class="feature__main">
          <div class="feature__header">
            <h2 class="feature__title">{!! $room['title'] !!}</h2>
          </div>
          <div class="feature__content">
            <p>{!! $room['content'] !!}</p>
            @if (isset($room['facilities']))
              <ul class="facilities facilities--room">
                @foreach ($room['facilities'] as $facility)
                  <li>
                    <span class="icon ys-{{$facility['value']}}"></span> {{ $facility['label'] }}
                  </li>
                @endforeach
              </ul>
            @endif
          </div>
          @if (isset($room['action']))
            <div class="feature__action">
              <a class="button button--s" data-open="book-now" href="{{ $room['action']['link'] }}">{{ $room['action']['label'] }}</a>
            </div>
          @endif
        </div><!-- /.feature__main -->
      </div><!-- /.feature -->
      @endif
      @endforeach
    @endcomponent
  </section><!-- /.section -->
  @if(get_queried_object()->term_id==10)
  <section class="tile tile--has-cover tile--has-arrow-on-desktop">
    <img class="js-lazy tile__image" data-src="/app/uploads/sites/2/2021/07/camere-private-banner.jpg" alt="Private Room" aria-hidden="true" />
    @component('components.6-columns')
    <header class="tile__header">
      <h3 class="tile__title">{{ __('Vuoi più intimità?', 'ys') }}</h3>
    </header><!-- /.tile__header -->
    <div class="tile__main">
      <!-- todo -->
      <a class="button button--l" href="/{{ get_current_blog_id() == App\config('theme.destinations.milan') ? 'milan' : 'roma' }}/room-cat/private/">{{ __('Scopri le camere private', 'ys') }}</a>
    </div><!-- /.tile__main -->
    @endcomponent
  </section>
  @else
  <section class="tile tile--has-cover tile--has-arrow-on-desktop">
    <img class="js-lazy tile__image" data-src="/app/uploads/sites/2/2021/07/camere-dorm-banner.jpg" alt="Private Room" aria-hidden="true" />
    @component('components.6-columns')
    <header class="tile__header">
      <h3 class="tile__title">{{ __('Vuoi fare la vita da ostello e dormire in un dormitorio?', 'ys') }}</h3>
    </header><!-- /.tile__header -->
    <div class="tile__main">
      <!-- todo -->
      <a class="button button--l" href="/{{ get_current_blog_id() == App\config('theme.destinations.milan') ? 'milan' : 'roma' }}/room-cat/dormitori-misti-o-femminili/">{{ __('Scopri i nostri dormitori', 'ys') }}</a>
    </div><!-- /.tile__main -->
    @endcomponent
  </section>
  @endif


  {{--
  //
  // LaundroMat promotion
  //
  @if (get_current_blog_id() != App\config('theme.destinations.milan')) {
  --}}
    @include('partials.laundromat', ['class' => 'tile--has-arrow-on-mobile'])
  {{--
  @endif
  //
  // Facilities
  //
  --}}
  @include('partials.facilities')
@endsection
