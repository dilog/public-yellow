{{--
  Template Name: Rooms
  Template Post Type: page
--}}

@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    {{--
    //
    // Categories
    //
    --}}
    <section class="section">
      @component('components/container')
        <div class="features">
          @foreach ($rooms_categories as $item)
            <div class="feature feature--room-cat">
              <div class="feature__header-mobile">
                <h3 class="feature__title">{!! $item['title'] !!}</h3>
                <div class="rooms-capacity"> <span class="icon ys-people-more"></span> {{ $item['capacity'] }}</div>
              </div>
              <div class="feature__media">
                @if ($item['gallery'])
                  @component('components.carousel-card', ['shadow' => true])
                    @foreach ($item['gallery'] as $image)
                      @include('partials.slides.card')
                    @endforeach
                  @endcomponent
                @endif
                @if (isset($item['patch']))
                  @include('partials.patch-from-price', ['data' => $item['patch']])
                @endif
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h4 class="feature__title">{!! $item['title'] !!}</h4>
                  <div class="rooms-capacity"> <span class="icon ys-people-more"></span> {{ $item['capacity'] }}</div>
                </div>
                <div class="feature__content">
                  <p>{!! $item['description'] !!}</p>
                </div>
                @if (isset($item['action']))
                  <div class="feature__action">
                    <a class="button" href="{{ $item['action']['link'] }}">{{ $item['action']['label'] }}</a>
                  </div>
                @endif
              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
          @endforeach
        </div><!-- /.features -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // LaundroMat promotion
    //
    @if (get_current_blog_id() != App\config('theme.destinations.milan')) {
    --}}
      @include('partials.laundromat', ['class' => 'tile--has-arrow-on-mobile'])
    {{--
    @endif
    //
    // Facilities
    //
    --}}
    @include('partials.facilities')
  @endwhile
@endsection
