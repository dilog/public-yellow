{{--
  Template Name: Rooms
  Template Post Type: page
--}}

@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
<section class="section">
  @component('components.6-columns')
    <header class="section__header mb-0">
      <h2 class="section__title h3">{{ _x('Offerte', 'corporate@front-page', 'ys') }}</h2>
    </header><!-- /.section__header -->
  @endcomponent
  <div class="container offer-archive">
    <div class="columns is-multiline">
      @foreach ($offers as $item)
        @component('components.4-columns-without-cont')
          @include('partials.cards.card-offer-with-image')
        @endcomponent
      @endforeach
    </div>
  </div>
</section>
@endsection
