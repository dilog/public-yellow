@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <article class="{{ $classes }}">
      <header class="post-header">
        <div class="post-header__content">
          <h1 class="post-header__title entry-title"><span class="post-header__title-desc">{{ $category['title'] }}</span>{!! get_the_title() !!}</h1>
        </div><!-- /.post-header__content -->
        @if (isset($patch))
        <div class="post-header__patch">
          @include('partials.patch', ['type' => $patch, 'animated' => true])
        </div>
        @endif

      </header><!-- /.post-header -->
      {{--
      //
      // Header Slider
      //
      --}}
      <section class="tile tile__header-banner">
        <div class="tile__main">
          @component('components.carousel', ['namespace' => 'banner'])
            @foreach ($gallery as $image)
              @include('partials.slides.header')
            @endforeach
          @endcomponent
        </div><!-- /.tile__main -->
        @if (isset($price) || !empty($registration['link']))
          <div class="registration">
            @if (isset($price))
            <h3 class="registration__label">{{ $price  }}</h3>
            @endif
            @if (!empty($registration['link']))
              <a class="button button--s" href="{{ $registration['link'] }}">{{ $registration['label'] ? $registration['label'] : 'Registration' }}</a>
            @endif
          </div>
        @endif
      </section><!-- /.tile -->
{{--
      <div class="container post-opening-section">
        @if ($opening_hours)
          <div class="opening-hours">
            {!! $opening_hours !!}
          </div>
        @endif
      </div> --}}

      <section class="section--m-xs content">
        <div class="container">
          <div class="columns is-gapless">
            <div class="column is-7">
              {!! $content !!}
            </div>
          </div>
        </div>
      </section>

      {{-- <section class="section--m-xs">
        <div class="container">
          <h4 class="no-mar">Share with your friend</h4>
          @include('partials.share', ['version' => 2])
        </div>
      </section> --}}
      <section class="section--m-xs content">
        <div class="container">
          <h2 class="about-title">Info</h2>
          <ul class="about-list">
            @foreach ($about as $item)
              @if($item!='')
                <li>{!! $item !!}</li>
              @endif
            @endforeach
          </ul>
        </div>
      </section>

      <section class="section--m-xs content">
        <div class="container">
          <h4>Cosa è incluso</h4>
          <ul class="about-list">
            @foreach ($whats_included as $item)
            <li><img src="@asset('images/icons/check-green.svg')"> {{ $item['included'] }}</li>
          @endforeach
          </ul>
        </div>
      </section>

      <section class="section--m-xs content">
        <div class="container">
          <h4>Cosa non è incluso</h4>
          <ul class="about-list">
            <!-- todo -->
            @foreach ($whats_not_included as $item)
            <li><img src="@asset('images/icons/cancel-red.svg')"> {{ $item['included'] }}</li>
          @endforeach
          </ul>
        </div>
      </section>

      @if($landmarks)
      <section class="section--m-xs content">
        <div class="container">
          <h4>Tappe</h4>
          <ul class="about-list">
            <li>

              <img src="@asset('images/icons/map-yellow.svg')">

              @foreach ($landmarks as $item)
                {{ $item['included'] }},
              @endforeach
            </li>
          </ul>
        </div>
      </section>
      @endif

      <div class="container where">
        <h4>Punto di incontro</h4>
        {!! $location['address'] !!}

        <div class="responsive-map">
          <iframe src="{{ $location['gmap_url'] }}" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
      </div>
    </article>
  @endwhile

  @if ($related)
    <section class="related">
      <header class="related__header">
        <div class="related__container">
          <div class="related__title">{{ _x('Leggi altri articoli', 'corporate@blog', 'ys') }}</div>
        </div>
      </header><!-- /.related__header -->
      <div class="related__main">
        @component('components.carousel-full')
          @foreach ($related as $item)
            <div class="carousel__slide swiper-slide">
              @include('partials.cards/card-'.$item['post_type'])
            </div><!-- /.carousel__slide -->
          @endforeach
        @endcomponent
      </div><!-- /.related__main -->
    </section><!-- /.related -->
  @endif
@endsection
