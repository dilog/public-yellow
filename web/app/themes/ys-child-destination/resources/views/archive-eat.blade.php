@extends('layouts.app')

@section('below-header')
@include('partials.breadcrumbs')
@endsection

@section('content')
    <div
    class="page-header @if (isset($patch) && $position === 'right') page-header--has-right-patch @endif @if (isset($patch) && $position === 'left') page-header--has-left-patch @endif">
    @component('components.6-columns')
      <div class="page-header__content">
        <h1 class="page-header__title">Eat & Party</h1>


        @if (get_current_blog_id() == App\config('theme.destinations.rome'))
          <p class="page-header__subtitle">Sei a Roma, divertiti come i romani.</p>
        @elseif (get_current_blog_id() == App\config('theme.destinations.milan'))
          <p class="page-header__subtitle">Lavorare, mangiare, festeggiare e… ripetere.
            <p>Se siete preoccupati per il momento della prova costume non vi preoccupate… noi puntiamo sulla bellezza interiore!</p>
        @endif
      </div>
      @if (isset($patch))
        <div class="page-header__patch">
          @include('partials.patch', ['type' => $patch, 'animated' => true])
        </div>
      @endif
    @endcomponent
    </div><!-- /.page-header -->

    <section class="section eat-party-archive">
      @component('components.container')
        @foreach ($eat_and_party_posts as $key => $post)
          <div class="feature feature--horz {{ ($key % 2 == 0) ? 'feature--reverse' : ''  }}">
            <div class="feature__header-mobile">
              <h3 class="feature__title">{{ _x($post['title'], 'corporate@tour-experience', 'ys') }}</h3>
            </div>
            <!--<div class="feature__media {{ ($key % 2 == 0) ? 'has-wobble-arrow--left' : 'has-wobble-arrow--right' }}">-->
            <div class="feature__media">
              @component('components.carousel-card', ['shadow' => true])
                @foreach ($post['gallery'] as $gallery_item)
                  <div class="carousel__slide swiper-slide">
                    <figure class="carousel__image">
                      <img src="{{ $gallery_item['link'] }}" alt="" />
                    </figure>
                  </div><!-- /.carousel__slide -->
                @endforeach
              @endcomponent
            </div><!-- /.feature__media -->
            <div class="feature__main">
              <div class="feature__header">
                <h3 class="feature__title">{{ _x( $post['title'], 'corporate@tour-experience', 'ys') }}</h3>
              </div>
              <div class="feature__content">
                <p>
                  {{ _x($post['subtitle'], 'corporate@tour-experience', 'ys') }}
                </p>
              </div><!-- /.feature__content -->
              <div class="feature__action">
                <a class="button button--s" href="{{ $post['permalink'] }}">{{ __($post['post_link_label'], 'ys') }}</a>
              </div><!-- /.feature__action -->
            </div><!-- /.feature__main -->
          </div><!-- /.feature -->
        @endforeach
      @endcomponent
    </section><!-- /.section -->
@endsection
