@extends('layouts.app')
@section('content')
  @while(have_posts()) @php the_post() @endphp
    {{--
    //
    // Hero
    //
    --}}
    <section class="hero hero--destination">
      {!! $hero_image !!}
      <div class="hero__layer">
        <header class="hero__header">
          <div class="hero__title">
            {{ App::title() }}
          </div><!-- /.hero__title -->
          <div class="hero__subtitle">
            <a class="js-fb-gallery" href="#" role="button" data-gallery="{{ $gallery }}"><i class="icon ys-camera"></i> {{ _x('Gallery', 'destination@front-page', 'ys') }}</a>
          </div><!-- /.hero__subtitle -->
        </header><!-- /.hero__header -->
        <div class="hero__content">
          @component('components.10-columns')
            @include('partials.booking')
          @endcomponent
        </div><!-- /.hero__content -->
      </div>
    </section><!-- /.hero -->
    {{--
    //
    // Rooms
    //
    --}}
    <section class="section section--{!! get_field('section_background_color') !!}">

      <div class="intro__heading">
        <p class="h4 mb-3">{!! get_field('body_title') !!}</p>
        <p>{!! get_field('body_subtitle') !!}</p>
        <br>
      </div>

      <header class="section__header section__header--compact ">
        @component('components.6-columns')
          <h2 class="section__title h4">
            {{ _x('Le nostre camere', 'destination@front-page', 'ys') }}
          </h2>
        @endcomponent
      </header><!-- /.section__header -->
      <div class="section__main room__types">
        <div class="tabs js-tabs">
          @component('components.4-columns')
            <nav class="tabs__nav">
              <ul class="tabs__list">
                <li class="tabs__item tabs__item--active">
                  <a class="h5" href="#private-room" role="tab" aria-controls="private-room" aria-selected="true">{{ __('Dormitori', 'ys') }}</a>
                </li>
                <li class="tabs__item">
                  <a class="h5" href="#shared-room" role="tab" aria-controls="shared-room" aria-selected="false">{{ __('Private', 'ys') }}</a>
                </li>
              </ul>
            </nav><!-- /tabs__nav -->
          @endcomponent
        </div><!-- /.tabs -->
        {{--
          Private
        --}}
        <div id="private-room" class="tabs-content tabs-content--active tabs-content--shown" role="tabpanel">
          @component('components.container')
            <div class="feature feature--horz feature-standard-flow">
              <div class="feature__media">
                @component('components.carousel-card', ['shadow' => true])
                    <div class="carousel__slide swiper-slide">
                      <figure class="carousel__image">
                        <img src="{{ get_field('dormitori_pic') }}" alt="" />
                      </figure>
                    </div><!-- /.carousel__slide -->
                @endcomponent
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h3 class="feature__title">{{ _x('Dormitori', 'destination@front-page', 'ys') }}</h3>
                </div>
                <div class="feature__content">
                  <p>
                    {!! get_field('dormitori_text') !!}
                  </p>
                </div><!-- /.feature__content -->
                <div class="feature__action">
                  <a class="button button--s" href="/{{$room_link['destination']}}/room-cat/dormitori-misti-o-femminili/">{{ _x('Scopri i dormitori', 'destination@front-page', 'ys') }}</a>
                </div><!-- /.feature__action -->
              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
          @endcomponent
        </div><!-- /.tabs__content -->
        {{--
          Shared
        --}}
        <div id="shared-room" class="tabs-content" role="tabpanel">
          @component('components.container')
            <div class="feature feature--horz feature-standard-flow">
              <div class="feature__media">
                @component('components.carousel-card', ['shadow' => true])
                    <div class="carousel__slide swiper-slide">
                      <figure class="carousel__image">
                        <img src="{{ get_field('private_pic') }}" alt="" />
                      </figure>
                    </div><!-- /.carousel__slide -->
                @endcomponent
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__header">
                  <h3 class="feature__title">{{ _x('Camere private', 'destination@front-page', 'ys') }}</h3>
                </div>
                <div class="feature__content">
                  <p>
                    {!! get_field('private_text') !!}
                  </p>
                </div><!-- /.feature__content -->
                <div class="feature__action">
                  <a class="button button--s" href="/{{$room_link['destination']}}/room-cat/private/">{{ __('Scopri le camere private', 'destination@front-page', 'ys') }}</a>
                </div><!-- /.feature__action -->
              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
          @endcomponent
        </div><!-- /.tabs__content -->
      </div><!-- /.section__main -->
    </section><!-- /.section -->
    {{--
    //
    // Infographic
    //
    --}}
    <section class="section section--{!! get_field('section_background_color') !!} section--paddingless">
      @component('components.6-columns')
        <header class="section__header">
          <h3 class="section__title">{{ _x('I nostri spazi', 'destination@front-page', 'ys') }}</h3>
        </header><!-- /.section__header -->
      @endcomponent
      <div class="section__main">
        @component('components.container')
          {!! $infographic !!}
        @endcomponent
      </div><!-- /.section__main -->
    </section><!-- /.section -->
    {{--
    //
    // Features
    //
    --}}
    <section class="section section--{!! get_field('section_background_color') !!}">
      @component('components.container')
        <div class="features features--alternate">
          @foreach ($features as $item)
            <div class="feature feature--horz">
              <div class="feature__header-mobile">
                <h2 class="feature__title">{!! $item['title'] !!}</h2>
              </div>
              <div class="feature__media">
                @if ($item['gallery'])
                  @component('components.carousel-card', ['shadow' => true])
                    @foreach ($item['gallery'] as $image)
                      @include('partials.slides.card')
                    @endforeach
                  @endcomponent
                @endif
              </div><!-- /.feature__media -->
              <div class="feature__main">
                <div class="feature__preheader is-hidden-mobile mb-3">{!! $item['over_title'] !!}</div>
                <div class="feature__header">
                  <h2 class="feature__title">{!! $item['title'] !!}</h2>
                </div>
                <div class="feature__content">
                  <p>{!! $item['description'] !!}</p>
                </div>
                @if (isset($item['action']))
                  <div class="feature__action">
                    <a class="magic-button-outline" href="{{ $item['action']['link'] }}">{{ $item['action']['label'] }}</a>
                  </div>
                @endif
              </div><!-- /.feature__main -->
            </div><!-- /.feature -->
          @endforeach
        </div><!-- /.features -->
      @endcomponent
    </section><!-- /.section -->
    {{--
    //
    // Offers
    //
    --}}
    <section class="section section--full-width-mobile section--offers">
      @component('components.6-columns')
        <header class="section__header mb-0">
          <h2 class="section__title h3">{{ _x('Offerte speciali', 'corporate@front-page', 'ys') }}</h2>
        </header><!-- /.section__header -->
      @endcomponent
      <div class="section__main">
        @component('components.carousel-full', [
          'class' => 'carousel--offers carousel--has-patch',
        ])
          @foreach ($offers as $item)
            <div class="carousel__slide swiper-slide">
              @include('partials.cards.card-offer-with-image')
            </div><!-- /.carousel__slide -->
          @endforeach
        @endcomponent
      </div><!-- /.section__main -->
    </section><!-- /.section--offers -->
    {{--
    //
    // Facilities
    //
    --}}
    @include('partials.facilities')
    {{--
    //
    // Tips
    //
    --}}
    <section class="tile tile--violet tile--has-shadow">
      @component('components.8-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ __('Consigli dello staff', 'ys') }}</h3>
        </header><!-- /.tile__header -->
      @endcomponent
      <div class="tile__main">
        @component('components.8-columns')
          @component('components.carousel', ['namespace' => 'tips'])
            @foreach ($tips as $item)
              @include('partials.slides.tip')
            @endforeach
          @endcomponent
        @endcomponent
      </div><!-- /.tile__main -->
    </section><!-- /.tile -->
    {{--
    //
    // Testimonial
    //
    --}}
    <section class="tile tile--green tile--has-shadow">
      @component('components.8-columns')
        <header class="tile__header">
          <h3 class="tile__title">{{ __('Cosa dicono di Yellow Square', 'ys') }}</h3>
        </header><!-- /.tile__header -->
      @endcomponent
      <div class="tile__main">
        @component('components.8-columns')
          @component('components.carousel', ['namespace' => 'reviews'])
            @foreach ($reviews as $item)
              @include('partials.slides.review')
            @endforeach
          @endcomponent
        @endcomponent
      </div><!-- /.tile__main -->
    </section><!-- /.tile -->
    {{--
    //
    // Contacts
    //
    --}}
    @if ($contacts)
      <section class="contacts">
        <div class="contacts__main">
          <div class="contacts__title">{{ $contacts['title'] }}</div>
          <p class="contacts__address">{{ $contacts['street'] }}<br>{{ $contacts['zip'] }} {{ $contacts['city'] }}<br>{{ $contacts['country'] }}</p>
          <p>{{ __('Whatsapp:', 'ys') }}<br><span class="whatsapp-phone">{{ $contacts['phone'] }}</span></p>
          <p class="contacts__email">{!! $contacts['mail'] !!}</p>
          <p class="is-hidden-tablet">
            <a class="button button--outline" href="#" rel="noopener noreferrer" target="_blank" rel>{{ __('Dimmi di più', 'ys') }}</a>
          </p>
        </div>
        <div class="contacts__map">
          <div id="js-map" class="map" data-lat="{{ $contacts['map']['lat'] }}" data-lng="{{ $contacts['map']['lng'] }}"></div>
        </div>
      </section>
    @endif
    @include('partials.Front-Page.newsletter')
  @endwhile
@endsection
