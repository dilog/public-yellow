@extends('layouts.app')

@section('below-header')
  @include('partials.breadcrumbs')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <article class="{{ $classes }}">
      @component('components.8-columns')
        <header class="post-header">
          <div class="post-header__content">
            <h1 class="post-header__title entry-title"><span class="post-header__title-desc">{{ $category['title'] }}</span>{!! get_the_title() !!}</h1>
          </div><!-- /.post-header__content -->
          <div class="post-header__patch">
            @include('partials.patch', ['type' => $patch, 'animated' => true])
          </div>
        </header><!-- /.post-header -->
      @endcomponent



      @component('components.8-columns')
        <figure class="post-media">
          <img src="{{ $thumbnail }}" class="post-media__image" alt="">
        </figure><!-- /.post-media -->
      @endcomponent

      @component('components.6-columns')
        <div class="post-content entry-content">
          <div class="event-horizon">
            {!! $content !!}
          </div>
          @if ($coupon_code)
          <!-- todo -->
          <div style="border: dashed 2px;padding: 15px;background: white;border-radius: 12px;margin: 30px 0px;">
            Coupon: <strong>{!! $coupon_code !!}</strong>
          </div>
          @endif
        </div><!-- /.post-content -->
      @endcomponent



    </article>
  @endwhile

  @if ($related)
    <section class="related">
      <header class="related__header">
        <div class="related__container">
          <div class="related__title">{{ _x('Leggi altri articoli', 'corporate@blog', 'ys') }}</div>
        </div>
      </header><!-- /.related__header -->
      <div class="related__main">
        @component('components.carousel-full')
          @foreach ($related as $item)
            <div class="carousel__slide swiper-slide">
              @include('partials.cards/card-'.$item['post_type'])
            </div><!-- /.carousel__slide -->
          @endforeach
        @endcomponent
      </div><!-- /.related__main -->
    </section><!-- /.related -->
  @endif
@endsection
