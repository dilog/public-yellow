<?php
define('WP_DEV_TLD', 'yellow-square.test');
define('WP_PROD_TLD', 'staging-yellowsquare.dev01.it');

function dev_get_site_by_path($_site, $_domain, $_path, $_segments, $_paths)
{
    global $wpdb, $path;

    $domain = str_replace(WP_DEV_TLD, WP_PROD_TLD, $_domain);

    $site = $wpdb->get_row(
        $wpdb->prepare(
            "SELECT * FROM $wpdb->blogs WHERE domain = %s and path = %s",
            $domain,
            $_paths[0]
        )
    );
    $current_path = $_paths[0];

    if ($site === null) {
        $site = $wpdb->get_row(
            $wpdb->prepare(
                "SELECT * FROM $wpdb->blogs WHERE domain = %s and path = %s",
                $domain,
                '/'
            )
        );
        $current_path = '/';
    }

    $path = $current_path;

    return $site;
}
if (WP_ENV === 'development') {
    add_filter('pre_get_site_by_path', 'dev_get_site_by_path', 1, 5);
    add_filter('pre_get_network_by_path', 'dev_get_site_by_path', 1, 5);
}

/**
 * Filter the site_url and home options for each site, and
 * filter /wp-includes/link-template.php::network_site_url()
 * and /wp-includes/link-template.php::network_home_url()
 * so that our network site link is correct in the admin menu
 */
function dev_network_url($_url = '')
{
    return str_replace(WP_PROD_TLD, WP_DEV_TLD, $_url);
}
if (WP_ENV === 'development') {
    add_filter('network_site_url', 'dev_network_url');
    add_filter('network_home_url', 'dev_network_url');
    add_filter('redirect_network_admin_request', function () {
        return 0;
    });
    add_filter('option_siteurl', 'dev_network_url');
    add_filter('option_home', 'dev_network_url');
}
